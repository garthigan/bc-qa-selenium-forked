import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.bib import BibPage
from pages.core.user_dashboard import UserDashboardPage
from utils.bc_test_connector import TestConnector

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46012: Add a comment")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46012", "TestRail")
class TestC46012:
    def test_C46012(self):
        self.user = TestConnector.User().create(self.driver)
        self.item_id = "719979001"

        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.header.log_in(self.user.barcode, self.user.pin)
        bib_page.community_activity.add_comment.click()
        comment_body = "Added by Web Automation [{}]".format(datetime.datetime.now().strftime("%H%M%S%f"))
        bib_page.overlay.add_a_comment.edit_comment.clear()
        bib_page.overlay.add_a_comment.edit_comment.send_keys(comment_body)
        bib_page.overlay.add_a_comment.post_comment.click()
        bib_page.community_activity.comments[0].content.text.should.equal(comment_body)
        user_dashboard_page = UserDashboardPage(self.driver, configuration.system.base_url).open()
        user_dashboard_page.recent_activity.feed_events[0].comment.should.equal(comment_body)

        # Visit the Bib page and delete the comment added during the test run:
        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        bib_page.community_activity.add_comment.click()
        bib_page.overlay.add_a_comment.edit_comment.clear()
        bib_page.overlay.add_a_comment.post_comment.click()
