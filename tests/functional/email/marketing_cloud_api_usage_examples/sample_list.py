import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    NewListName = "PythonSDKList"

    # Create List 
    print('>>> Create List')
    post_list = FuelSDK.ET_List()
    post_list.auth_stub = stubObj
    post_list.props = {"ListName": NewListName, "Description": "This list was created with the PythonSDK", "Type": "Private"}
    postResponse = post_list.post()
    print('Post Status: ' + str(postResponse.status))
    print('Code: ' + str(postResponse.code))
    print('Message: ' + str(postResponse.message))
    print('Result Count: ' + str(len(postResponse.results)))
    print('Results: ' + str(postResponse.results))

    # Make sure the list created correctly and the 1st dict in it has a NewID...
    if postResponse.status and 'NewID' in postResponse.results[0]:
        
        newListID = postResponse.results[0]['NewID']
    
        # Retrieve newly created List by ID
        print('>>> Retrieve newly created List')
        get_list = FuelSDK.ET_List()
        get_list.auth_stub = stubObj
        get_list.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Client.PartnerClientKey", "ListName", "Description", "Category", "Type", "CustomerKey", "ListClassification",
                         "AutomatedEmail.ID"]
        get_list.search_filter = {'Property': 'ID', 'SimpleOperator': 'equals', 'Value': newListID}
        get_response = get_list.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('Results Length: ' + str(len(get_response.results)))
        print('Results: ' + str(get_response.results))
            
        # Update List 
        print('>>> Update List')
        patch_sub = FuelSDK.ET_List()
        patch_sub.auth_stub = stubObj
        patch_sub.props = {"ID": newListID, "Description": "I updated the description"}
        patch_response = patch_sub.patch()
        print('Patch Status: ' + str(patch_response.status))
        print('Code: ' + str(patch_response.code))
        print('Message: ' + str(patch_response.message))
        print('Result Count: ' + str(len(patch_response.results)))
        print('Results: ' + str(patch_response.results))
        
        # Retrieve List that should have description updated 
        print('>>> Retrieve List that should have description updated ')
        get_list = FuelSDK.ET_List()
        get_list.auth_stub = stubObj
        get_list.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Client.PartnerClientKey", "ListName", "Description", "Category", "Type", "CustomerKey", "ListClassification",
                         "AutomatedEmail.ID"]
        get_list.search_filter = {'Property': 'ID', 'SimpleOperator': 'equals', 'Value': newListID}
        get_response = get_list.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('Results Length: ' + str(len(get_response.results)))
        print('Results: ' + str(get_response.results))
        
        # Delete List
        print('>>> Delete List')
        delete_sub = FuelSDK.ET_List()
        delete_sub.auth_stub = stubObj
        delete_sub.props = {"ID": newListID}
        delete_response = delete_sub.delete()
        print('Delete Status: ' + str(delete_response.status))
        print('Code: ' + str(delete_response.code))
        print('Message: ' + str(delete_response.message))
        print('Results Length: ' + str(len(delete_response.results)))
        print('Results: ' + str(delete_response.results))
        
        # Retrieve List to confirm deletion
        print('>>> Retrieve List to confirm deletion')
        get_list = FuelSDK.ET_List()
        get_list.auth_stub = stubObj
        get_list.props = ["ID", "PartnerKey", "CreatedDate", "ModifiedDate", "Client.ID", "Client.PartnerClientKey", "ListName", "Description", "Category", "Type", "CustomerKey", "ListClassification",
                         "AutomatedEmail.ID"]
        get_list.search_filter = {'Property': 'ID', 'SimpleOperator': 'equals', 'Value': newListID}
        get_response = get_list.get()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('Results Length: ' + str(len(get_response.results)))
        print('Results: ' + str(get_response.results))

except Exception as e:
    print('Caught exception: ' + str(e))
