from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
from pypom import Region


class EventsHome(BasePage):

    URL_TEMPLATE = "/events"
    _search_textbox_locator = (By.CSS_SELECTOR, "[testid='main_search_input']")
    _search_box_icon_locator = (By.CSS_SELECTOR, ".header_collapsible_search_trigger")
    _show_more_locator = (By.CSS_SELECTOR, ".btn-lg")
    _search_results_locator = (By.CSS_SELECTOR, "div.row:nth-child(12)")
    _search_textbox_icon_locator = (By.CSS_SELECTOR, "div.header_search_box:nth-child(2)>span:nth-child(3)>button:nth-child(1)")
    _search_for_another_locator = (By.CSS_SELECTOR, ".clear-filters-btn")
    _header_hours_and_location_link_locator = (By.CSS_SELECTOR, "#header_locations_dropdown_trigger [aria-hidden='true']:nth-child(2)")
    _header_hours_and_location_textbox_locator = (By.CSS_SELECTOR, "[name='near_addr']")
    _header_hours_and_location_search_link_locator = (By.CSS_SELECTOR, ".icon-search")
    _header_hours_and_location_all_location_link_locator = (By.CSS_SELECTOR, "[testid='header_locations_see_all']")
    _clear_filters_search_result_link_locator = (By.CSS_SELECTOR, ".secondary-link")
    _number_of_search_results_locator = (By.CSS_SELECTOR, ".last-word")
    _events_locator = (By.CSS_SELECTOR, "div.row.event-row")
    _events_detail_offline_event_space_available_locator = (By.CSS_SELECTOR, ".notification-inverse")
    _event_detail_close_button_locator = (By.CSS_SELECTOR, "[class='glyphicon glyphicon-remove-2']")
    _event_detail_description_field_locator = (By.CSS_SELECTOR, "[itemprop='description']")
    _event_detail_suitable_locator = (By.CSS_SELECTOR, "[itemprop='audience']")
    _event_detail_type_locator = (By.CSS_SELECTOR, ".text-left")
    _event_detail_language_locator = (By.CSS_SELECTOR, "[itemprop='inLanguage']")
    _events_search_registration_status_locator = (By.CSS_SELECTOR, ".registration-status.pull-left")
    _detail_page_registration_required_locator = (By.CSS_SELECTOR, ".hidden-xs.collapsible-heading")
    _register_button_locator = (By.CSS_SELECTOR, ".btn-primary.hidden-print")
    _event_detail_seats_remaining_locator = (By.CSS_SELECTOR, ".notification-inverse.notification-success.notification-space-available")
    _event_detail_attendees_locator = (By.CSS_SELECTOR, ".chosen-single")
    _attendees_dropdown_selection_locator = (By.CSS_SELECTOR, ".active-result.ember-view")
    _complete_registration_locator = (By.CSS_SELECTOR, ".save-button.btn-primary")
    _registration_error_messages_locator = (By.CSS_SELECTOR, ".error-messages")
    _registration_text_fields_locator = (By.CSS_SELECTOR, ".ember-text-field.form-control")
    _registration_confirm_thanks_locator = (By.CSS_SELECTOR, ".thanks")
    _registration_confirmation_sent_locator = (By.CSS_SELECTOR, ".text-muted")
    _registration_confirmation_email_locator = (By.CSS_SELECTOR, ".confirmation-email")
    _detail_event_registration_status_locator = (By.CSS_SELECTOR, ".glyphicon.glyphicon-remove")
    _first_name_login_user_locator = (By.CSS_SELECTOR, ".first-name")
    _last_name_login_user_locator = (By.CSS_SELECTOR, ".last-name")
    _already_registered_message_locator = (By.CSS_SELECTOR, ".alert.alert-danger.alert-already-registered")
    _resend_confirmation_email_locator = (By.CSS_SELECTOR, ".progress-button.btn.btn-block.resend-button.btn-primary")
    _go_back_locator = (By.CSS_SELECTOR, ".center-block.cancel-wrapper")
    _date_header_locator = (By.CSS_SELECTOR, ".row.hidden-print .row.result-dates")
    _search_results_locators = (By.CSS_SELECTOR, ".last-word")
    _share_and_permalink_locators = (By.CSS_SELECTOR, ".btn.btn-default.btn-no-contrast.btn-sm")
    _permalink_url_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.permalink-field.form-control")
    _event_detail_location_name_locator = (By.CSS_SELECTOR, ".event-location")
    _event_detail_location_street_address = (By.CSS_SELECTOR, "[itemprop='streetAddress']")
    _event_detail_location_city = (By.CSS_SELECTOR, "[itemprop='addressLocality']")
    _event_detail_location_province_or_state = (By.CSS_SELECTOR, "[itemprop='addressRegion']")
    _event_detail_location_postal_code = (By.CSS_SELECTOR, "[itemprop='postalCode']")
    _event_detail_name = (By.CSS_SELECTOR, ".event-summary-title.hidden-print")
    _event_detail_time = (By.CSS_SELECTOR, ".event-time")
    _event_detail_date = (By.CSS_SELECTOR, ".event-date")
    _event_detail_map = (By.CSS_SELECTOR, ".ember-view.google-map-container")
    _event_link_text_locator = (By.CSS_SELECTOR, ".event-title .ember-view")

    @property
    def search_box_icon(self):
        return self.find_element(*self._search_box_icon_locator)

    @property
    def is_show_more_displayed(self):
        try:
            return self.find_element(*self._show_more_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def search_textbox(self):
        return self.find_element(*self._search_textbox_locator)

    @property
    def search_textbox_icon(self):
        return self.find_element(*self._search_textbox_icon_locator)

    @property
    def search_for_another(self):
        return self.find_element(*self._search_for_another_locator)

    @property
    def is_search_for_another_displayed(self):
        try:
            return self.find_element(*self._search_for_another_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_link(self):
        return self.find_element(*self._header_hours_and_location_link_locator)

    @property
    def is_hours_and_location_link_displayed(self):
        try:
            return self.find_element(*self._header_hours_and_location_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_textbox(self):
        return self.find_element(*self._header_hours_and_location_textbox_locator)

    @property
    def is_hours_and_location_textbox_displayed(self):
        try:
            return self.find_element(*self._header_hours_and_location_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_search_link(self):
        return self.find_element(*self._header_hours_and_location_search_link_locator)

    @property
    def is_hours_and_location_search_link_displayed(self):
        try:
            return self.find_element(*self._header_hours_and_location_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def hours_and_location_all_location(self):
        return self.find_element(*self._header_hours_and_location_all_location_link_locator)

    @property
    def is_clear_filters_search_result_link_displayed(self):
        try:
            return self.find_element(*self._clear_filters_search_result_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def events_detail_offline_event_space_available(self):
        try:
            return self.find_element(*self._events_detail_offline_event_space_available_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def event_detail_close_button(self):
        return self.find_element(*self._event_detail_close_button_locator)

    @property
    def is_close_button_displayed(self):
        try:
            return self.find_element(*self._event_detail_close_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def description_field(self):
        return self.find_element(*self._event_detail_description_field_locator)

    @property
    def suitable_field(self):
        return self.find_element(*self._event_detail_suitable_locator)

    @property
    def type_field(self):
        return self.find_element(*self._event_detail_type_locator)

    @property
    def language_field(self):
        return self.find_element(*self._event_detail_language_locator)

    @property
    def location_name(self):
        return self.find_element(*self._event_detail_location_name_locator)

    @property
    def location_street_address(self):
        return self.find_element(*self._event_detail_location_street_address)

    @property
    def location_city(self):
        return self.find_element(*self._event_detail_location_city)

    @property
    def location_province_or_state(self):
        return self.find_element(*self._event_detail_location_province_or_state)

    @property
    def location_postal_code(self):
        return self.find_element(*self._event_detail_location_postal_code)

    @property
    def event_name(self):
        return self.find_element(*self._event_detail_name)

    @property
    def event_time(self):
        return self.find_element(*self._event_detail_time)

    @property
    def event_date(self):
        return self.find_element(*self._event_detail_date)

    @property
    def location_map(self):
        return self.find_element(*self._event_detail_map)

    @property
    def is_location_map_displayed(self):
        try:
            return self.find_element(*self._event_detail_map).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def registration_status(self):
        return self.find_elements(*self._events_search_registration_status_locator)

    @property
    def registration_required(self):
        return self.find_elements(*self._detail_page_registration_required_locator)

    @property
    def register_button(self):
        return self.find_element(*self._register_button_locator)

    @property
    def is_register_button_displayed(self):
        try:
            return self.find_element(*self._register_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def seats_remaining(self):
        return self.find_element(*self._event_detail_seats_remaining_locator)

    @property
    def attendees_dropdown(self):
        return self.find_element(*self._event_detail_attendees_locator)

    @property
    def is_attendees_dropdown_displayed(self):
        return self.find_element(*self._event_detail_attendees_locator).is_displayed()

    @property
    def attendees_dropdown_selection(self):
        return self.find_elements(*self._attendees_dropdown_selection_locator)

    @property
    def never_mind_go_back(self):
        return self.find_elements(*self._clear_filters_search_result_link_locator)

    @property
    def complete_registration(self):
        return self.find_element(*self._complete_registration_locator)

    @property
    def is_complete_registration_displayed(self):
        try:
            return self.find_element(*self._complete_registration_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def registration_error_messages(self):
        return self.find_elements(*self._registration_error_messages_locator)

    @property
    def registration_text_fields(self):
        return self.find_elements(*self._registration_text_fields_locator)

    @property
    def thanks_for_registering(self):
        return self.find_element(*self._registration_confirm_thanks_locator)

    @property
    def is_thanks_for_registering_displayed(self):
        try:
            return self.find_element(*self._registration_confirm_thanks_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def confirmation_email_sent(self):
        return self.find_element(*self._registration_confirmation_sent_locator)

    @property
    def confirmation_email_address(self):
        return self.find_element(*self._registration_confirmation_email_locator)

    @property
    def detail_registration_status(self):
        return self.find_elements(*self._detail_event_registration_status_locator)

    @property
    def first_name_login_user(self):
        return self.find_element(*self._first_name_login_user_locator)

    @property
    def is_first_name_login_user_displayed(self):
        try:
            return self.find_element(*self._first_name_login_user_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def last_name_login_user(self):
        return self.find_element(*self._last_name_login_user_locator)

    @property
    def is_last_name_login_user_displayed(self):
        try:
            return self.find_element(*self._last_name_login_user_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def date_header(self):
        return self.find_elements(*self._date_header_locator)

    # Registration must start from detail view with registration available
    def event_detail_registration(self,  first_name, last_name, email, attendees, user_status):
        attendees_dropdown = attendees - 1
        if user_status == "logged_in":
            self.wait.until(lambda s: self.is_register_button_displayed)
            self.register_button.click()
            self.wait.until(lambda s: self.is_complete_registration_displayed)
            self.registration_text_fields[2].clear()
            self.registration_text_fields[2].send_keys(email)
            self.attendees_dropdown.click()
            self.attendees_dropdown_selection[attendees_dropdown].click()
            self.complete_registration.click()
            self.wait.until(lambda s: self.is_thanks_for_registering_displayed)
            self.thanks_for_registering.text.should.equal("Thanks for registering!")
            self.confirmation_email_sent.text.should.equal("We've sent a confirmation email to")
            self.confirmation_email_address.text.should.equal(email)
        else:
            self.wait.until(lambda s: self.is_register_button_displayed)
            self.register_button.click()
            self.wait.until(lambda s: self.is_complete_registration_displayed)
            self.registration_text_fields[2].send_keys(first_name)
            self.registration_text_fields[3].send_keys(last_name)
            self.registration_text_fields[4].send_keys(email)
            self.attendees_dropdown.click()
            self.attendees_dropdown_selection[attendees_dropdown].click()
            self.complete_registration.click()
            self.wait.until(lambda s: self.is_thanks_for_registering_displayed)
            self.thanks_for_registering.text.should.equal("Thanks for registering!")
            self.confirmation_email_sent.text.should.equal("We've sent a confirmation email to")
            self.confirmation_email_address.text.should.equal(email)
            self.wait.until(lambda s: self.is_close_button_displayed)
            self.event_detail_close_button.click()

    @property
    def already_registered(self):
        return self.find_element(*self._already_registered_message_locator)

    @property
    def go_back(self):
        return self.find_element(*self._go_back_locator)

    @property
    def is_go_back_displayed(self):
        try:
            return self.find_element(*self._go_back_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def all_search_results(self):
        return self.find_elements(*self._search_results_locators)

    @property
    def resend_email_confirmation(self):
        return self.find_element(*self._resend_confirmation_email_locator)

    @property
    def is_resend_email_confirmation_displayed(self):
        try:
            return self.find_element(*self._resend_confirmation_email_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def share_or_permalink(self):
        return self.find_elements(*self._share_and_permalink_locators)

    @property
    def permalink_url(self):
        return self.find_element(*self._permalink_url_locator)

    @property
    def is_permalink_url_displayed(self):
        try:
            return self.find_element(*self._permalink_url_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def event_name_last_word(self):
        return self.find_element(*self._event_link_text_locator)

    @property
    def is_event_name_last_word_displayed(self):
        try:
            return self.find_element(*self._event_link_text_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def events(self):
        return [self.EventRow(self, element) for element in self.find_elements(*self._events_locator)]

    class EventRow(Region):

        _title_locator = (By.CSS_SELECTOR, "div.row.event-row > div > div > div > h2 > a")

        @property
        def title(self):
            return self.find_element(*self._title_locator)
