import pytest
import allure
import sure
import sys

sys.path.append('tests')

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.v2.shelves import ShelvesPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C69368: Empty shelves availability results page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/69368", "TestRail")
class TestC69368:
    def test_C69368(self):
        # Log into an account with shelf items and perform a "Find Available Titles" action such that no search results
        # Are present
        self.name ="21817002369272"
        self.password = "1992"
        base_url = "https://coaldale.demo.bibliocommons.com"
        home_page = HomePage(self.driver, base_url).open()
        home_page.header.log_in(self.name, self.password)
        home_page.header.login_state_user_logged_in.click()

        # Go to shelf with only unavailable items
        home_page.header.for_later_shelf.click()
        shelves_page = ShelvesPage(self.driver)

        # Click Find Available Titles button
        shelves_page.find_available_button.click()
        search_results = SearchResultsPage(self.driver)

        # Verify being sent to the "no results" page correctly:

        #   Assert that search results header shows correct shelf
        search_results.shelves_availability_header.text.should.equal("Titles from my For Later shelf")

        #   Assert that there are no results
        search_results.wait.until(lambda s: search_results.is_no_titles_text_displayed)
        search_results.no_titles_text.text.should.contain("No available items found.")

        # Go back to shelves
        search_results.shelves_availability_back_link.click()
        shelves_page = ShelvesPage(self.driver)

        # Verify sent to the correct shelf
        shelves_page.wait.until(lambda s: shelves_page.is_current_shelf_for_later)
        shelves_page.is_current_shelf_for_later.should.be.true
