from selenium.webdriver.common.by import By
from pages.web.staff_base import StaffBasePage
from selenium.common.exceptions import NoSuchElementException
from pages.web.components.select_widget_image import SelectWidgetImage
from pages.web.components.insert_media import InsertMedia
from selenium.webdriver.support import expected_conditions as EC
from utils.selenium_helpers import click


class BannerHeroSlideBasePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/post-new.php"

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _save_draft_locator = (By.CSS_SELECTOR, "input#save-post")
    _publish_locator = (By.CSS_SELECTOR, "input#publish")
    _success_message_locator = (By.CSS_SELECTOR, "#message>p")

    # Taxonomies
    _audience_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_audience_category-0']")
    _audience_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_audience_category_0_chosen > div > ul > li")
    _related_format_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_related_format-0']")
    _related_format_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_related_format_0_chosen > div > ul > li")
    _programs_and_campaigns_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bc_program-0']")
    _programs_and_campaigns_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bc_program_0_chosen > div > ul > li")
    _genre_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_genre-0']")
    _genre_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_tax_genre_0_chosen > div > ul > li")
    _topic_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_topic-0']")
    _topic_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_tax_topic_0_chosen > div > ul > li")
    _tags_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-post_tag-0']")
    _tags_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_post_tag_0_chosen > div > ul > li")
    _tags_field_search_choices_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_post_tag_0_chosen > ul > li.search-choice > span")
    _free_text_tags_input_locator = (By.CSS_SELECTOR, "input#new-tag-post_tag")

    @property
    def page_heading(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def save_draft(self):
        return self.find_element(*self._save_draft_locator)

    @property
    def publish(self):
        return self.find_element(*self._publish_locator)

    @property
    def is_success_message_displayed(self):
        try:
            return self.find_element(*self._success_message_locator).is_displayed()
        except NoSuchElementException:
            return False

    def scroll_to_top(self):
        self.driver.execute_script("return arguments[0].scrollIntoView(true);", self.page_heading)

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)

    @property
    def insert_media(self):
        return InsertMedia(self)

    @property
    def is_free_text_tags_field_displayed(self):
        try:
            return self.find_element(*self._free_text_tags_input_locator).is_displayed()
        except NoSuchElementException:
            return False

    def taxonomy(self, taxonomy):
        taxonomies = {
            "audience": self._audience_dropdown_locator,
            "related format": self._related_format_dropdown_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_locator,
            "genre": self._genre_dropdown_locator,
            "topic": self._topic_dropdown_locator,
            "tags": self._tags_dropdown_locator
        }

        return self.find_element(*(taxonomies.get(taxonomy.casefold())))

    def select_taxonomy(self, taxonomy_dropdown, taxonomy_name):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == taxonomy_name:
                    self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    def is_taxonomy_displayed(self, taxonomy):
        taxonomies = {
            "audience": self._audience_dropdown_locator,
            "related format": self._related_format_dropdown_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_locator,
            "genre": self._genre_dropdown_locator,
            "topic": self._topic_dropdown_locator,
            "tags": self._tags_dropdown_locator
        }

        try:
            return self.find_element(*(taxonomies.get(taxonomy.casefold()))).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_tags_taxonomy_displayed(self):
        try:
            return self.find_element(*self._tags_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_tags_taxonomy_term_selected(self):
        results = self.find_elements(*self._tags_field_search_choices_locator)

        if results is not None:
            if len(results) > 0:
                return True
            else:
                return False
        else:
            raise Exception

    def taxonomy_terms(self, taxonomy):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        _ = []

        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        for index, result in enumerate(results):
            _.append(result.get_attribute("textContent"))

        return _

    def select_taxonomy_term(self, taxonomy_type, taxonomy_term):
        click(self.taxonomy(taxonomy_type))
        click(self.select_taxonomy(taxonomy_type, taxonomy_term))

