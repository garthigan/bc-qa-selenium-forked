from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from pages.web.staff_base import StaffBasePage


class CreateNewFormPage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/admin.php?page=gf_new_form"

    _create_new_form_window_locator = (By.CSS_SELECTOR, "#TB_window")
    _form_title_locator = (By.CSS_SELECTOR, "#new_form_title")
    _form_description_locator = (By.CSS_SELECTOR, "#new_form_description")
    _create_form_locator = (By.CSS_SELECTOR, "#save_new_form")

    @property
    def visibility_of_form_window(self):
        self.wait.until(EC.invisibility_of_element_located(self._create_new_form_window_locator))
        return False

    @property
    def form_title(self):
        return self.find_element(*self._form_title_locator)

    @property
    def form_description(self):
        return self.find_element(*self._form_description_locator)

    @property
    def create_form(self):
        return self.find_element(*self._create_form_locator)
