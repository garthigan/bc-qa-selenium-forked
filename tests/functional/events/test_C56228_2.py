import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
from mimesis import Text, Person
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from pages.events.manage_registration import ManageRegistration
from selenium.webdriver import ActionChains
person = Person('en')


@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56228_1 - Register and Display Available Seating")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56228", "TestRail")
class TestC56235:
    def test_c56235(self):

        self.base_url = "https://chipublib.demo.bibliocommons.com/events/"

        # log in
        home_page = HomePage(self.driver, self.base_url).open()
        home_page.header.log_in("cpltest3", "60643")
        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # navigate to events page
        events_admin = AdminEventPage(self.driver, self.base_url+"admin")
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)
        title_name = "test_C56228"

        events_admin.title_textbox.send_keys(title_name)
        events_admin.type_dropdown[0].click()
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_displayed)
        events_admin.audience_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        events_admin.wait.until(lambda s: events_admin.is_description_field_displayed)
        description = Text('en').text(quantity=3)
        events_admin.description_field.send_keys(description)
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[80].is_displayed())
        events_admin.start_time[80].click()

        capacity = 10
        events_admin.registration_required_radio_buttons[1].click()
        events_admin.event_capacity_text_box.send_keys(capacity)

        # location and publish
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_archer_height_displayed)
        events_admin.location_dropdown_archer.click()

        events_admin.wait.until(lambda s: events_admin.save_and_publish.is_enabled())
        events_admin.save_and_publish.click()
        events_admin.wait.until(lambda s: events_admin.is_left_chevron_button_displayed)

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # search for event
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        home_page_events.registration_status[1].text.should.equal(f"{capacity} seat(s) remaining")
        events_admin.event_name_last_word.text.should.equal(title_name)
        events_admin.event_name_last_word.click()

        # verify fields
        home_page_events.wait.until(lambda s: home_page_events.is_register_button_displayed)
        home_page_events.seats_remaining.text.should.equal(f"{capacity} seats remaining")
        home_page_events.registration_required[2].text.should.equal("REGISTRATION REQUIRED")
        home_page_events.register_button.click()
        home_page_events.wait.until(lambda s: home_page_events.is_first_name_login_user_displayed)
        registrant_1_first_name = home_page_events.first_name_login_user.text
        home_page_events.wait.until(lambda s: home_page_events.is_last_name_login_user_displayed)
        registrant_1_last_name = home_page_events.last_name_login_user.text
        home_page_events.wait.until(lambda s: home_page_events.is_complete_registration_displayed)

        # register first registrant
        home_page_events.wait.until(lambda s: home_page_events.is_complete_registration_displayed)
        home_page_events.registration_text_fields[2].clear()
        home_page_events.complete_registration.click()
        home_page_events.registration_error_messages[0].text.should.equal("Invalid email")
        registrant_1_email = person.email()
        home_page_events.registration_text_fields[2].send_keys(registrant_1_email)
        home_page_events.attendees_dropdown.click()
        home_page_events.attendees_dropdown_selection[9].text.should.equal(f'{capacity}')
        home_page_events.attendees_dropdown_selection[4].click()
        home_page_events.complete_registration.click()
        home_page_events.wait.until(lambda s: home_page_events.is_thanks_for_registering_displayed)
        home_page_events.thanks_for_registering.text.should.equal("Thanks for registering!")
        home_page_events.confirmation_email_sent.text.should.equal("We've sent a confirmation email to")
        home_page_events.confirmation_email_address.text.should.equal(registrant_1_email)

        # confirm seats decreased
        home_page_events.wait.until(lambda s: home_page_events.is_close_button_displayed)
        home_page_events.event_detail_close_button.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        new_capacity = capacity - 5
        home_page_events.registration_status[1].text.should.equal(f"{new_capacity} seat(s) remaining")
        events_admin.event_name_last_word.text.should.equal(title_name)
        events_admin.event_name_last_word.click()
        home_page_events.wait.until(lambda s: home_page_events.is_register_button_displayed)
        home_page_events.seats_remaining.text.should.equal(f"{new_capacity} seats remaining")

        # register 1st registrant again
        home_page_events.register_button.click()
        home_page_events.wait.until(lambda s: home_page_events.is_complete_registration_displayed)
        home_page_events.registration_text_fields[2].clear()
        home_page_events.registration_text_fields[2].send_keys(registrant_1_email)
        home_page_events.attendees_dropdown.click()
        home_page_events.attendees_dropdown_selection[4].text.should.equal(f'{new_capacity}')
        home_page_events.attendees_dropdown_selection[4].click()
        home_page_events.complete_registration.click()
        home_page_events.wait.until(lambda s: home_page_events.is_resend_email_confirmation_displayed)
        home_page_events.already_registered.text.should.equal("It seems that you have already registered for this event.")
        home_page_events.is_resend_email_confirmation_displayed.should.be.true
        home_page_events.go_back.click()
        home_page_events.wait.until(lambda s: home_page_events.is_register_button_displayed)

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        home_page.header.log_out()
        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        home_page_events.registration_status[1].text.should.equal(f"{new_capacity} seat(s) remaining")
        events_admin.event_name_last_word.text.should.equal(title_name)
        events_admin.event_name_last_word.click()
        home_page_events.wait.until(lambda s: home_page_events.is_register_button_displayed)
        home_page_events.seats_remaining.text.should.equal(f"{new_capacity} seats remaining")

        # registration 2 register
        registrant_2_first_name = person.name()
        registrant_2_last_name = person.last_name()
        registrant_2_email = person.email()
        attendees = new_capacity
        home_page_events.event_detail_registration(registrant_2_first_name, registrant_2_last_name, registrant_2_email, attendees, "logged_out")

        # confirm registration is closed
        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        home_page_events.registration_status[1].text.should.equal("Registration closed")

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        home_page.header.log_in("cpltest3", "60643")
        events_admin.wait.until(lambda s: events_admin.is_admin_link_displayed)
        events_admin.open()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)
        events_admin.retry_search_until_results_appear(title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)
        manage_registration_button = events_admin.admin_event_action_links[4]
        ActionChains(self.driver).move_to_element(manage_registration_button).perform()
        events_admin.admin_event_action_links[4].click()

        manage_registration = ManageRegistration(self.driver)
        manage_registration.wait.until(lambda s: manage_registration.registration_tabs[21].is_displayed())
        manage_registration.registration_tabs[-1].is_displayed().should.be.true
        manage_registration.registration_tabs[-2].is_displayed().should.be.true
        manage_registration.is_register_user_displayed.should.be.false
        manage_registration.wait.until(lambda s: manage_registration.is_number_of_spots_reserved_displayed)
        manage_registration.number_of_spots_reserved.text.should.equal(f"{capacity} of {capacity} spots reserved")
        manage_registration.is_this_event_full_displayed.should.be.true
        list_registrants = f"{registrant_1_last_name}, {registrant_1_first_name}", f"{registrant_2_last_name}, {registrant_2_first_name}"
        sorted_list_registrants = sorted(list_registrants)
        manage_registration.name_column[1].text.should.contain(str(sorted_list_registrants[0]))
        manage_registration.name_column[2].text.should.contain(str(sorted_list_registrants[1]))
        if list_registrants[0] == sorted_list_registrants[0]:
            manage_registration.contact_column[1].text.should.equal(registrant_1_email)
        else:
            manage_registration.contact_column[2].text.should.equal(registrant_1_email)

        if list_registrants[1] == sorted_list_registrants[1]:
            manage_registration.contact_column[2].text.should.equal(registrant_2_email)
        else:
            manage_registration.contact_column[1].text.should.equal(registrant_2_email)

        registrant_1_spots = int(manage_registration.spots_reserved_column[1].text)
        registrant_2_spots = int(manage_registration.spots_reserved_column[2].text)
        total_registrant_spots = registrant_1_spots + registrant_2_spots
        if total_registrant_spots == capacity:
            spots_reserved = True
        else:
            spots_reserved = False
        spots_reserved.should.be.true
        manage_registration.wait.until(lambda s: manage_registration.is_back_to_event_listing_displayed)
        manage_registration.back_to_event_listing.click()

        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        cancel_button = events_admin.admin_event_action_links[3]
        ActionChains(self.driver).move_to_element(cancel_button).perform()
        events_admin.wait.until(lambda s: events_admin.admin_event_action_links[3].is_displayed())
        events_admin.admin_event_action_links[3].click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)
        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()

        delete_button = events_admin.admin_event_action_links[7]
        ActionChains(self.driver).move_to_element(delete_button).perform()
        events_admin.wait.until(lambda s: events_admin.admin_event_action_links[7].is_displayed())
        events_admin.admin_event_action_links[7].click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)
        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
