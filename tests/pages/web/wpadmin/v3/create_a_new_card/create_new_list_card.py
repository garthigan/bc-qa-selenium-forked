from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_a_new_card.new_card_base import NewCardBasePage
from pages.web.components.select_widget_image import SelectWidgetImage


class CreateNewListCard(NewCardBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")

    # Card Information
    _list_url_locator = (By.CSS_SELECTOR, "input#fm-bw_card_list-0-bw_card_url-0")
    _grab_list_info_locator = (By.CSS_SELECTOR, "button#js-grab-list-button")
    _card_image_locator = (By.CSS_SELECTOR, "button#js--open-crop")
    _card_title_locator = (By.CSS_SELECTOR, "input[data-key='settings-card-title']")
    _card_description_locator = (By.CSS_SELECTOR, "textarea[data-key='settings-card-description']")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def list_url(self):
        return self.find_element(*self._list_url_locator)

    @property
    def grab_list_info(self):
        return self.find_element(*self._grab_list_info_locator)

    @property
    def card_image(self):
        return self.find_element(*self._card_image_locator)

    @property
    def card_title(self):
        return self.find_element(*self._card_title_locator)

    @property
    def card_description(self):
        return self.find_element(*self._card_description_locator)

    @property
    def select_widget_image(self):
        return SelectWidgetImage(self)
