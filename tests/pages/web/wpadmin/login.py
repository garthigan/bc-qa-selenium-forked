from pypom import Page
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.web.components.wpheader import WPHeader
from pages.web.components.wpsidemenu import WPSideMenu
from utils.selenium_helpers import enter_text


class LoginPage(Page):

    URL_TEMPLATE = "/wp-admin"

    _username_wordpress_locator = (By.ID, "user_login")
    _password_wordpress_locator = (By.ID, "user_pass")
    _login_button_wordpress_locator = (By.ID, "wp-submit")
    _v3_landing_page_locator = (By.CSS_SELECTOR, "[data-key='dashboard-title']")
    _v2_landing_page_locator = (By.CSS_SELECTOR, "[id='dashboard-widgets-wrap']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._username_wordpress_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def username_wordpress(self):
        return self.find_element(*self._username_wordpress_locator)

    @property
    def password_wordpress(self):
        return self.find_element(*self._password_wordpress_locator)

    @property
    def log_in_button_wordpress(self):
        return self.find_element(*self._login_button_wordpress_locator)

    def log_in(self, user_name, user_password):
        self.wait.until(lambda s: self.loaded)
        self.wait.until(lambda condition: self.username_wordpress.is_displayed())
        enter_text(self.username_wordpress, user_name)
        self.wait.until(lambda condition: self.password_wordpress.is_displayed())
        enter_text(self.password_wordpress, user_password)
        self.log_in_button_wordpress.click()

    # After logged-in
    @property
    def wpheader(self):
        return WPHeader(self)

    @property
    def wpsidemenu(self):
        return WPSideMenu(self)

    @property
    def is_v3_landing_page_displayed(self):
        return self.find_element(*self._v3_landing_page_locator).is_displayed()

    @property
    def is_v2_landing_page_displayed(self):
        return self.find_element(*self._v2_landing_page_locator).is_displayed()
