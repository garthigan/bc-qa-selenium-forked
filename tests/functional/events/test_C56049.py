import pytest
import allure
# noinspection PyUnresolvedReferences
import sure
from mimesis import Text
from pages.core.home import HomePage
from pages.events.events_admin import AdminEventPage
from pages.events.home_page_events import EventsHome
from selenium.webdriver import ActionChains


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56049 - Create Event with only required fields- Event Test1")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56049", "TestRail")
class TestC56049:
    def test_c56049(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/events"

        # log in
        home_page = HomePage(self.driver, self.base_url).open()

        home_page_events = EventsHome(self.driver)
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        # navigate to events page
        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)

        title_name = "py_test_C56049"

        events_admin.title_textbox.send_keys(title_name)
        events_admin.wait.until(lambda s: events_admin.type_dropdown[0].is_displayed())
        events_admin.type_dropdown[0].click()
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        events_admin.audience_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        events_admin.wait.until(lambda s: events_admin.is_description_field_displayed)
        description_text = Text('en').text(quantity=1)
        events_admin.description_field.send_keys(description_text)
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[80].is_displayed())
        events_admin.start_time[80].click()
        type_dropdown = events_admin.dropdown_required_choices[0].text
        audience_dropdown = events_admin.dropdown_required_choices[1].text
        language_dropdown = events_admin.dropdown_required_choices[2].text

        # location and publish
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_albany_park_displayed)
        events_admin.location_dropdown_albany_park.click()

        events_admin.wait.until(lambda s: events_admin.save_and_publish.is_enabled())
        events_admin.save_and_publish.click()
        events_admin.wait.until(lambda s: events_admin.is_left_chevron_button_displayed)

        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)

        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page_events.search_textbox.send_keys(title_name)
        home_page_events.search_textbox_icon.click()
        home_page_events.wait.until(lambda s: events_admin.is_event_name_last_word_displayed)
        events_admin.event_name_last_word.text.should.equal(title_name)
        events_admin.event_name_last_word.click()
        home_page_events.wait.until(lambda s: home_page_events.is_close_button_displayed)
        home_page_events.description_field.text.should.equal(description_text)
        home_page_events.suitable_field.text.should.equal(audience_dropdown)
        home_page_events.type_field.text.should.equal(type_dropdown)
        language_dropdown.should.contain(home_page_events.language_field.text)
        home_page_events.event_detail_close_button.click()

        # deleting events
        home_page.open()
        home_page_events.wait.until(lambda s: home_page_events.is_show_more_displayed)
        events_admin.wait.until(lambda s: events_admin.is_admin_link_displayed)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)

        events_admin.retry_search_until_results_appear(title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)

        delete_button = events_admin.admin_delete_button
        ActionChains(self.driver).move_to_element(delete_button).perform()
        events_admin.wait.until(lambda s: events_admin.admin_event_action_links[6].is_displayed())
        events_admin.admin_event_action_links[6].click()
        events_admin.wait.until(lambda s: events_admin.is_confirm_delete_button_displayed)

        events_admin.confirm_delete_button.click()
        events_admin.wait.until(lambda s: events_admin.is_close_delete_overlay_button_displayed)
        events_admin.close_delete_overlay_button.click()
        events_admin.wait.until(lambda s: events_admin.no_results_displayed)
        events_admin.assert_is_no_results_displayed.should.be.true
