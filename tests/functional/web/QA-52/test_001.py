import pytest
import allure
import sure
import configuration.system
import configuration.user
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.dashboard import DashboardPage


@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
# @allure.title("")
# @allure.testcase("", "TestRail")
class Test001:
    def test_001(self):

        login_page = LoginPage(self.driver, configuration.system.urls_web[0]).open()
        login_page.log_in(configuration.user.user['web']['stage']['libadmin']['name'], configuration.user.user['web']['stage']['libadmin']['password'])
        login_page.wait.until(lambda s: login_page.wpheader.is_wp_admin_header_displayed)
        login_page.wpsidemenu.is_wp_admin_sidemenu_displayed.should.be.true

        admin_page = DashboardPage(self.driver, configuration.system.urls_web[0]).open()
        main_menu = admin_page.wpsidemenu.main_menu_list
        len(main_menu).should.equal(10)
        main_menu[0].text.should.match("Dashboard")
        main_menu[1].text.should.match("All Content")
        admin_page.wpsidemenu.current_menu_item.text.should.match("Dashboard")
        len(admin_page.wpsidemenu.displayed_submenu_list).should.equal(0)

        admin_page.wpsidemenu.menu_forms.click()
        len(admin_page.wpsidemenu.displayed_submenu_list).should.equal(6)
        admin_page.wpsidemenu.current_submenu_item.text.should.match(admin_page.wpsidemenu.displayed_submenu_list[0].text)
        admin_page.wpsidemenu.submenu_form_entries.click()
        admin_page.wpsidemenu.current_submenu_item.text.should.match(admin_page.wpsidemenu.submenu_form_entries.text)

        admin_page.wpsidemenu.menu_switch_to_v2_menu.click()
        admin_page.wait.until(lambda s: admin_page.wpsidemenu.menu_switch_to_v3_menu)
        len(admin_page.wpsidemenu.main_menu_list).should.equal(18)
        admin_page.wpsidemenu.current_submenu_item.text.should.match(admin_page.wpsidemenu.v2_submenu_entries.text)

        admin_page.wpsidemenu.v2_menu_pages.click()
        admin_page.wpsidemenu.current_submenu_item.text.should.match(admin_page.wpsidemenu.displayed_submenu_list[0].text)

        admin_page.wpsidemenu.menu_switch_to_v3_menu.click()
        admin_page.wait.until(lambda s: admin_page.wpsidemenu.menu_switch_to_v2_menu)
        admin_page.wpsidemenu.current_menu_item.text.should.match("All Content")
        len(admin_page.wpsidemenu.displayed_submenu_list).should.equal(10)
        admin_page.wpsidemenu.current_submenu_item.text.should.match("All Pages")

