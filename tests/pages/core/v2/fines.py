from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
from pypom import Page, Region

# https://<library>.<environment>.bibliocommons.com/v2/fines


class FinesPage(BasePage):

    URL_TEMPLATE = "/v2/fines"

    _heading_locator = (By.CLASS_NAME, "page-heading")  # "Fees" heading
    _fines_total_locator = (By.CLASS_NAME, "cp-fines-total-capsule")
    _fines_table_rows_locator = (By.CSS_SELECTOR, "table.fines-table tbody tr")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def fines_total(self):
        return self.find_element(*self._fines_total_locator)

    @property
    def fines(self):
        return [self.Fine(self, element) for element in self.find_elements(*self._fines_table_rows_locator)]

    class Fine(Region):
        _checkbox_locator = (By.CSS_SELECTOR, "button[role='checkbox']")
        _date_locator = (By.CSS_SELECTOR, "[data-label='Date'] > span")
        _details_locator = (By.CSS_SELECTOR, "[data-label='Details'] a")
        _reason_locator = (By.CSS_SELECTOR, "[data-label='Reason'] > span")
        _amount_locator = (By.CLASS_NAME, "fine-amount-and-status")

        @property
        def checkbox(self):
            return self.find_element(*self._checkbox_locator)

        @property
        def date(self):
            return self.find_element(*self._date_locator)

        @property
        def details(self):
            return self.find_element(*self._details_locator)

        @property
        def reason(self):
            return self.find_element(*self._reason_locator)

        @property
        def amount(self):
            return self.find_element(*self._amount_locator)
