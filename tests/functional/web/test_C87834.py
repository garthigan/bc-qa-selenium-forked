import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.forms.create_new_form import CreateNewFormPage
from pages.web.wpadmin.v3.all_contents_page.all_forms import AllFormsPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.forms.edit_form import EditFormPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_poll_card import CreateNewPollCard
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage
from utils.image_download_helper import *
from utils.selenium_helpers import click
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage


PAGE = "bibliocommons-settings"
FORM_INFO = {
    'title': ' '.join(Text('en').words(quantity=3)),
    'description': Text('en').sentence()
}
POLL_INFO = {
    'description': Text('en').sentence(),
    'question': ' '.join(Text('en').words(quantity=3)),
    'choice_one': Text('en').word(),
    'choice_two': Text('en').word(),
    'choice_three': Text('en').word(),
}
IMAGE_TITLE = Text('en').words(quantity=1)[0]
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Poll"


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87834: Form + Poll Card - New")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87834", "TestRail")
class TestC87834:
    def test_C87834(self):

        # Log in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web,
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.base_url_web,
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        default_settings_page.check_all_display_taxonomy_links_checkboxes()
        default_settings_page.select_poll_card_style_dropdown("Display Responses")
        default_settings_page.save_changes_button_click()

        self.driver.delete_all_cookies()

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        settings_general_tab = SettingsGeneralTabPage(self.driver, configuration.system.base_url_web, page=PAGE,
                                                      tab='generic').open()
        click(settings_general_tab.disable_structured_tags)
        settings_general_tab.save_changes.click()

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Creating a new form
        new_form = CreateNewFormPage(self.driver, configuration.system.base_url_web).open()
        new_form.form_title.send_keys(FORM_INFO['title'])
        new_form.form_description.send_keys(FORM_INFO['description'])
        new_form.create_form.click()
        wait = WebDriverWait(self.driver, 6, poll_frequency=2, ignored_exceptions=IndexError)
        wait.until(lambda condition: not new_form.visibility_of_form_window)

        # Getting the ID of the created form
        all_forms_page = AllFormsPage(self.driver, configuration.system.base_url_web).open()
        all_forms_page.search_forms_input.send_keys(FORM_INFO['title'])
        all_forms_page.search_forms_button.click()
        all_forms_page.rows[0].title.text.should.match(FORM_INFO['title'])
        form_id = all_forms_page.rows[0].id.text

        # Opening the created form
        edit_form_page = EditFormPage(self.driver, configuration.system.base_url_web, form_id=form_id).open()
        edit_form_page.advanced_fields_heading.click()
        wait.until(lambda condition: edit_form_page.advanced_fields.poll.is_displayed())

        # Creating the poll in the form
        ActionChains(self.driver).drag_and_drop(edit_form_page.advanced_fields.poll, edit_form_page.body).perform()
        wait.until(lambda condition: edit_form_page.poll.poll_body.is_displayed())
        edit_form_page.hover_on_element(edit_form_page.poll.poll_body)
        edit_form_page.poll.poll_body.click()
        edit_form_page.wait.until(lambda condition: edit_form_page.poll.general_tab.is_displayed())
        edit_form_page.poll.general.description.send_keys(POLL_INFO['description'])
        edit_form_page.poll.general.poll_question.clear()
        edit_form_page.poll.general.poll_question.send_keys(POLL_INFO['question'])
        edit_form_page.poll.general.select_poll_type("Radio Buttons")
        edit_form_page.poll.general.poll_choice[0].text.clear()
        edit_form_page.poll.general.poll_choice[0].text.send_keys(POLL_INFO['choice_one'])
        edit_form_page.poll.general.poll_choice[0].check_default.click()
        edit_form_page.poll.general.poll_choice[1].text.clear()
        edit_form_page.poll.general.poll_choice[1].text.send_keys(POLL_INFO['choice_two'])
        edit_form_page.poll.general.poll_choice[2].text.clear()
        edit_form_page.poll.general.poll_choice[2].text.send_keys(POLL_INFO['choice_three'])
        edit_form_page.poll.general.required.click()
        edit_form_page.update.click()

        # Creating the poll card
        create_new_poll_card = CreateNewPollCard(self.driver, configuration.system.base_url_web, post_type='bw_poll').open()
        create_new_poll_card.select_a_poll_dropdown.click()
        create_new_poll_card.select_a_poll(POLL_INFO['question']).click()
        create_new_poll_card.card_image.click()
        create_new_poll_card.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        create_new_poll_card.select_widget_image.add_image_to_widget_button.click()
        create_new_poll_card.select_default_image_crop_views()
        create_new_poll_card.publish.click()

        # PART 2 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        # Select single card module
        page_staff.add_single_card(CONTENT_TYPE, POLL_INFO['question'])
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: page_staff.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        page_staff.page_builder.done_and_publish()
        page_staff.log_out_from_header()

        # PART 3 - Patron view
        self.driver.delete_all_cookies()

        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.false
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(POLL_INFO['question'])

        # PART 4 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty

        all_forms_page.open()
        all_forms_page.search_and_delete(FORM_INFO['title'])
        all_forms_page.rows.should.be.empty

        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(POLL_INFO['question'])
        all_cards_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_TITLE)
        media_library_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)
