from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.base import BasePage
from tenacity import*
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from utils.selenium_helpers import click
from mimesis import Text
from selenium.common.exceptions import TimeoutException
# noinspection PyUnresolvedReferences
import sure


class AdminEventPage(BasePage):

    _admin_link_locator = (By.CSS_SELECTOR, ".admin-link .primary-link")
    _new_revised_locator = (By.PARTIAL_LINK_TEXT, "New/Revised")
    _previous_page_locator = (By.CSS_SELECTOR, ".glyphicon-chevron-left")
    _create_event_button_locator = (By.CSS_SELECTOR, ".btn-info")
    _title_textbox_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control.event-edit-title")
    _type_dropdown_locator = (By.CSS_SELECTOR, "ul.chosen-choices")
    _type_dropdown_business_locator = (By.CSS_SELECTOR, "li.active-result:nth-child(5)")
    _audience_dropdown_locator = (By.CSS_SELECTOR, "Select[data-placeholder='Select Audiences'] + div")
    _audience_dropdown_families_locator = (By.CSS_SELECTOR, "[data-option-array-index='3']")
    _description_field_locator = (By.CSS_SELECTOR, ".redactor-in-0")
    _location_dropdown_locator = (By.CSS_SELECTOR, "div.controls.location-section")
    _location_dropdown_archer_height = (By.CSS_SELECTOR, "div.controls.location-section *:nth-child(4)")
    _start_date_button_locator = (By.CSS_SELECTOR, ".ember-view.form-control.start-date.picker__input")
    _calendar_next_month_button_locator = (By.CSS_SELECTOR, ".picker__nav--next")
    _calendar_start_end_date_select_locator = (By.CSS_SELECTOR, ".picker__day.picker__day--infocus")
    _end_date_button_locator = (By.CSS_SELECTOR, ".ember-view.form-control.expire-date.picker__input")
    _time_selector_locator = (By.CSS_SELECTOR, "input[class*='start-time']")
    _start_time_selector_locator = (By.CSS_SELECTOR, ".picker__list-item")
    _save_and_publish_locator = (By.CSS_SELECTOR, ".ember-view.progress-button.btn.btn-success.btn-publish")
    _event_link_text_locator = (By.CSS_SELECTOR, ".event-title .ember-view")
    _event_link_text_test_C67848_locator = (By.PARTIAL_LINK_TEXT, "test_C67848")
    _published_tab_locator = (By.PARTIAL_LINK_TEXT, "Published")
    _admin_events_search_textbox_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control.form-control-enhanced")
    _admin_event_title_locator = (By.CSS_SELECTOR, ".admin-summary-title .ember-view")
    _admin_delete_buttons_locator = (By.CSS_SELECTOR, "[role='row'] :nth-of-type(2) .events-admin-action:nth-of-type(7) .primary-link")
    _confirm_delete_button_locator = (By.CSS_SELECTOR, ".progress-button")
    _close_delete_overlay_button_locator = (By.CSS_SELECTOR, "span.glyphicon.glyphicon-remove-2")
    _no_results_displayed_text_locator = (By.CSS_SELECTOR, "div.well.well-lg.no-results")
    _left_chevron_admin_events_page_button_locator = (By.CSS_SELECTOR, ".glyphicon-chevron-left")
    _events_location_show_locations_with_filter_button_locator = (By.CSS_SELECTOR, "[data-bindattr-654]")
    _event_creation_page_this_event_happens_dropdown_locator = (By.CSS_SELECTOR, ".ember-view.ember-select.cp-styled-selector.cp-generator-selector")
    _event_creation_page_this_event_happens_daily_locator = (By.CSS_SELECTOR, "[value='daily']")
    _add_another_date_date_time_pattern_button_locator = (By.CSS_SELECTOR, ".add-pattern-btn")
    _delete_this_and_subsequent_locator = (By.CSS_SELECTOR, "[role='row']:nth-of-type(2) .events-admin-action:nth-of-type(7) [role='presentation']:nth-of-type(3) .ember-view")
    _search_progress_spinner_locator = (By.CSS_SELECTOR, ".progress-spinner")
    _exclusion_buttons_locator = (By.CSS_SELECTOR, "button.btn-exclusions")
    _exclusion_list_items_locator = (By.CSS_SELECTOR, ".exclusion-checkbox")
    _exclusion_done_button_locator = (By.CSS_SELECTOR, "[class='btn btn-default']")
    _date_and_time_published_tab_locator = (By.CSS_SELECTOR, "dd")
    _registration_required_radio_button_locator = (By.CSS_SELECTOR, "[name='registration_provider']")
    _event_capacity_text_box_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control.event-registration-cap")
    _offline_registration_instructions_text_box_locator = (By.CSS_SELECTOR, "[rows='5']")
    _staff_notes_text_box_locator = (By.CSS_SELECTOR, ".redactor-styles.redactor-in.redactor-in-1")
    _contact_text_box_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-field.form-control")
    _featured_and_full_toggle_locator = (By.CSS_SELECTOR, ".ember-view.ember-checkbox.ace.ace-switch.ace-switch-5.ace-switch-green")
    _this_event_happens_dropdown_locator = (By.CSS_SELECTOR,".ember-view.ember-select.cp-styled-selector.cp-generator-selector")
    _daily_dropdown_locator = (By.CSS_SELECTOR, "[value='daily']")
    _weekly_dropdown_locator = (By.CSS_SELECTOR, "[value='weekday']")
    _monthly_dropdown_locator = (By.CSS_SELECTOR, "[value='weekdayPerMonth']")
    _start_or_end_date_button_locator = (By.CSS_SELECTOR, "[role='gridcell']")
    _single_entry_radio_button_locator = (By.CSS_SELECTOR, ".radio.clearfix")
    _monday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='0']")
    _tuesday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='1']")
    _wednesday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='2']")
    _thursday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='3']")
    _friday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='4']")
    _saturday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='5']")
    _sunday_dropdown_locator = (By.CSS_SELECTOR, "[data-option-array-index='6']")
    _on_every_dropdown_locator = (By.CSS_SELECTOR, "[value='Choose day(s) of week']")
    _on_every_dropdown_selection_locator = (By.CSS_SELECTOR, "li.active-result.ember-view")
    _location_detail_text_box_locator = (By.CSS_SELECTOR, ".ember-view.ember-text-area.form-control")
    _event_action_link_locator = (By.CSS_SELECTOR, ".events-admin-action")
    _dropdown_required_choices_locator = (By.CSS_SELECTOR, ".search-choice")
    _excluding_list_after_selection_locator = (By.CSS_SELECTOR, ".exclusion-list .ember-view")
    _calendar_icon_locator = (By.CSS_SELECTOR, ".glyphicon.glyphicon-calendar")

    @property
    def admin_link(self):
        return self.find_element(*self._admin_link_locator)

    @property
    def is_admin_link_displayed(self):
        try:
            return self.find_element(*self._admin_link_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def new_revised(self):
        return self.find_element(*self._new_revised_locator)

    @property
    def is_new_revised_is_displayed(self):
        try:
            return self.find_element(*self._new_revised_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_previous_page_icon_displayed(self):
        try:
            return self.find_element(*self._previous_page_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def create_event_button(self):
        return self.find_element(*self._create_event_button_locator)

    @property
    def title_textbox(self):
        return self.find_element(*self._title_textbox_locator)

    @property
    def is_title_textbox_displayed(self):
        try:
            return self.find_element(*self._title_textbox_locator).is_displayed()
        except NoSuchElementException:
            return False

    # this covers type, program, audience, language and date picker drop-downs
    @property
    def type_dropdown(self):
        return self.find_elements(*self._type_dropdown_locator)

    @property
    def type_dropdown_business(self):
        return self.find_element(*self._type_dropdown_business_locator)

    @property
    def is_type_dropdown_business_displayed(self):
        try:
            return self.find_element(*self._type_dropdown_business_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def audience_dropdown(self):
        return self.find_element(*self._audience_dropdown_locator)

    @property
    def is_type_audience_dropdown_displayed(self):
        try:
            return self.find_element(*self._audience_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_type_audience_dropdown_families_displayed(self):
        try:
            return self.find_element(*self._audience_dropdown_families_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def type_audience_dropdown_families(self):
        return self.find_element(*self._audience_dropdown_families_locator)

    @property
    def description_field(self):
        return self.find_element(*self._description_field_locator)

    @property
    def is_description_field_displayed(self):
        try:
            return self.find_element(*self._description_field_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def location_dropdown(self):
        return self.find_element(*self._location_dropdown_locator)

    @property
    def is_location_dropdown_displayed(self):
        return self.find_element(*self._location_dropdown_locator).is_displayed()

    @property
    def location_dropdown_archer(self):
        return self.find_element(*self._location_dropdown_archer_height)

    @property
    def is_location_dropdown_archer_height_displayed(self):
        try:
            return self.find_element(*self._location_dropdown_archer_height).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def time_selector(self):
        return self.find_element(*self._time_selector_locator)

    @property
    def is_time_selector_displayed(self):
        try:
            return self.find_element(*self._time_selector_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_time(self):
        return self.find_elements(*self._start_time_selector_locator)

    @property
    def is_save_and_publish_displayed(self):
        try:
            return self.find_element(*self._save_and_publish_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def time_selectors(self):
        return self.find_elements(*self._time_selector_locator)

    @property
    def save_and_publish(self):
        return self.find_element(*self._save_and_publish_locator)

    @property
    def event_name_last_word(self):
        return self.find_element(*self._event_link_text_locator)

    @property
    def is_event_name_last_word_displayed(self):
        try:
            return self.find_element(*self._event_link_text_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def event_name_last_words(self):
        return self.find_elements(*self._event_link_text_locator)

    @property
    def admin_published_tab(self):
        return self.find_element(*self._published_tab_locator)

    @property
    def is_admin_published_tab_displayed(self):
        return self.find_element(*self._published_tab_locator)

    @property
    def admin_events_search_textbox(self):
        return self.find_element(*self._admin_events_search_textbox_locator)

    @property
    def is_admin_events_search_textbox_displayed(self):
        try:
            return self.find_element(*self._admin_events_search_textbox_locator).is_displayed()

        except NoSuchElementException:
            return False

    @property
    def admin_event_title(self):
        return self.find_element(*self._admin_event_title_locator)

    @property
    def is_admin_event_title_displayed(self):
        try:
            return self.find_element(*self._admin_event_title_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def admin_delete_button(self):
        return self.find_element(*self._admin_delete_buttons_locator)

    @property
    def admin_delete_buttons(self):
        return self.find_elements(*self._admin_delete_buttons_locator)

    @property
    def is_admin_delete_button_displayed(self):
        try:
            return self.find_element(*self._admin_delete_buttons_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def admin_event_titles(self):
        return self.find_elements(*self._admin_event_title_locator)

    @property
    def confirm_delete_button(self):
        return self.find_element(*self._confirm_delete_button_locator)

    @property
    def is_confirm_delete_button_displayed(self):
        try:
            return self.find_element(*self._confirm_delete_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def close_delete_overlay_button(self):
        return self.find_element(*self._close_delete_overlay_button_locator)

    @property
    def is_close_delete_overlay_button_displayed(self):
        try:
            return self.find_element(*self._close_delete_overlay_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def no_results_displayed(self):
        return self.find_element(*self._no_results_displayed_text_locator).text

    def assert_is_no_results_displayed(self):
        try:
            return self.find_element(*self._no_results_displayed_text_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_left_chevron_button_displayed(self):
        try:
            return self.find_element(*self._left_chevron_admin_events_page_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def event_creation_page_this_event_happens_dropdown(self):
        return self.find_element(*self._event_creation_page_this_event_happens_dropdown_locator)

    @property
    def is_event_creation_page_this_event_happens_dropdown_displayed(self):
        try:
            return self.find_element(*self._event_creation_page_this_event_happens_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_date_button(self):
        return self.find_element(*self._start_date_button_locator)

    @property
    def is_event_start_date_button_displayed(self):
        try:
            return self.find_element(*self._start_date_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def calendar_next_month_button(self):
        return self.find_elements(*self._calendar_next_month_button_locator)

    @property
    def calendar_start_end_date_select(self):
        return self.find_elements(*self._calendar_start_end_date_select_locator)

    @property
    def end_date_button(self):
        return self.find_element(*self._end_date_button_locator)

    @property
    def is_end_date_button_displayed(self):
        try:
            return self.find_element(*self._end_date_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_another_date_date_time_pattern(self):
        return self.find_element(*self._add_another_date_date_time_pattern_button_locator)

    @property
    def is_add_another_date_date_time_pattern_displayed(self):
        try:
            return self.find_element(*self._add_another_date_date_time_pattern_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def this_event_happens_dropdown_pattern_2_find(self):
        return self.find_elements(*self._event_creation_page_this_event_happens_dropdown_locator)

    @property
    def event_creation_page_this_event_happens_dropdown_pattern_2(self):
        return self.this_event_happens_dropdown_pattern_2_find[1]

    @property
    def is_event_creation_page_this_event_happens_dropdown_pattern_2_displayed(self):
        try:
            return self.event_creation_page_this_event_happens_dropdown_pattern_2.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_date_button_2nd_find(self):
        return self.find_elements(*self._start_date_button_locator)

    @property
    def end_date_button_2nd_find(self):
        return self.find_elements(*self._end_date_button_locator)

    @property
    def delete_this_and_subsequent(self):
        return self.find_element(*self._delete_this_and_subsequent_locator)

    @property
    def exclusion_buttons(self):
        return self.find_element(*self._exclusion_buttons_locator)

    @property
    def is_exclusion_buttons_displayed(self):
        try:
            return self.exclusion_buttons.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def exclusion_buttons_find(self):
        return self.find_elements(*self._exclusion_list_items_locator)

    @property
    def exclusion_done_button(self):
        return self.find_element(*self._exclusion_done_button_locator)

    @property
    def is_exclusion_done_button_displayed(self):
        try:
            return self.find_element(*self._exclusion_done_button_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def excluding_date_list(self):
        return self.find_elements(*self._excluding_list_after_selection_locator)

    @property
    def search_progress_spinner(self):
        return self.find_element(*self._search_progress_spinner_locator)

    @property
    def is_search_progress_spinner_displayed(self):
        try:
            return self.search_progress_spinner.is_displayed()
        except NoSuchElementException:
            return False

    @property
    def date_and_time_published_tab(self):
        return self.find_elements(*self._date_and_time_published_tab_locator)

    @property
    def registration_required_radio_buttons(self):
        return self.find_elements(*self._registration_required_radio_button_locator)

    @property
    def event_capacity_text_box(self):
        return self.find_element(*self._event_capacity_text_box_locator)

    @property
    def offline_registration_instruction_text_box(self):
        return self.find_element(*self._offline_registration_instructions_text_box_locator)

    @property
    def is_offline_registration_instruction_text_box_displayed(self):
        try:
            return self.find_element(*self._offline_registration_instructions_text_box_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def staff_notes_text_box(self):
        return self.find_element(*self._staff_notes_text_box_locator)

    @property
    def contact_text_box(self):
        return self.find_elements(*self._contact_text_box_locator)

    @property
    def featured_and_offline_full_toggle(self):
        return self.find_elements(*self._featured_and_full_toggle_locator)

    @property
    def this_event_happens_dropdown(self):
        return self.find_element(*self._this_event_happens_dropdown_locator)

    @property
    def is_this_event_happens_dropdown_displayed(self):
        try:
            return self.find_element(*self._this_event_happens_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def daily_dropdown(self):
        return self.find_element(*self._daily_dropdown_locator)

    @property
    def is_daily_dropdown_displayed(self):
        try:
            return self.find_element(*self._daily_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def weekly_dropdown(self):
        return self.find_element(*self._weekly_dropdown_locator)

    @property
    def is_weekly_dropdown_displayed(self):
        try:
            return self.find_element(*self._weekly_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def monthly_dropdown(self):
        return self.find_element(*self._monthly_dropdown_locator)

    @property
    def is_monthly_dropdown_displayed(self):
        try:
            return self.find_element(*self._monthly_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def start_or_end_date_day_button(self):
        return self.find_elements(*self._start_or_end_date_button_locator)

    @property
    def single_entry_radio_button(self):
        return self.find_elements(*self._single_entry_radio_button_locator)

    @property
    def monday_dropdown(self):
        return self.find_elements(*self._monday_dropdown_locator)

    @property
    def tuesday_dropdown(self):
        return self.find_element(*self._tuesday_dropdown_locator)

    @property
    def is_tuesday_dropdown_displayed(self):
        try:
            return self.find_element(*self._tuesday_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def on_every_dropdown(self):
        return self.find_element(*self._on_every_dropdown_locator)

    @property
    def on_every_dropdown_selection(self):
        return self.find_elements(*self._on_every_dropdown_selection_locator)

    @property
    def location_details_text_box(self):
        return self.find_elements(*self._location_detail_text_box_locator)

    @retry(stop=stop_after_attempt(10), wait=wait_fixed(1))
    def retry_search_until_results_appear(self, title_name):
        self.admin_events_search_textbox.clear()
        self.admin_events_search_textbox.send_keys(title_name)
        self.admin_events_search_textbox.clear()
        self.admin_events_search_textbox.send_keys(f"{title_name} ")
        self.admin_events_search_textbox.clear()
        self.admin_events_search_textbox.send_keys(title_name)
        events_name = self.admin_event_title.text
        self.wait.until(lambda s: events_name == title_name)
        if events_name != title_name:
            raise Exception

    _globe_icon_selector = (By.CSS_SELECTOR, "glyphicon glyphicon-globe")

    @property
    def globe_icon(self):
        return self.find_element(*self._globe_icon_selector)

    @property
    def is_globe_icon_displayed(self):
        try:
            return self.find_element(*self._globe_icon_selector).is_displayed()
        except NoSuchElementException:
            return False

    # Contains: Preview, Report, Edit, Cancel, Unpublish, Featured Event, Delete, Make a Copy
    @property
    def admin_event_action_links(self):
        return self.find_elements(*self._event_action_link_locator)

    # covers type, audience, language
    @property
    def dropdown_required_choices(self):
        return self.find_elements(*self._dropdown_required_choices_locator)

    @property
    def calendar_icon(self):
        return self.find_element(*self._calendar_icon_locator)

    def create_event(self, title_name, staff_note="no"):
        # navigate to events page
        events_admin = AdminEventPage(self.driver)
        events_admin.admin_link.click()
        events_admin.wait.until(lambda s: events_admin.is_previous_page_icon_displayed)

        # Create events
        events_admin.create_event_button.click()
        events_admin.wait.until(lambda s: events_admin.is_title_textbox_displayed)

        events_admin.title_textbox.send_keys(title_name)
        try:
            click(events_admin.type_dropdown[0])
        except TimeoutException:
            click(events_admin.type_dropdown[0])
        events_admin.wait.until(lambda s: events_admin.is_type_dropdown_business_displayed)
        events_admin.type_dropdown_business.click()
        click(events_admin.audience_dropdown)
        events_admin.wait.until(lambda s: events_admin.is_type_audience_dropdown_families_displayed)
        events_admin.type_audience_dropdown_families.click()
        description = Text('en').text(quantity=3)
        events_admin.description_field.send_keys(description)
        events_admin.time_selector.click()
        events_admin.wait.until(lambda s: events_admin.start_time[80].is_displayed())
        events_admin.start_time[80].click()
        events_admin.wait.until(lambda s: events_admin.is_event_creation_page_this_event_happens_dropdown_displayed)
        events_admin.event_creation_page_this_event_happens_dropdown.click()

        # location
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_displayed)
        events_admin.location_dropdown.click()
        events_admin.wait.until(lambda s: events_admin.is_location_dropdown_archer_height_displayed)
        events_admin.location_dropdown_archer.click()

        # staff notes
        if staff_note == "yes":
            events_admin.staff_notes_text_box.send_keys(description)

    # Default is no registration
    def registration_type(self, registration, capacity=10, offline_full_toggle="no"):
        events_admin = AdminEventPage(self.driver)
        if registration == "online":
            events_admin.registration_required_radio_buttons[1].click()
            events_admin.event_capacity_text_box.send_keys(capacity)
        elif registration == "offline":
            # offline registration
            events_admin.registration_required_radio_buttons[2].click()
            events_admin.wait.until(lambda s: events_admin.is_offline_registration_instruction_text_box_displayed)
            description = Text('en').text(quantity=3)
            events_admin.offline_registration_instruction_text_box.send_keys(description)
            if offline_full_toggle == "yes":
                events_admin.featured_and_offline_full_toggle[1].click()
        else:
            events_admin.registration_required_radio_buttons[0].is_enabled().should.be.true

    def save_and_publish_with_retry(self):
        events_admin = AdminEventPage(self.driver)
        events_admin.wait.until(lambda s: events_admin.save_and_publish.is_enabled())
        click(events_admin.save_and_publish)

    def exclusions(self, event_happens):

        events_admin = AdminEventPage(self.driver)
        if event_happens == "daily":
            events_admin.wait.until(lambda s: events_admin.event_creation_page_this_event_happens_dropdown)
            my_select = Select(events_admin.event_creation_page_this_event_happens_dropdown)
            my_select.select_by_value("daily")

            events_admin.wait.until(lambda s: events_admin.is_event_start_date_button_displayed)
            events_admin.start_date_button.click()
            events_admin.wait.until(lambda s: events_admin.calendar_next_month_button[0].is_displayed())
            events_admin.calendar_next_month_button[0].click()
            events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[13].is_displayed())
            events_admin.calendar_start_end_date_select[13].click()
            events_admin.wait.until(lambda s: events_admin.is_end_date_button_displayed)
            events_admin.end_date_button.click()
            events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[46].is_displayed())
            events_admin.calendar_start_end_date_select[46].click()

            events_admin.exclusion_buttons.click()
            events_admin.wait.until(lambda s: events_admin.exclusion_buttons_find[0].is_displayed())
            events_admin.exclusion_buttons_find[0].click()
            events_admin.wait.until(lambda s: events_admin.exclusion_buttons_find[3].is_displayed())
            events_admin.exclusion_buttons_find[-1].click()
            events_admin.wait.until(lambda s: events_admin.is_exclusion_done_button_displayed)
            events_admin.exclusion_done_button.click()

        elif event_happens == "weekly":
            calendar_icon = events_admin.calendar_icon
            self.driver.execute_script('arguments[0].scrollIntoView(true);', calendar_icon)
            events_admin.wait.until(lambda s: events_admin.event_creation_page_this_event_happens_dropdown)
            my_select = Select(events_admin.event_creation_page_this_event_happens_dropdown)
            my_select.select_by_value("weekday")

            events_admin.wait.until(lambda s: events_admin.on_every_dropdown.is_displayed())
            events_admin.on_every_dropdown.click()
            events_admin.wait.until(lambda s: events_admin.on_every_dropdown_selection[0].is_displayed())
            events_admin.on_every_dropdown_selection[0].click()

            events_admin.wait.until(lambda s: events_admin.is_event_start_date_button_displayed)
            events_admin.start_date_button.click()
            events_admin.wait.until(lambda s: events_admin.calendar_next_month_button[0].is_displayed())
            events_admin.calendar_next_month_button[0].click()
            events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[13].is_displayed())
            events_admin.calendar_start_end_date_select[13].click()

            events_admin.wait.until(lambda s: events_admin.is_end_date_button_displayed)
            events_admin.end_date_button.click()
            events_admin.wait.until(lambda s: events_admin.calendar_next_month_button[1].is_displayed())
            events_admin.calendar_next_month_button[1].click()
            events_admin.wait.until(lambda s: events_admin.calendar_start_end_date_select[46].is_displayed())
            events_admin.calendar_start_end_date_select[46].click()

    def deleted_events(self, title_name):
        events_admin = AdminEventPage(self.driver)
        events_admin.wait.until(lambda s: events_admin.is_admin_published_tab_displayed)
        events_admin.admin_published_tab.click()
        events_admin.wait.until(lambda s: events_admin.is_admin_events_search_textbox_displayed)
        events_admin.retry_search_until_results_appear(title_name)

        admin_event_title = events_admin.admin_event_title
        self.driver.execute_script('arguments[0].scrollIntoView(true);', admin_event_title)
        manage_registration_button = events_admin.admin_event_action_links[4]
        ActionChains(self.driver).move_to_element(manage_registration_button).perform()
        events_admin.admin_event_action_links[4].click()
