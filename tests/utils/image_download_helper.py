import os
import urllib.request

def get_image_path_name(name, type):
    return os.getcwd() + "/" + name + type

def download_image(url, imagePath):
    urllib.request.urlretrieve(url, imagePath)

def delete_downloaded_image(imagePath):
    os.remove(imagePath)
