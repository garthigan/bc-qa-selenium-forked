from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pypom import Region
from selenium.common.exceptions import NoSuchElementException
from utils.selenium_helpers import click


class PageBuilderHeader(Region):

    _page_builder_header_bar_locator = (By.CSS_SELECTOR, "div.fl-builder-bar")

    _done_locator = (By.CSS_SELECTOR, "button.fl-builder-done-button.fl-builder-button")
    _discard_locator = (By.CSS_SELECTOR, "span[data-action='discard']")
    _save_draft_locator = (By.CSS_SELECTOR, "span[data-action='draft']")
    _publish_locator = (By.CSS_SELECTOR, "span[data-action='publish']")
    _cancel_locator = (By.CSS_SELECTOR, "span[data-action='dismiss']")
    _body_locator = (By.CSS_SELECTOR, "div.entry-content")
    _add_content_locator = (By.CSS_SELECTOR, "button.fl-builder-content-panel-button.fl-builder-button")
    _modules_locator = (By.CSS_SELECTOR, "button[data-tab='modules']")
    _rows_locator = (By.CSS_SELECTOR, "button[data-tab='rows']")

    @property
    def page_builder_header_bar(self):
        return self.find_element(*self._page_builder_header_bar_locator)

    @property
    def is_page_builder_header_bar_displayed(self):
        try:
            return self.find_element(*self._page_builder_header_bar_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def add_content(self):
        return self.find_element(*self._add_content_locator)

    @property
    def done(self):
        return self.find_element(*self._done_locator)

    @property
    def body(self):
        return self.find_element(*self._body_locator)

    @property
    def discard(self):
        return self.find_element(*self._discard_locator)

    @property
    def save_draft(self):
        return self.find_element(*self._save_draft_locator)

    @property
    def publish(self):
        return self.find_element(*self._publish_locator)

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)

    def accept_alert(self):
        alert = self.wait.until(EC.alert_is_present())
        alert.accept()

    def done_and_publish(self):
        click(self.done)
        self.wait.until(lambda condition: self.publish.is_displayed())
        click(self.publish)


class BuilderPanel(Region):

    _heading_locator = (By.CSS_SELECTOR, "div.fl-builder--panel-content")

    # ------Modules-----
    _modules_tab = (By.CSS_SELECTOR, "button[data-tab='modules']")

    # Cards
    _single_card_locator = (By.CSS_SELECTOR, "span[data-type='SingleCard']")
    _masonry_card_collection_locator = (By.CSS_SELECTOR, "span[data-type='MasonryCardCollection']")
    _card_collection_locator = (By.CSS_SELECTOR, "span[data-type='CardCollection']")
    _structured_card_collection_locator = (By.CSS_SELECTOR, "span[data-type='StructuredCardCollection']")
    _tiled_card_collection_locator = (By.CSS_SELECTOR, "span[data-type='TiledCard']")
    _horizontal_card_locator = (By.CSS_SELECTOR, "span[data-type='HorizontalCard']")

    # Blocks
    _hero_slider_locator = (By.CSS_SELECTOR, "span[data-type='HeroSlider']")
    _card_slider_locator = (By.CSS_SELECTOR, "span[data-type='CardSlider']")
    _banner_locator = (By.CSS_SELECTOR, "span[data-type='Banner']")
    _biblioevents_widget_locator = (By.CSS_SELECTOR, "span[data-type='EventsWidget']")
    _faq_locator = (By.CSS_SELECTOR, "span[data-type='Faq']")

    # Carousels
    _catalog_carousel_locator = (By.CSS_SELECTOR, "span[data-type='CatalogTitleCarousel']")
    _comments_carousel_locator = (By.CSS_SELECTOR, "span[data-type='CommentsCarousel']")
    _tabbed_catalog_carousel_locator = (By.CSS_SELECTOR, "span[data-type='TabbedCatalogTitleCarousel']")
    _quotes_carousel_locator = (By.CSS_SELECTOR, "span[data-type='QuotesCarousel']")

    # Basic Modules
    _text_editor_locator = (By.CSS_SELECTOR, "span[data-type='rich-text']")
    _video_locator = (By.CSS_SELECTOR, "span[data-type='video']")
    _modules_heading_locator = (By.CSS_SELECTOR, "span[data-type='heading']")
    _separator_locator = (By.CSS_SELECTOR, "span[data-type='separator']")
    _photo_locator = (By.CSS_SELECTOR, "span[data-type='photo']")
    _audio_locator = (By.CSS_SELECTOR, "span[data-type='audio']")
    _single_button_locator = (By.CSS_SELECTOR, "span[data-type='SingleButton']")
    _button_group_locator = (By.CSS_SELECTOR, "span[data-type='ButtonGroup']")

    # More
    _map_locator = (By.CSS_SELECTOR, "span[data-type='map']")
    _sidebar_menu_locator = (By.CSS_SELECTOR, "span[data-type='SidebarMenu']")
    _gallery_locator = (By.CSS_SELECTOR, "span[data-type='gallery']")
    _accordion_locator = (By.CSS_SELECTOR, "span[data-type='accordion']")
    _tabs_locator = (By.CSS_SELECTOR, "span[data-type='tabs']")

    # ------Rows-----
    _rows_tab = (By.CSS_SELECTOR, "button[data-tab='rows']")

    # Rows
    _1_column_locator = (By.CSS_SELECTOR, "span[data-cols='1-col']")
    _2_columns_locator = (By.CSS_SELECTOR, "span[data-cols='2-cols']")
    _3_columns_locator = (By.CSS_SELECTOR, "span[data-cols='3-cols']")
    _4_columns_locator = (By.CSS_SELECTOR, "span[data-cols='4-cols']")
    _5_columns_locator = (By.CSS_SELECTOR, "span[data-cols='5-cols']")
    _6_columns_locator = (By.CSS_SELECTOR, "span[data-cols='6-cols']")
    _left_sidebar_locator = (By.CSS_SELECTOR, "span[data-cols='left-sidebar']")
    _right_sidebar_locator = (By.CSS_SELECTOR, "span[data-cols='right-sidebar']")
    _left_and_right_sidebar_locator = (By.CSS_SELECTOR, "span[data-cols='left-right-sidebar']")

    @property
    def is_panel_visible(self):
        try:
            self.wait.until(EC.visibility_of_element_located(self._heading_locator))
            panel = self.find_element(*self._heading_locator)
            if panel is None:
                return False
            else:
                return True
        except Exception:
            return False

    @property
    def modules_tab(self):
        return self.find_element(*self._modules_tab)

    @property
    def single_card(self):
        self.wait.until(EC.visibility_of_element_located(self._single_card_locator))
        return self.find_element(*self._single_card_locator)

    @property
    def masonry_card_collection(self):
        return self.find_element(*self._masonry_card_collection_locator)

    @property
    def card_collection(self):
        return self.find_element(*self._card_collection_locator)

    @property
    def structured_card_collection(self):
        return self.find_element(*self._structured_card_collection_locator)

    @property
    def tiled_card(self):
        return self.find_element(*self._tiled_card_collection_locator)

    @property
    def horizontal_card(self):
        return self.find_element(*self._horizontal_card_locator)

    @property
    def hero_slider(self):
        return self.find_element(*self._hero_slider_locator)

    @property
    def card_slider(self):
        return self.find_element(*self._card_slider_locator)

    @property
    def banner(self):
        return self.find_element(*self._banner_locator)

    @property
    def biblioevents_widget(self):
        return self.find_element(*self._biblioevents_widget_locator)

    @property
    def faq(self):
        return self.find_element(*self._faq_locator)

    @property
    def catalog_carousel(self):
        return self.find_element(*self._catalog_carousel_locator)

    @property
    def comments_carousel(self):
        return self.find_element(*self._comments_carousel_locator)

    @property
    def tab_catalog_carousel(self):
        return self.find_element(*self._tabbed_catalog_carousel_locator)

    @property
    def quotes_carousel(self):
        return self.find_element(*self._quotes_carousel_locator)

    @property
    def text_editor(self):
        return self.find_element(*self._text_editor_locator)

    @property
    def video(self):
        return self.find_element(*self._video_locator)

    @property
    def heading(self):
        return self.find_element(*self._modules_heading_locator)

    @property
    def separator(self):
        return self.find_element(*self._separator_locator)

    @property
    def photo(self):
        return self.find_element(*self._photo_locator)

    @property
    def audio(self):
        return self.find_element(*self._audio_locator)

    @property
    def single_button(self):
        return self.find_element(*self._single_button_locator)

    @property
    def button_group(self):
        return self.find_element(*self._button_group_locator)

    @property
    def map(self):
        return self.find_element(*self._map_locator)

    @property
    def sidebar_menu(self):
        return self.find_element(*self._sidebar_menu_locator)

    @property
    def gallery(self):
        return self.find_element(*self._gallery_locator)

    @property
    def accordion(self):
        return self.find_element(*self._accordion_locator)

    @property
    def tabs(self):
        return self.find_element(*self._tabs_locator)

    @property
    def rows_tab(self):
        return self.find_element(*self._rows_tab)

    @property
    def one_column(self):
        return self.find_element(*self._1_column_locator)

    @property
    def two_columns(self):
        return self.find_element(*self._2_columns_locator)

    @property
    def three_columns(self):
        return self.find_element(*self._3_columns_locator)

    @property
    def four_columns(self):
        return self.find_element(*self._4_columns_locator)

    @property
    def five_columns(self):
        return self.find_element(*self._5_columns_locator)

    @property
    def six_columns(self):
        return self.find_element(*self._6_columns_locator)

    @property
    def left_sidebar(self):
        return self.find_element(*self._left_sidebar_locator)

    @property
    def right_sidebar(self):
        return self.find_element(*self._right_sidebar_locator)

    @property
    def left_and_right_sidebar(self):
        return self.find_element(*self._left_and_right_sidebar_locator)

    def scroll_to_bottom(self):
        self.wait.until(EC.presence_of_element_located(self._sidebar_menu_locator))
        self.driver.execute_script("return arguments[0].scrollIntoView(true);", self.find_element(*self._sidebar_menu_locator))
