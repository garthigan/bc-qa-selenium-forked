import os
import sys
from datetime import datetime
from datetime import timedelta
sys.path.append('tests')
import FuelSDK

# Change the value below to the number of days in the past from which you would like to retrieve the results
DAYS = 0

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    # Modify the date below to reduce the number of results returned from the request
    # Setting this too far in the past could result in a very large response size
    delta = timedelta(days=DAYS)
    retrieve_date = str(datetime.now() - delta)
    
    print('>>> Retrieve Filtered UnsubEvents with GetMoreResults')
    get_unsub_event = FuelSDK.ET_UnsubEvent()
    get_unsub_event.auth_stub = stubObj
    get_unsub_event.props = ["SendID", "SubscriberKey", "EventDate", "Client.ID", "EventType", "BatchID", "TriggeredSendDefinitionObjectID", "PartnerKey"]
    get_unsub_event.search_filter = {'Property': 'EventDate', 'SimpleOperator': 'greaterThan', 'DateValue': retrieve_date}
    get_response = get_unsub_event.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    print('results: ' + str(get_response.results))
    # Since this could potentially return a large number of results, we do not want to print the results
    # print 'Results: ' + str(get_response.results)
    
    while get_response.more_results:
        print('>>> Continue Retrieve Filtered UnsubEvents with GetMoreResults')
        get_response = get_unsub_event.getMoreResults()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        print('Results Length: ' + str(len(get_response.results)))
        print('results: ' + str(get_response.results))
    
    #  The following request could potentially bring back large amounts of data if run against a production account
    '''
    print('>>> Retrieve All UnsubEvents with GetMoreResults')
    get_unsub_event = FuelSDK.ET_UnsubEvent()
    get_unsub_event.auth_stub = stubObj
    get_unsub_event.props = ["SendID", "SubscriberKey", "EventDate", "Client.ID", "EventType", "BatchID", "TriggeredSendDefinitionObjectID", "PartnerKey"]
    get_response = get_unsub_event.get()
    print('Retrieve Status: ' + str(get_response.status))
    print('Code: ' + str(get_response.code))
    print('Message: ' + str(get_response.message))
    print('MoreResults: ' + str(get_response.more_results))
    print('RequestID: ' + str(get_response.request_id))
    print('Results Length: ' + str(len(get_response.results)))
    # Since this could potentially return a large number of results, we do not want to print the results
    #print 'Results: ' + str(get_response.results)
    
    while get_response.more_results:
        print('>>> Continue Retrieve All UnsubEvents with GetMoreResults')
        get_response = get_unsub_event.getMoreResults()
        print('Retrieve Status: ' + str(get_response.status))
        print('Code: ' + str(get_response.code))
        print('Message: ' + str(get_response.message))
        print('MoreResults: ' + str(get_response.more_results))
        print('RequestID: ' + str(get_response.request_id))
        print('Results Length: ' + str(len(get_response.results)))
    '''

except Exception as e:
    print('Caught exception: ' + str(e))
