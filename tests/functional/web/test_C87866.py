import pytest
import allure
import sure
import configuration.user
import configuration.system
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException, StaleElementReferenceException
from selenium.webdriver.support.ui import WebDriverWait
from mimesis import Text, Internet
from utils.image_download_helper import *
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.page_builder import PageBuilderPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderSingleCard, SideBarMenu, RowModules
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from selenium.webdriver.common.keys import Keys
from utils.selenium_helpers import click

PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CUSTOM_CARD_INFO = {
    'title': ''.join(Text('en').words(quantity=3)),
    'url': Internet('en').home_page(),
    'description': ' '.join(Text('en').words(quantity=6))
}
IMAGE_TITLE = Text('en').word()
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")


@pytest.mark.usefixtures('selenium_setup_and_teardown')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@allure.title("C87866: PB - Add rows")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87866", "TestRail")
class TestC87866:
    def test_C87866(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as Implementing
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page="bibliocommons-settings", tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2EdPMqG", IMAGE_PATH)

        # Logging as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        # Creating a new custom card
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web,
                                              post_type='bw_custom_card').open()
        new_custom_card.card_title.send_keys(CUSTOM_CARD_INFO['title'])
        new_custom_card.card_image.click()
        new_custom_card.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        click(new_custom_card.select_widget_image.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 20, poll_frequency=2, ignored_exceptions=[NoSuchElementException, IndexError, StaleElementReferenceException, ElementNotInteractableException])
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        new_custom_card.image_cropper.image_one_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.next.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_visible)
        new_custom_card.image_cropper.image_two_crop()
        new_custom_card.image_cropper.crop_image.click()
        new_custom_card.image_cropper.done.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_cropper_modal_visible == False)
        new_custom_card.image_cropper.is_crop_one_preview_visible.should.be.true
        new_custom_card.image_cropper.is_crop_two_preview_visible.should.be.true
        new_custom_card.card_url.send_keys(CUSTOM_CARD_INFO['url'])
        new_custom_card.card_description.send_keys(CUSTOM_CARD_INFO['description'])
        new_custom_card.select_resource_type("Catalog Search")
        new_custom_card.scroll_to_top()
        new_custom_card.publish.click()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)
        self.driver.refresh()
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: new_custom_card.image_cropper.is_crop_two_preview_visible)

        delete_downloaded_image(IMAGE_PATH)

        # Create a New Page
        create_new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        create_new_page.title.send_keys(PAGE_TITLE)
        create_new_page.page_builder_section.click()
        click(create_new_page.publish)
        create_new_page.wait.until(lambda condition: self.driver.page_source.should.contain("Update"))

        # Opening the new page in the frontend
        new_page = PageBuilderPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        new_page.wpheader.page_builder.click()
        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.rows_tab.click()

        # Adding a right sidebar to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.right_sidebar,
                                                new_page.page_builder.body).perform()

        right_sidebar = RowModules(new_page)
        wait.until(lambda condition: right_sidebar.column_body(0).is_displayed())
        ActionChains(self.driver).move_to_element(right_sidebar.column_body(0)).click().perform()
        wait.until(lambda condition: right_sidebar.column_settings.styles_tab.is_displayed())
        right_sidebar.column_settings.styles_tab.click()
        right_sidebar.column_settings.styles_tab_contents.select_equalize_heights("Yes")
        right_sidebar.column_settings.save.click()

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.rows_tab.click()

        # Adding a 4 columns module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.four_columns,
                                                right_sidebar.column_body(0)).perform()

        four_columns = RowModules(new_page)
        wait.until(lambda condition: four_columns.column_body(1).is_displayed())
        wait.until(lambda condition: four_columns.column_body(2).is_displayed())
        wait.until(lambda condition: four_columns.column_body(3).is_displayed())
        wait.until(lambda condition: four_columns.column_body(4).is_displayed())
        ActionChains(self.driver).move_to_element(four_columns.column_body(1)).click().perform()
        wait.until(lambda condition: four_columns.column_settings.styles_tab.is_displayed())
        four_columns.column_settings.styles_tab.click()
        four_columns.column_settings.styles_tab_contents.select_equalize_heights("Yes")
        four_columns.column_settings.save.click()

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a single card module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card,
                                                four_columns.column_body(1)).perform()

        # Selecting the custom card that was created
        single_card = PageBuilderSingleCard(new_page)
        wait.until(lambda condition: single_card.module_heading.is_displayed())
        single_card.styles_tab.click()
        wait.until(lambda condition: single_card.border_style.is_displayed())
        single_card.select_border_style("Show Border")
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card 1")
        if single_card.show_visually.get_attribute("checked") == "true":
            single_card.show_visually.click()
        single_card.edit_content_link.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card_by_title(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: new_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(1)

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a single card module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card,
                                                four_columns.column_body(2)).perform()

        # Selecting the custom card that was created
        single_card = PageBuilderSingleCard(new_page)
        wait.until(lambda condition: single_card.module_heading.is_displayed())
        single_card.styles_tab.click()
        wait.until(lambda condition: single_card.border_style.is_displayed())
        single_card.select_border_style("Show Border")
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card 2")
        if single_card.show_visually.get_attribute("checked") == "true":
            single_card.show_visually.click()
        single_card.edit_content_link.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card_by_title(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: new_page.user_facing_modules.single_cards[1].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(2)

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a single card module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card,
                                                four_columns.column_body(3)).perform()

        # Selecting the custom card that was created
        single_card = PageBuilderSingleCard(new_page)
        wait.until(lambda condition: single_card.module_heading.is_displayed())
        single_card.styles_tab.click()
        wait.until(lambda condition: single_card.border_style.is_displayed())
        single_card.select_border_style("Show Border")
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card 3")
        if single_card.show_visually.get_attribute("checked") == "true":
            single_card.show_visually.click()
        single_card.edit_content_link.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card_by_title(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: new_page.user_facing_modules.single_cards[2].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(3)

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a single card module to the page from Page Builder
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.single_card,
                                                four_columns.column_body(4)).perform()

        # Selecting the custom card that was created
        single_card = PageBuilderSingleCard(new_page)
        wait.until(lambda condition: single_card.module_heading.is_displayed())
        single_card.styles_tab.click()
        wait.until(lambda condition: single_card.border_style.is_displayed())
        single_card.select_border_style("Show Border")
        single_card.content_tab.click()
        single_card.module_heading.send_keys("Custom Card 4")
        if single_card.show_visually.get_attribute("checked") == "true":
            single_card.show_visually.click()
        single_card.edit_content_link.click()
        single_card.edit_card.general_tab.click()
        single_card.edit_card.card_type.click()
        single_card.edit_card.select_card_type("Custom Card")
        single_card.edit_card.choose_card.click()
        single_card.edit_card.select_card_by_title(CUSTOM_CARD_INFO['title']).click()
        single_card.edit_card.save.click()
        single_card.save()
        wait.until(lambda s: new_page.user_facing_modules.single_cards[3].cards[0].is_card_title_displayed)
        len(new_page.user_facing_modules.single_cards).should.equal(4)

        if new_page.builder_panel.is_panel_visible == False:
            new_page.page_builder.add_content.click()
        new_page.builder_panel.modules_tab.click()

        # Adding a sidebar menu to the page from Page Builder
        new_page.builder_panel.scroll_to_bottom()
        ActionChains(self.driver).drag_and_drop(new_page.builder_panel.sidebar_menu,
                                                four_columns.column_body(5)).perform()

        sidebar_menu = SideBarMenu(new_page)
        sidebar_menu.content_tab.click()
        sidebar_menu.content_tab_contents.select_a_menu.click()
        sidebar_menu.content_tab_contents.sidebar_menu_results(0).click()
        sidebar_menu.save.click()
        wait.until(lambda condition: len(new_page.user_facing_modules.single_cards) == 4)
        wait.until(lambda condition: len(new_page.user_facing_modules.sidebar_menus) == 1)
        new_page.page_builder.done.click()
        new_page.wait.until(lambda condition: new_page.page_builder.publish.is_displayed())
        new_page.page_builder.publish.click()
        wait.until(lambda condition: len(new_page.user_facing_modules.single_cards) == 4)
        wait.until(lambda condition: len(new_page.user_facing_modules.sidebar_menus) == 1)
        for sidebar_link in new_page.user_facing_modules.sidebar_menus[0].sidebar_links:
            wait.until(lambda condition: sidebar_link.is_displayed())
        new_page.open()
        wait.until(lambda condition: len(new_page.user_facing_modules.single_cards) == 4)
        wait.until(lambda condition: len(new_page.user_facing_modules.sidebar_menus) == 1)
        for sidebar_link in new_page.user_facing_modules.sidebar_menus[0].sidebar_links:
            wait.until(lambda condition: sidebar_link.is_displayed())

        # Asserting that the borders line up (checking that the height of each module is equal)
        # Commenting this out now as there is a bug that exists that makes the below assertions not work. After the bug is fixed, will uncomment the code and ensure it works.
        # new_page.user_facing_modules.single_cards[0].cards[0].get_attribute("clientHeight").should.equal(new_page.user_facing_modules.single_cards[1].cards[0].get_attribute("clientHeight"))
        # new_page.user_facing_modules.single_cards[1].cards[0].get_attribute("clientHeight").should.equal(new_page.user_facing_modules.single_cards[2].cards[0].get_attribute("clientHeight"))
        # new_page.user_facing_modules.single_cards[2].cards[0].get_attribute("clientHeight").should.equal(new_page.user_facing_modules.single_cards[3].cards[0].get_attribute("clientHeight"))
        # new_page.user_facing_modules.single_cards[3].cards[0].get_attribute("clientHeight").should.equal(new_page.user_facing_modules.single_cards[4].cards[0].get_attribute("clientHeight"))
        # new_page.user_facing_modules.single_cards[4].cards[0].get_attribute("clientHeight").should.equal(new_page.user_facing_modules.sidebar_menus[0].get_attribute("clientHeight"))

        # Deleting the created custom card
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_input.send_keys(CUSTOM_CARD_INFO['title'])
        all_cards_page.search_button.click()
        all_cards_page.rows[0].title.get_attribute("textContent").should.match(CUSTOM_CARD_INFO['title'])
        all_cards_page.rows[0].hover_on_title()
        all_cards_page.rows[0].delete.click()
        all_cards_page.rows.should.be.empty

        # Deleting the created page
        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_input.send_keys(PAGE_TITLE)
        all_pages_page.search_button.click()
        all_pages_page.rows[0].title.get_attribute("textContent").should.match(PAGE_TITLE)
        all_pages_page.rows[0].hover_on_title()
        all_pages_page.rows[0].delete.click()
        all_pages_page.rows.should.be.empty

        # Deleting the uploaded images from the media library
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: len(media_library_page.rows) == 0)
