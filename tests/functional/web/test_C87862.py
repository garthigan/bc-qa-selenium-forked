import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.dashboard import DashboardPage
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.explore import ExplorePagePage
from pages.web.page_builder import PageBuilderPage
from pages.web.user import UserPage


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87862: Launch PB 1 - Through PB button")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87862", "TestRail")
class TestC87862:
    def test_C87862(self):
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        dashboard_page = DashboardPage(self.driver)
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.page_heading.text.should.equal("Dashboard")
        dashboard_page.content_blocks[1].create_new_button_content_page_custom.click()

        # Creating new page
        create_new_page = CreateNewPagePage(self.driver)
        create_new_page.wait.until(lambda s: create_new_page.is_heading_displayed)
        create_new_page.heading.text.should.equal("Create New Page")
        page_title = '-'.join(Text('en').words(quantity=3))  # Converting list to a string
        create_new_page.title.send_keys(page_title)
        create_new_page.publish.click()
        create_new_page.wait.until(lambda s: create_new_page.is_success_message_displayed)
        create_new_page.is_success_message_displayed.should.be.true
        create_new_page.launch_page_builder.click()

        # Page Builder
        page_builder_page = PageBuilderPage(self.driver)
        page_builder_page.system_message.all_messages_displayed.should.be.true
        page_builder_page.page_builder.done.click()
        page_builder_page.wait.until(lambda condition: page_builder_page.page_builder.publish.is_displayed())
        page_builder_page.page_builder.publish.click()
        self.driver.delete_all_cookies()

        # As patron
        user_page = UserPage(self.driver, configuration.system.base_url_web + page_title).open()
        user_page.is_error_title_displayed.should.be.false
        user_page.system_message.all_messages_displayed.should.be.true

        # Explore Page
        explore_page = ExplorePagePage(self.driver, configuration.system.base_url_web + "explore").open()
        explore_page.is_explore_heading_displayed()
        explore_page.system_message.all_messages_displayed.should.be.true
