import pytest
import allure
import sure
import configuration.user
import configuration.system
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.create_new_content.create_new_blog_post import CreateNewBlogPostPage
from pages.web.wpadmin.v3.create_new_content.create_new_news_post import CreateNewNewsPostPage
from pages.web.wpadmin.v3.create_a_new_card.create_new_list_card import CreateNewListCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_catalog_comment_card import CreateNewCatalogCommentCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_event_card import CreateNewEventCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_online_resource_card import CreateNewOnlineResourceCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_poll_card import CreateNewPollCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_twitter_card import CreateNewTwitterCard
from pages.web.wpadmin.v3.create_a_new_card.create_new_custom_card import CreateNewCustomCard
from pages.web.wpadmin.v3.create_new_featured_block.create_new_banner import CreateNewBannerPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_hero_slide import CreateNewHeroSlidePage
from utils.selenium_helpers import click
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage

PAGE = 'bibliocommons-settings'
TAGS = ['test 1', 'test 2']


@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):
    driver = request.cls.driver

    # Log in as Network Admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                      configuration.user.user['web']['local']['admin']['password'])
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, configuration.system.base_url_web,
                                                                 page=PAGE, tab='system').open()
    settings_system_settings_tab.v3_status_enabled.click()
    settings_system_settings_tab.save_changes.click()

    settings_general_tab = SettingsGeneralTabPage(driver, configuration.system.base_url_web, page=PAGE,
                                                  tab='generic').open()
    click(settings_general_tab.disable_structured_tags)
    settings_general_tab.save_changes.click()

    default_settings_page = DefaultSettingsPageBuilderPage(driver, configuration.system.base_url_web,
                                                           post_type='fl-builder-template',
                                                           page='default-page-builder-settings').open()
    default_settings_page.check_all_display_taxonomy_links_checkboxes()
    default_settings_page.save_changes_button_click()

    driver.delete_all_cookies()

    # Logging in as lib admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                      configuration.user.user['web']['local']['libadmin']['password'])


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('login_and_setup')
@allure.title("C87829: Unstructured Tags taxonomy")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87829", "TestRail")
class TestC87829:
    def test_C87829_1(self):
        new_blog_post = CreateNewBlogPostPage(self.driver, configuration.system.base_url_web, post_type='post').open()

        new_blog_post.is_tags_taxonomy_displayed.should.be.false
        new_blog_post.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_blog_post.free_text_tags.send_keys(tag)
            new_blog_post.add_free_text_tags.click()

        new_blog_post.are_free_text_tags_added.should.be.true

    def test_C87829_2(self):
        new_news_post = CreateNewNewsPostPage(self.driver, configuration.system.base_url_web, post_type='bccms_news').open()

        new_news_post.is_tags_taxonomy_displayed.should.be.false
        new_news_post.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_news_post.free_text_tags.send_keys(tag)
            new_news_post.add_free_text_tags.click()

        new_news_post.are_free_text_tags_added.should.be.true

    def test_C87829_3(self):
        new_list_card = CreateNewListCard(self.driver, configuration.system.base_url_web, post_type='bw_list').open()

        new_list_card.is_tags_taxonomy_displayed.should.be.false
        new_list_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_list_card.free_text_tags.send_keys(tag)
            new_list_card.add_free_text_tags.click()

        new_list_card.are_free_text_tags_added.should.be.true

    def test_C87829_4(self):
        new_catalog_comment_card = CreateNewCatalogCommentCard(self.driver, configuration.system.base_url_web, post_type='bw_catalog_comment').open()

        new_catalog_comment_card.is_tags_taxonomy_displayed.should.be.false
        new_catalog_comment_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_catalog_comment_card.free_text_tags.send_keys(tag)
            new_catalog_comment_card.add_free_text_tags.click()

        new_catalog_comment_card.are_free_text_tags_added.should.be.true

    def test_C87829_5(self):
        new_event_card = CreateNewEventCard(self.driver, configuration.system.base_url_web, post_type='bw_event').open()

        new_event_card.is_tags_taxonomy_displayed.should.be.false
        new_event_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_event_card.free_text_tags.send_keys(tag)
            new_event_card.add_free_text_tags.click()

    def test_C87829_6(self):
        new_online_resource_card = CreateNewOnlineResourceCard(self.driver, configuration.system.base_url_web, post_type='bw_or_card').open()

        new_online_resource_card.is_tags_taxonomy_displayed.should.be.false
        new_online_resource_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_online_resource_card.free_text_tags.send_keys(tag)
            new_online_resource_card.add_free_text_tags.click()

        new_online_resource_card.are_free_text_tags_added.should.be.true

    def test_C87829_7(self):
        new_poll_card = CreateNewPollCard(self.driver, configuration.system.base_url_web, post_type='bw_poll').open()

        new_poll_card.is_tags_taxonomy_displayed.should.be.false
        new_poll_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_poll_card.free_text_tags.send_keys(tag)
            new_poll_card.add_free_text_tags.click()

        new_poll_card.are_free_text_tags_added.should.be.true

    def test_C87829_8(self):
        new_twitter_card = CreateNewTwitterCard(self.driver, configuration.system.base_url_web, post_type='bw_twitter').open()

        new_twitter_card.is_tags_taxonomy_displayed.should.be.false
        new_twitter_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_twitter_card.free_text_tags.send_keys(tag)
            new_twitter_card.add_free_text_tags.click()

        new_twitter_card.are_free_text_tags_added.should.be.true

    def test_C87829_9(self):
        new_custom_card = CreateNewCustomCard(self.driver, configuration.system.base_url_web, post_type='bw_custom_card').open()

        new_custom_card.is_tags_taxonomy_displayed.should.be.false
        new_custom_card.is_free_text_tags_field_displayed.should.be.true

        for tag in TAGS:
            new_custom_card.free_text_tags.send_keys(tag)
            new_custom_card.add_free_text_tags.click()

        new_custom_card.are_free_text_tags_added.should.be.true

    def test_C87829_10(self):
        new_banner = CreateNewBannerPage(self.driver, configuration.system.base_url_web, post_type='bw_banner').open()

        new_banner.is_tags_taxonomy_displayed.should.be.true
        new_banner.is_free_text_tags_field_displayed.should.be.false

    def test_C87829_11(self):
        new_hero_slide = CreateNewHeroSlidePage(self.driver, configuration.system.base_url_web, post_type='bw_hero_slide').open()

        new_hero_slide.is_tags_taxonomy_displayed.should.be.false
        new_hero_slide.is_free_text_tags_field_displayed.should.be.false
