from selenium.webdriver.common.by import By
from pypom import Region
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import ElementNotSelectableException


class Poll(Region):

    _poll_body_locator = (By.CSS_SELECTOR, "#field_1 > div.gfield_admin_icons")
    _delete_field_locator = (By.CSS_SELECTOR, "#gfield_delete_1 > i")
    _duplicate_field_locator = (By.CSS_SELECTOR, "#gfield_duplicate_1 > i")
    _expand_field_locator = (By.CSS_SELECTOR, "a.field_edit_icon.edit_icon_collapsed > i")
    _general_locator = (By.CSS_SELECTOR, "#ui-id-10")
    _appearance_locator = (By.CSS_SELECTOR, "#ui-id-11")
    _advanced_locator = (By.CSS_SELECTOR, "#ui-id-12")

    @property
    def poll_body(self):
        return self.find_element(*self._poll_body_locator)

    @property
    def delete_field(self):
        return self.find_element(*self._delete_field_locator)

    @property
    def duplicate_field(self):
        return self.find_element(*self._duplicate_field_locator)

    @property
    def general_tab(self):
        return self.find_element(*self._general_locator)

    @property
    def appearance_tab(self):
        return self.find_element(*self._appearance_locator)

    @property
    def advanced_tab(self):
        return self.find_element(*self._advanced_locator)

    def hover_on_element(self, element):
        ActionChains(self.driver).move_to_element(element).perform()

    @property
    def general(self):
        return self.General(self)

    @property
    def appearance(self):
        return self.Appearance(self)

    @property
    def advanced(self):
        return self.Advanced(self)

    class General(Region):

        _description_locator = (By.CSS_SELECTOR, "#field_description")
        _poll_question_locator = (By.CSS_SELECTOR, "#poll_question")
        _poll_type_locator = (By.CSS_SELECTOR, "select#poll_field_type")
        _poll_choice_row_locator = (By.CSS_SELECTOR, "#field_choices > li")
        _bulk_add_predefined_choices_locator = (By.CSS_SELECTOR, "#gform_tab_1 > ul > li.choices_setting.field_setting > input")
        _enable_other_choice_locator = (By.CSS_SELECTOR, "#gform_tab_1 > ul > li.other_choice_setting.field_setting > label")
        _randomize_order_locator = (By.CSS_SELECTOR, "#gform_tab_1 > ul > li.randomize_choices_setting.field_setting > label")
        _required_locator = (By.CSS_SELECTOR, "label[for='field_required']")

        @property
        def description(self):
            return self.find_element(*self._description_locator)

        @property
        def poll_question(self):
            return self.find_element(*self._poll_question_locator)

        @property
        def poll_type(self):
            return self.find_element(*self._poll_type_locator)

        def select_poll_type(self, value):
            options = []

            select = Select(self.poll_type)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def poll_choice(self):
            return [self.PollChoice(self, element) for element in self.find_elements(*self._poll_choice_row_locator)]

        class PollChoice(Region):

            _check_default_locator = (By.CSS_SELECTOR, "input.gfield_choice_radio")
            _text_locator = (By.CSS_SELECTOR, "input.field-choice-input.field-choice-text")
            _add_locator = (By.CSS_SELECTOR, "i.gficon-add")
            _remove_locator = (By.CSS_SELECTOR, "i.gficon-subtract")

            @property
            def check_default(self):
                return self.find_element(*self._check_default_locator)

            @property
            def text(self):
                return self.find_element(*self._text_locator)

            @property
            def add(self):
                return self.find_element(*self._add_locator)

            @property
            def remove(self):
                return self.find_element(*self._remove_locator)

        @property
        def bulk_add_predefined_choices(self):
            return self.find_element(*self._bulk_add_predefined_choices_locator)

        @property
        def enable_other_choice(self):
            return self.find_element(*self._enable_other_choice_locator)

        @property
        def randomize_order_of_choices(self):
            return self.find_element(*self._randomize_order_locator)

        @property
        def required(self):
            return self.find_element(*self._required_locator)

    class Appearance(Region):

        _custom_validation_message_locator = (By.CSS_SELECTOR, "#field_error_message")

        @property
        def custom_validation_message(self):
            return self.find_element(*self._custom_validation_message_locator)

    class Advanced(Region):

        _admin_field_label_locator = (By.CSS_SELECTOR, "#field_admin_label")
        _visible_locator = (By.CSS_SELECTOR, "label[for='field_visibility_visible']")
        _hidden_locator = (By.CSS_SELECTOR, "label[for='field_visibility_hidden']")
        _administrative_locator = (By.CSS_SELECTOR, "label[for='field_visibility_hidden']")
        _allow_field_to_be_populated_locator = (By.CSS_SELECTOR, "label[for='field_prepopulate']")
        _parameter_name_locator = (By.CSS_SELECTOR, "#field_input_name")

        @property
        def admin_field_label(self):
            return self.find_element(*self._admin_field_label_locator)

        @property
        def visible(self):
            return self.find_element(*self._visible_locator)

        @property
        def hidden(self):
            return self.find_element(*self._hidden_locator)

        @property
        def administrative(self):
            return self.find_element(*self._administrative_locator)

        @property
        def allow_field_to_be_populated(self):
            return self.find_element(*self._allow_field_to_be_populated_locator)

        @property
        def parameter_name(self):
            return self.find_element(*self._parameter_name_locator)
