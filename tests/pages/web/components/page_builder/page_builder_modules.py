from pages.web.components.page_builder.page_builder_modules_base import *
from selenium.webdriver.common.action_chains import ActionChains


class ModulesWithPlaceHolders(Region):
    _module_body_locator = (By.CSS_SELECTOR, "[data-key='module-placeholder']")

    @property
    def module_body(self):
        return self.find_elements(*self._module_body_locator)


class PageBuilderSingleCard(PageBuilderModuleBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='SingleCard']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False


class PageBuilderMasonryCardCollection(PageBuilderMasonryCardCollectionBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='MasonryCardCollection']")
    _masonry_card_collection_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_placeholder_displayed(self):
        try:
            return self.find_element(*self._masonry_card_collection_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class PageBuilderTiledCard(PageBuilderTiledCardBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='TiledCard']")
    _tiled_card_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_tiled_card_placeholder_displayed(self):
        try:
            return self.find_element(*self._tiled_card_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class PageBuilderHorizontalCard(PageBuilderHorizontalCardBase):
    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='HorizontalCard']")
    _horizontal_card_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_horizontal_card_placeholder_displayed(self):
        try:
            return self.find_element(*self._horizontal_card_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class PageBuilderHeroSlider(PageBuilderHeroSliderBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='HeroSlider']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False


class PageBuilderBanner(PageBuilderModuleBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='Banner']")
    _banner_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_banner_placeholder_displayed(self):
        try:
            return self.find_element(*self._banner_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class RowModules(RowModulesBase):

    _column_body_locator = (By.CSS_SELECTOR, "div.fl-col-content.fl-node-content.ui-sortable")

    def column_body(self, index):
        _ = self.find_elements(*self._column_body_locator)
        return _[index]

    def hover_on_element(self, element):
        ActionChains(self.driver).move_to_element(element).click().perform()


class SideBarMenu(SidebarMenuBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='SidebarMenu']")
    _sidebar_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")
    _sidebar_menu_name_locator = (By.CSS_SELECTOR, "nav[aria-label]")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_sidebar_placeholder_displayed(self):
        try:
            return self.find_element(*self._sidebar_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def sidebar_menu_name(self):
        return self.find_element(*self._sidebar_menu_name_locator)


class CardSlider(CardSliderBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='CardSlider']")
    _card_slider_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_placeholder_displayed(self):
        try:
            return self.find_element(*self._card_slider_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class CatalogCarousel(CarouselsBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='CatalogTitleCarousel']")
    _catalog_carousel_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_placeholder_displayed(self):
        try:
            return self.find_element(*self._catalog_carousel_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class CommentsCarousel(CommentsCarouselBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='CommentsCarousel']")
    _catalog_carousel_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_placeholder_displayed(self):
        try:
            return self.find_element(*self._catalog_carousel_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class QuotesCarousel(QuotesCarouselBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='QuotesCarousel']")
    _catalog_carousel_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_placeholder_displayed(self):
        try:
            return self.find_element(*self._catalog_carousel_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False


class TabbedCatalogCarousel(TabbedCatalogCarouselBase):

    _heading_locator = (By.CSS_SELECTOR, "form[data-form-id='TabbedCatalogTitleCarousel']")
    _catalog_carousel_placeholder_locator = (By.CSS_SELECTOR, "div.c-placeholder__inner-block")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_placeholder_displayed(self):
        try:
            return self.find_element(*self._catalog_carousel_placeholder_locator).is_displayed()
        except NoSuchElementException:
            return False
