import os
import sys
sys.path.append('tests')
import FuelSDK

try:
    debug = False
    stubObj = FuelSDK.ET_Client(False, debug,
        {
            'clientid': os.environ.get("CLIENT_ID"),
            'clientsecret': os.environ.get("CLIENT_PASSWORD"),
            'defaultwsdl': os.environ.get("DEFAULT_WSDL"),
            'soapendpoint': os.environ.get("SOAP_ENDPOINT"),
            'wsdl_file_local_loc': os.environ.get("WSDL_FILE_LOCAL_LOC")
        })
    
    name_of_attribute = "PythonSDKEmail"

    # Create Profile Attribute 
    print('>>> Create Profile Attribute')
    post_attribute = FuelSDK.ET_ProfileAttribute()
    post_attribute.auth_stub = stubObj
    post_attribute.props = {"Name": name_of_attribute, "PropertyType": "string"}
    postResponse = post_attribute.post()
    print('Post Status: ' + str(postResponse.status))
    print('Code: ' + str(postResponse.code))
    print('Message: ' + str(postResponse.message))
    print('Result Count: ' + str(len(postResponse.results)))
    print('Results: ' + str(postResponse.results))

except Exception as e:
    print('Caught exception: ' + str(e))
