from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from pages.web.staff_base import StaffBasePage
from selenium.common.exceptions import NoSuchElementException


class CreateNewPagePage(StaffBasePage):

    URL_TEMPLATE = "/wp-admin/post-new.php"

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _title_locator = (By.CSS_SELECTOR, "#title")
    _text_editor_section_locator = (By.CSS_SELECTOR, "a.fl-enable-editor")
    _page_builder_section_locator = (By.CSS_SELECTOR, "a.fl-enable-builder.fl-active")
    _launch_page_builder_locator = (By.CSS_SELECTOR, "#post-body-content > div.fl-builder-admin > div.fl-builder-admin-ui > a")

    # Publish
    _save_draft_locator = (By.CSS_SELECTOR, "input#save-post")
    _preview_locator = (By.CSS_SELECTOR, "a#post-preview")
    _publish_locator = (By.CSS_SELECTOR, "input#publish")
    _success_message_locator = (By.CSS_SELECTOR, "#message>p")

    @property
    def heading(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def is_heading_displayed(self):
        try:
            return self.find_element(*self._page_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def text_editor_section(self):
        return self.find_element(*self._text_editor_section_locator)

    def accept_alert(self):
        alert = self.wait.until(EC.alert_is_present())
        alert.accept()

    @property
    def page_builder_section(self):
        return self.find_element(*self._page_builder_section_locator)

    @property
    def launch_page_builder(self):
        return self.find_element(*self._launch_page_builder_locator)

    @property
    def save_draft(self):
        return self.find_element(*self._save_draft_locator)

    @property
    def preview(self):
        return self.find_element(*self._preview_locator)

    @property
    def publish(self):
        return self.find_element(*self._publish_locator)

    @property
    def is_success_message_displayed(self):
        try:
            return self.find_element(*self._success_message_locator).is_displayed()
        except NoSuchElementException:
            return False
