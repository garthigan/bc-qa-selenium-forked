from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pypom import Region


class Modules(Region):

    _single_card_locator = (By.CSS_SELECTOR, "[data-key='module-single-card']")
    _tiled_card_locator = (By.CSS_SELECTOR, "[data-key='module-tiled-card']")
    _card_collection_locator = (By.CSS_SELECTOR, "[data-key='module-card-collection']")
    _structured_card_collection_locator = (By.CSS_SELECTOR, "[data-key='module-structured-card-collection']")
    _masonry_card_collection_locator = (By.CSS_SELECTOR, "[data-key='module-masonry']")
    _sidebar_menu_locator = (By.CSS_SELECTOR, "div > div > aside")
    _hero_slider_locator = (By.CSS_SELECTOR, "[data-key='module-hero-slider']")
    _banner_locator = (By.CSS_SELECTOR, ".fl-module-content.fl-node-content")
    _placeholder_locator = (By.CSS_SELECTOR, "[data-key='module-placeholder']")
    _card_slider_locator = (By.CSS_SELECTOR, "[data-key='module-card-slider-slides']")
    _catalog_carousel_locator = (By.CSS_SELECTOR, "[data-key='module-catalog-carousel']")
    _quotes_carousel_locator = (By.CSS_SELECTOR, "[data-key='module-quotes'")
    _comments_carousel_locator = (By.CSS_SELECTOR, "[data-key='module-comments-carousel']")

    @property
    def single_cards(self):
        return [self.Module(self, element) for element in self.find_elements(*self._single_card_locator)]

    @property
    def tiled_cards(self):
        return [self.Module(self, element) for element in self.find_elements(*self._tiled_card_locator)]

    @property
    def card_collections(self):
        return [self.Module(self, element) for element in self.find_elements(*self._card_collection_locator)]

    @property
    def structured_card_collections(self):
        return [self.Module(self, element) for element in self.find_elements(*self._structured_card_collection_locator)]

    @property
    def masonry_card_collections(self):
        return [self.Module(self, element) for element in self.find_elements(*self._masonry_card_collection_locator)]

    @property
    def sidebar_menus(self):
        return [self.SidebarMenu(self, element) for element in self.find_elements(*self._sidebar_menu_locator)]

    @property
    def hero_sliders(self):
        return [self.HeroSlider(self, element) for element in self.find_elements(*self._hero_slider_locator)]

    @property
    def banners(self):
        return [self.Banner(self, element) for element in self.find_elements(*self._banner_locator)]

    @property
    def placeholders(self):
        return [self.Placeholder(self, element) for element in self.find_elements(*self._placeholder_locator)]

    class Placeholder(Region):

        _title_locator = (By.CSS_SELECTOR, "[data-key='placeholder-title']")
        _error_locator = (By.CSS_SELECTOR, "[data-key='placeholder-error']")

        @property
        def title(self):
            return self.find_element(*self._title_locator)

        @property
        def error(self):
            return self.find_element(*self._error_locator)

    class HeroSlider(Region):

        _previous_button_locator = (By.CSS_SELECTOR, "[data-key='slider-previous']")
        _next_button_locator = (By.CSS_SELECTOR, "[data-key='slider-next']")
        _pager_dot_locator = (By.CSS_SELECTOR, "[data-key^='slider-pager-']")
        _slide_single_locator = (By.CSS_SELECTOR, "[data-key='slider-slick'] [data-key='hero']")
        _slide_multi_locator = (By.CSS_SELECTOR, "div.slick-slide:not(.slick-cloned) [data-key='hero']")

        @property
        def is_more_than_one_slides_displayed(self):
            try:
                return self.find_element(*self._slide_multi_locator)
            except NoSuchElementException:
                return False

        @property
        def hero_slides(self):
            if self.is_more_than_one_slides_displayed:
                return [self.Slide(self,element) for element in self.find_elements(*self._slide_multi_locator)]
            else:
                return [self.Slide(self,element) for element in self.find_elements(*self._slide_single_locator)]

        @property
        def pager_dots(self):
            return self.find_elements(*self._pager_dot_locator)

        @property
        def next_button(self):
            return self.find_element(*self._next_button_locator)

        @property
        def previous_button(self):
            return self.find_element(*self._previous_button_locator)

        class Slide(Region):

            _title_locator = (By.CSS_SELECTOR, "[data-key='hero-title']")
            _url_locator = (By.CSS_SELECTOR, "a[class='o-slide__link js-slide__link']")
            _desktop_image_locator = (By.CSS_SELECTOR, "div[class='o-slide__image-inner-container o-slide__image-inner-container--hero@mobile-large-up js-slider__image-inner-container'] "
                                                       "> img")
            _large_mobile_image_locator = (By.CSS_SELECTOR, "div[class='o-slide__image-inner-container o-slide__image-inner-container--hero@mobile-large js-slider__image-inner-container'] "
                                                            "> img")
            _small_mobile_image_locator = (By.CSS_SELECTOR, "div[class='o-slide__image-inner-container o-slide__image-inner-container--hero@mobile-small js-slider__image-inner-container'] "
                                                            "> img")

            @property
            def title(self):
                return self.find_element(*self._title_locator)

            @property
            def url(self):
                return self.find_element(*self._url_locator)

            @property
            def desktop_image(self):
                return self.find_element(*self._desktop_image_locator)

            @property
            def is_desktop_image_displayed(self):
                try:
                    return self.find_element(*self._desktop_image_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def large_mobile_image(self):
                return self.find_element(*self._large_mobile_image_locator)

            @property
            def is_large_mobile_image_displayed(self):
                try:
                    return self.find_element(*self._large_mobile_image_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def small_mobile_image(self):
                return self.find_element(*self._small_mobile_image_locator)

            @property
            def is_small_mobile_image_displayed(self):
                try:
                    return self.find_element(*self._small_mobile_image_locator).is_displayed()
                except NoSuchElementException:
                    return False

    class Banner(Region):

        _desktop_image_locator = (By.CSS_SELECTOR, ".c-banner__image-wrapper > [data-key='banner-image']")
        _desktop_title_locator = (By.CSS_SELECTOR, "[data-key='banner-title']")
        _desktop_description_locator = (By.CSS_SELECTOR, "[data-key='banner-description']")
        _desktop_callout_solid_button_locator = (By.CSS_SELECTOR, ".c-banner__button--solid[data-key='banner-callout']")
        _desktop_callout_ghost_button_locator = (By.CSS_SELECTOR, ".c-banner__button--ghost[data-key='banner-callout']")
        _desktop_callout_link_locator = (By.CSS_SELECTOR, ".c-banner__link[data-key='banner-callout']")
        _mobile_title_locator = (By.CSS_SELECTOR, "[data-key='banner-title-mobile']")
        _mobile_callout_solid_button_locator = (By.CSS_SELECTOR, ".c-banner__button--solid[data-key='banner-callout-mobile']")
        _mobile_callout_ghost_button_locator = (By.CSS_SELECTOR, ".c-banner__button--ghost[data-key='banner-callout-mobile']")
        _mobile_callout_link_locator = (By.CSS_SELECTOR, ".c-banner__link[data-key='banner-callout-mobile']")
        _image_pos_top_locator = (By.CSS_SELECTOR, ".c-banner__image-wrapper--top > [data-key='banner-image']")
        _image_pos_middle_locator = (By.CSS_SELECTOR, ".c-banner__image-wrapper--middle > [data-key='banner-image']")
        _image_pos_bottom_locator = (By.CSS_SELECTOR, ".c-banner__image-wrapper--bottom > [data-key='banner-image']")

        @property
        def is_image_position_top(self):
            try:
                return self.find_element(*self._image_pos_top_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def is_image_position_middle(self):
            try:
                return self.find_element(*self._image_pos_middle_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def is_image_position_bottom(self):
            try:
                return self.find_element(*self._image_pos_bottom_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def desktop_image(self):
            return self.find_element(*self._desktop_image_locator)

        @property
        def is_desktop_image_displayed(self):
            try:
                return self.find_element(*self._desktop_image_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def desktop_title(self):
            return self.find_element(*self._desktop_title_locator)

        @property
        def is_desktop_title_displayed(self):
            try:
                return self.find_element(*self._desktop_title_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def desktop_description(self):
            return self.find_element(*self._desktop_description_locator)

        @property
        def is_desktop_description_displayed(self):
            try:
                return self.find_element(*self._desktop_description_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def desktop_callout_solid_button(self):
            return self.find_element(*self._desktop_callout_solid_button_locator)

        @property
        def is_desktop_callout_solid_button_displayed(self):
            try:
                return self.find_element(*self._desktop_callout_solid_button_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def desktop_callout_ghost_button(self):
            return self.find_element(*self._desktop_callout_ghost_button_locator)

        @property
        def is_desktop_callout_ghost_button_displayed(self):
            try:
                return self.find_element(*self._desktop_callout_ghost_button_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def desktop_callout_link(self):
            return self.find_element(*self._desktop_callout_link_locator)

        @property
        def is_desktop_callout_link_displayed(self):
            try:
                return self.find_element(*self._desktop_callout_link_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def mobile_title(self):
            return self.find_element(*self._mobile_title_locator)

        @property
        def is_mobile_title_displayed(self):
            try:
                return self.find_element(*self._mobile_title_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def mobile_callout_solid_button(self):
            return self.find_element(*self._mobile_callout_solid_button_locator)

        @property
        def is_mobile_callout_solid_button_displayed(self):
            try:
                return self.find_element(*self._mobile_callout_solid_button_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def mobile_callout_ghost_button(self):
            return self.find_element(*self._mobile_callout_ghost_button_locator)

        @property
        def is_mobile_callout_ghost_button_displayed(self):
            try:
                return self.find_element(*self._mobile_callout_ghost_button_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def mobile_callout_link(self):
            return self.find_element(*self._mobile_callout_link_locator)

        @property
        def is_mobile_callout_link_displayed(self):
            try:
                return self.find_element(*self._mobile_callout_link_locator).is_displayed()
            except NoSuchElementException:
                return False

    class SidebarMenu(Region):

        _sidebar_links_locator = (By.CSS_SELECTOR, "a.o-sidebar-menu__link")

        @property
        def sidebar_links(self):
            return self.find_elements(*self._sidebar_links_locator)

        def sidebar_link_by_index(self, index):
            sidebar_links = self.find_elements(*self._sidebar_links_locator)
            return sidebar_links[index]

    @property
    def card_sliders(self):
        return [self.CardSlider(self, element) for element in self.find_elements(*self._card_slider_locator)]

    class CardSlider(Region):

        _card_slider_heading_locator = (By.CSS_SELECTOR, "h2[data-key=''row-title]")
        _right_arrow_locator = (By.CSS_SELECTOR, "button[data-key='slider-next']")
        _left_arrow_locator = (By.CSS_SELECTOR, "button[data-key='slider-previous']")
        _slider_button_locator = (By.CSS_SELECTOR, "div > ul > li > button")
        _slide_title_locator = (By.CSS_SELECTOR, "div > div > div.slick-slide.slick-current.slick-active > div > div > a > div > div > div > h3")

        @property
        def card_slider_heading(self):
            return self.find_element(*self._card_slider_heading_locator)

        @property
        def right_arrow(self):
            right_arrow = self.find_elements(*self._right_arrow_locator)
            return right_arrow[0]

        @property
        def left_arrow(self):
            left_arrow = self.find_elements(*self._left_arrow_locator)
            return left_arrow[0]

        def card_slider_button(self, index):
            card_slider_buttons = self.find_elements(*self._slider_button_locator)
            return card_slider_buttons[index]

        @property
        def card_title(self):
            return self.find_element(*self._slide_title_locator)

    class Module(Region):

        _module_heading_locator = (By.CSS_SELECTOR, "h2[class*='c-module-heading']")
        _card_locator = (By.CSS_SELECTOR, "[data-key='card']")

        @property
        def module_heading(self):
            return self.find_element(*self._module_heading_locator)

        @property
        def is_module_heading_displayed(self):
            try:
                return self.find_element(*self._module_heading_locator).is_displayed()
            except NoSuchElementException:
                return False

        @property
        def cards(self):
            return [self.Card(self, element) for element in self.find_elements(*self._card_locator)]

        class Card(Region):

            _card_image_locator = (By.CSS_SELECTOR , "[data-key='card-image'] > img")
            _card_content_type_locator = (By.CSS_SELECTOR, "[data-key='card-content-type']")
            _card_title_locator = (By.CSS_SELECTOR, "[data-key='card-title']")
            _card_author_locator = (By.CSS_SELECTOR, "[data-key='card-author']")
            _card_description_locator = (By.CSS_SELECTOR, "[data-key='card-description']")
            _card_tag_locator = (By.CSS_SELECTOR, "[data-key='card-tag']")
            _card_bib_author_locator = (By.CSS_SELECTOR, "[data-key='card-bib-author']")
            _card_comment_author_locator = (By.CSS_SELECTOR, "[data-key='card-comment-author']")
            _card_comment_locator = (By.CSS_SELECTOR, "[data-key='card-comment']")
            _card_read_more_locator = (By.CSS_SELECTOR, "[data-key='card-read-more']")
            _card_twitter_text_locator = (By.CSS_SELECTOR, "div.o-card-twitter__text")

            @property
            def card_image(self):
                return self.find_element(*self._card_image_locator)

            @property
            def is_card_image_displayed(self):
                try:
                    return self.find_element(*self._card_image_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_content_type(self):
                return self.find_element(*self._card_content_type_locator)

            @property
            def is_card_content_type_displayed(self):
                try:
                    return self.find_element(*self._card_content_type_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_title(self):
                return self.find_element(*self._card_title_locator)

            @property
            def is_card_title_displayed(self):
                try:
                    return self.find_element(*self._card_title_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_author(self):
                return self.find_element(*self._card_author_locator)

            @property
            def is_card_author_displayed(self):
                try:
                    return self.find_element(*self._card_author_locator).is_displayed()
                except NoSuchElementException:
                    return False

            @property
            def card_description(self):
                return self.find_element(*self._card_description_locator)

            @property
            def is_card_description_displayed(self):
                try:
                    return self.find_element(*self._card_description_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_bib_author(self):
                return self.find_element(*self._card_bib_author_locator)

            @property
            def is_card_bib_author_displayed(self):
                try:
                    return self.find_element(*self._card_bib_author_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_comment_author(self):
                return self.find_element(*self._card_comment_author_locator)

            @property
            def is_card_comment_author_displayed(self):
                try:
                    return self.find_element(*self._card_comment_author_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_comment(self):
                return self.find_element(*self._card_comment_locator)

            @property
            def is_card_comment_displayed(self):
                try:
                    return self.find_element(*self._card_comment_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_read_more(self):
                return self.find_element(*self._card_read_more_locator)

            @property
            def is_card_read_more_displayed(self):
                try:
                    return self.find_element(*self._card_read_more_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_twitter_text(self):
                return self.find_element(*self._card_twitter_text_locator)

            @property
            def is_card_twitter_text_displayed(self):
                try:
                    return self.find_element(*self._card_twitter_text_locator)
                except NoSuchElementException:
                    return False

            @property
            def card_tags(self):
                return self.find_elements(*self._card_tag_locator)

    @property
    def catalog_carousels(self):
        return [self.CatalogCarousel(self, element) for element in self.find_elements(*self._catalog_carousel_locator)]

    class CatalogCarousel(Region):

        _catalog_carousel_heading_locator = (By.CSS_SELECTOR, "h2[class*='c-module-heading']")
        _carousel_heading_link_locator = (By.CSS_SELECTOR, "a.o-link--single-chevron.o-card-row__title.o-link--heading")

        @property
        def catalog_carousel_heading(self):
            return self.find_element(*self._catalog_carousel_heading_locator)

        def carousel_heading_link(self, index):
            carousel_heading_links = self.find_elements(*self._carousel_heading_link_locator)
            return carousel_heading_links[index]

    @property
    def quotes_carousels(self):
        return [self.QuotesCarousel(self, element) for element in self.find_elements(*self._quotes_carousel_locator)]

    class QuotesCarousel(Region):

        _quotes_carousel_heading_locator = (By.CSS_SELECTOR, "h2[class*='c-module-heading']")
        _quotes_cards_locator = (By.CSS_SELECTOR, "[data-key='card']")
        _right_arrow_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-arrow-button.cp-regular-arrow-button.cp-arrow-button-right > svg")
        _left_arrow_locator = (By.CSS_SELECTOR, "button.cp-btn.btn.cp-arrow-button.cp-regular-arrow-button.cp-arrow-button-left > svg")
        _carousel_heading_link_locator = (By.CSS_SELECTOR, "a.o-link--single-chevron.o-card-row__title.o-link--heading")

        @property
        def quotes_carousel_heading(self):
            return self.find_element(*self._quotes_carousel_heading_locator)

        def quotes_cards(self, index):
            quotes_cards = self.find_elements(*self._quotes_cards_locator)
            return quotes_cards[index]

        @property
        def right_arrow(self):
            right_arrow = self.find_elements(*self._right_arrow_locator)
            return right_arrow[0]

        @property
        def left_arrow(self):
            left_arrow = self.find_elements(*self._left_arrow_locator)
            return left_arrow[0]

        def carousel_heading_link(self, index):
            carousel_heading_links = self.find_elements(*self._carousel_heading_link_locator)
            return carousel_heading_links[index]

    @property
    def comments_carousels(self):
        return [self.CommentsCarousel(self, element) for element in self.find_elements(*self._comments_carousel_locator)]

    class CommentsCarousel(Region):

        _comments_carousel_heading_locator = (By.CSS_SELECTOR, "h2[class*='c-module-heading']")
        _carousel_heading_link_locator = (By.CSS_SELECTOR, "a.o-link--single-chevron.o-card-row__title.o-link--heading")

        @property
        def comments_carousel_heading(self):
            return self.find_element(*self._comments_carousel_heading_locator)

        def carousel_heading_link(self, index):
            carousel_heading_links = self.find_elements(*self._carousel_heading_link_locator)
            return carousel_heading_links[index]
