import pytest
import allure
import sure
import configuration.user
import configuration.system
from mimesis import Text, Internet
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.create_new_featured_block.create_new_hero_slide import CreateNewHeroSlidePage
from pages.web.staff_base import StaffBasePage
from pages.web.page_builder import PageBuilderPage
from selenium.webdriver.common.action_chains import ActionChains
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from pages.web.wpadmin.v3.all_contents_page.all_hero_slides import AllHeroSlidesPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.components.page_builder.page_builder_modules import PageBuilderHeroSlider
from utils.image_download_helper import *


PAGE = 'bibliocommons-settings'
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Hero Slide"

SLIDE_NAME = ' '.join(Text('en').words(quantity=3))
SLIDE_LINK = Internet('en').home_page()
DESKTOP_IMAGE_TITLE = ''.join(Text('en').words(quantity=2))
DESKTOP_IMAGE_PATH = get_image_path_name(DESKTOP_IMAGE_TITLE, ".jpg")
LARGE_IMAGE_TITLE = ''.join(Text('en').words(quantity=2))
LARGE_IMAGE_PATH = get_image_path_name(LARGE_IMAGE_TITLE, ".jpg")
SMALL_IMAGE_TITLE = ''.join(Text('en').words(quantity=2))
SMALL_IMAGE_PATH = get_image_path_name(SMALL_IMAGE_TITLE, ".jpg")

SLIDE_NAME2 = ' '.join(Text('en').words(quantity=3))
SLIDE_LINK2 = "http://www.google.ca"
DESKTOP_IMAGE_TITLE2 = ''.join(Text('en').words(quantity=2))
DESKTOP_IMAGE_PATH2 = get_image_path_name(DESKTOP_IMAGE_TITLE2, ".jpg")
LARGE_IMAGE_TITLE2 = ''.join(Text('en').words(quantity=2))
LARGE_IMAGE_PATH2 = get_image_path_name(LARGE_IMAGE_TITLE2, ".jpg")
SMALL_IMAGE_TITLE2 = ''.join(Text('en').words(quantity=2))
SMALL_IMAGE_PATH2 = get_image_path_name(SMALL_IMAGE_TITLE2, ".jpg")


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87869: PB - Hero Slider")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87869", "TestRail")
class TestC87869:
    def test_C87869(self):
        # Logged in as Network Admin
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])

        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3],
                                                                     page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_implementing.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.true
        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2kucHHR", DESKTOP_IMAGE_PATH)
        download_image("https://bit.ly/2kFap8z", LARGE_IMAGE_PATH)
        download_image("https://bit.ly/2kfgzwc", SMALL_IMAGE_PATH)
        download_image("https://bit.ly/2lUiQNR", DESKTOP_IMAGE_PATH2)
        download_image("https://bit.ly/2kT1pwG", LARGE_IMAGE_PATH2)
        download_image("https://bit.ly/2kRXuAq", SMALL_IMAGE_PATH2)

        # PART 1 - Create 2 Hero Slide
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        new_hero_slide = CreateNewHeroSlidePage(self.driver, configuration.system.urls_web[3], post_type='bw_hero_slide').open()
        new_hero_slide.create_slide(SLIDE_NAME, SLIDE_LINK, DESKTOP_IMAGE_PATH, LARGE_IMAGE_PATH, SMALL_IMAGE_PATH)

        new_hero_slide = CreateNewHeroSlidePage(self.driver, configuration.system.urls_web[3], post_type='bw_hero_slide').open()
        new_hero_slide.create_slide(SLIDE_NAME2, SLIDE_LINK2, DESKTOP_IMAGE_PATH2, LARGE_IMAGE_PATH2, SMALL_IMAGE_PATH2)

        # PART 2 - Create a New Page
        new_page = CreateNewPagePage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        page_staff = PageBuilderPage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_staff.wait.until(lambda s: page_staff.wpheader.is_wp_admin_header_displayed)
        page_staff.wpheader.page_builder.click()

        # Hero Slider 1 - Add 2 Hero Slides
        if page_staff.builder_panel.is_panel_visible is False:
            page_staff.wpheader.add_content.click()
        page_staff.builder_panel.modules_tab.click()

        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.hero_slider, page_staff.page_builder.body).perform()
        hero_slider = PageBuilderHeroSlider(page_staff)
        hero_slider.select_slide(SLIDE_NAME)
        hero_slider.add_content_button.click()
        hero_slider.select_slide(SLIDE_NAME2)
        hero_slider.save()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: len(page_staff.user_facing_modules.hero_sliders) > 0)

        # Hero Slider 2 - empty
        page_staff.page_builder.add_content.click()
        wait.until(lambda s: page_staff.builder_panel.is_panel_visible)
        ActionChains(self.driver).drag_and_drop(page_staff.builder_panel.hero_slider, page_staff.page_builder.body).perform()
        hero_slider = PageBuilderHeroSlider(page_staff)
        hero_slider.save()
        wait.until(lambda s: len(page_staff.user_facing_modules.placeholders) > 0)

        page_staff.page_builder.done.click()
        page_staff.wait.until(lambda condition: page_staff.page_builder.publish.is_displayed())
        page_staff.page_builder.publish.click()

        len(page_staff.user_facing_modules.hero_sliders).should.equal(1)
        len(page_staff.user_facing_modules.placeholders).should.equal(1)
        page_staff.user_facing_modules.placeholders[0].title.text.should.match("HERO SLIDER")
        page_staff.user_facing_modules.placeholders[0].error.text.should.match("No Slide Selected")
        self.driver.execute_script("arguments[0].className = 'menupop with-avatar hover'", page_staff.wpheader.my_account)
        page_staff.wpheader.wait.until(lambda s: page_staff.wpheader.is_my_account_submenu_displayed)
        page_staff.wpheader.my_account_log_out.click()

        # PART 3 - Patron view
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                          configuration.user.user['web']['local']['admin']['password'])
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.urls_web[3], page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()
        settings_system_settings_tab.wpsidemenu.is_menu_switch_to_v2_menu_displayed.should.be.false
        self.driver.delete_all_cookies()

        page_patron = StaffBasePage(self.driver, configuration.system.urls_web[3] + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_staff.user_facing_modules.placeholders).should.equal(0)
        len(page_patron.user_facing_modules.hero_sliders).should.equal(1)
        page_patron.user_facing_modules.hero_sliders[0].is_more_than_one_slides_displayed.should.be.true
        len(page_patron.user_facing_modules.hero_sliders[0].hero_slides).should.equal(2)
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[0].title.text.should.match(SLIDE_NAME)
        page_patron.user_facing_modules.hero_sliders[0].hero_slides[1].title.text.should.match(SLIDE_NAME2)
        self.driver.delete_all_cookies()

        # PART 4 - Delete contents
        login_page = LoginPage(self.driver, configuration.system.urls_web[3]).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        all_hero_slides_page = AllHeroSlidesPage(self.driver, configuration.system.urls_web[3], post_type='bw_hero_slide').open()
        all_hero_slides_page.delete_content(SLIDE_NAME)

        all_hero_slides_page = AllHeroSlidesPage(self.driver, configuration.system.urls_web[3], post_type='bw_hero_slide').open()
        all_hero_slides_page.delete_content(SLIDE_NAME2)

        all_pages_page = AllPagesPage(self.driver, configuration.system.urls_web[3], post_type='page').open()
        all_pages_page.delete_content(PAGE_TITLE)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.delete_image(DESKTOP_IMAGE_TITLE)
        delete_downloaded_image(DESKTOP_IMAGE_PATH)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.delete_image(DESKTOP_IMAGE_TITLE2)
        delete_downloaded_image(DESKTOP_IMAGE_PATH2)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.delete_image(LARGE_IMAGE_TITLE)
        delete_downloaded_image(LARGE_IMAGE_PATH)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.delete_image(LARGE_IMAGE_TITLE2)
        delete_downloaded_image(LARGE_IMAGE_PATH2)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.delete_image(SMALL_IMAGE_TITLE)
        delete_downloaded_image(SMALL_IMAGE_PATH)

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.urls_web[3], mode='list').open()
        media_library_page.delete_image(SMALL_IMAGE_TITLE2)
        delete_downloaded_image(SMALL_IMAGE_PATH2)

        self.driver.delete_all_cookies()
