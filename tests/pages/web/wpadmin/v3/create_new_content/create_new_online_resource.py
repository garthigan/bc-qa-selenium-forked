from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.web.staff_base import StaffBasePage
from pages.web.components.description_area_in_pages_and_posts import Visual, Text


class CreateNewOnlineResourcePage(StaffBasePage):
    _resource_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _resource_title_locator = (By.CSS_SELECTOR, "input#title")

    # Resource Link
    _resource_link_label_locator = (By.CSS_SELECTOR, "[for='bc_resource_link']")
    _resource_link_locator = (By.ID, "bc_resource_link")

    # Options Block
    _option_block_locator = (By.CSS_SELECTOR, "#bc_meta_box_options")
    _option_heading_locator = (By.CSS_SELECTOR, "#bc_meta_box_options .ui-sortable-handle")
    _mark_as_featured_locator = (By.ID, "is_custom_sticky")
    _mark_as_new_locator = (By.ID, "is_new")
    _only_available_at_library_locator = (By.ID, "is_in_library")
    _library_card_required_locator = (By.ID, "is_card_needed")
    _only_available_at_locator = (By.ID, "is_in_branch")
    _only_available_at_textbox_locator = (By.ID, "in_branch_localization")
    _ios_app_checkbox_locator = (By.ID, "is_iphone")
    _ios_app_link_locator = (By.ID, "iphone_url")
    _android_checkbox_locator = (By.ID, "is_android")
    _android_link_locator = (By.ID, "android_url")
    _amazon_checkbox_locator = (By.ID, "is_underground")
    _amazon_link_locator = (By.ID, "underground_url")

    # Publish Block
    _save_draft_button_locator = (By.ID, "save-post")
    _publish_button_locator = (By.CSS_SELECTOR, "input#publish")
    _success_message_locator = (By.CSS_SELECTOR, "#message>p")

    # Subject Blocks
    _online_resource_subject_heading_locator = (By.CSS_SELECTOR, "div#bccms_online_link_taxdiv > h2 > span")
    _online_resource_checkboxes_locator = (By.CSS_SELECTOR, "#bccms_online_link_tax-all > ul > li > .selectit")
    _learning_tools_heading_locator = (By.CSS_SELECTOR, "div#bccms_learning_toolsdiv > h2 > span")
    _learning_tools_checkboxes_locator = (By.CSS_SELECTOR, "#bccms_learning_tools-all > ul > li > .selectit")
    _kids_heading_locator = (By.CSS_SELECTOR, "div#bc_child_subjectdiv > h2 > span")
    _kids_checkboxes_locator = (By.CSS_SELECTOR, "#bc_child_subject-all > ul > li > .selectit")
    _teen_heading_locator = (By.CSS_SELECTOR, "div#bc_teen_subjectdiv > h2 > span")
    _teen_checkboxes_locator = (By.CSS_SELECTOR, "#bc_teen_subject-all > ul > li > .selectit")
    _format_heading_locator = (By.CSS_SELECTOR, "div#bccms_online_link_typediv > h2 > span")
    _format_checkboxes_locator = (By.CSS_SELECTOR, "#bccms_online_link_type-all > ul > li > .selectit")

    @property
    def option_heading(self):
        return self.find_element(*self._option_heading_locator)

    @property
    def is_resource_heading_displayed(self):
        try:
            return self.find_element(*self._resource_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def resource_title(self):
        return self.find_element(*self._resource_title_locator)

    @property
    def is_resource_link_label_displayed(self):
        try:
            return self.find_element(*self._resource_link_label_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def resource_link(self):
        return self.find_element(*self._resource_link_locator)

    @property
    def mark_as_featured(self):
        return self.find_element(*self._mark_as_featured_locator)

    @property
    def mark_as_new(self):
        return self.find_element(*self._mark_as_new_locator)

    @property
    def only_available_at_library(self):
        return self.find_element(*self._only_available_at_library_locator)

    @property
    def library_card_required(self):
        return self.find_element(*self._library_card_required_locator)

    @property
    def only_available_at(self):
        return self.find_element(*self._only_available_at_locator)

    @property
    def only_available_at_textbox(self):
        return self.find_element(*self._only_available_at_textbox_locator)

    @property
    def ios_app_checkbox(self):
        return self.find_element(*self._ios_app_checkbox_locator)

    @property
    def ios_app_link(self):
        return self.find_element(*self._ios_app_link_locator)

    @property
    def android_checkbox(self):
        return self.find_element(*self._android_checkbox_locator)

    @property
    def android_link(self):
        return self.find_element(*self._android_link_locator)

    @property
    def amazon_checkbox(self):
        return self.find_element(*self._amazon_checkbox_locator)

    @property
    def amazon_link(self):
        return self.find_element(*self._amazon_link_locator)

    @property
    def save_draft(self):
        return self.find_element(*self._save_draft_button_locator)

    @property
    def publish(self):
        return self.find_element(*self._publish_button_locator)

    @property
    def is_success_message_displayed(self):
        try:
            return self.find_element(*self._success_message_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def text(self):
        return Text(self)

    @property
    def visual(self):
        return Visual(self)

    @property
    def is_online_resource_subject_heading_displayed(self):
        try:
            return self.find_element(*self._online_resource_subject_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_learning_tools_heading_displayed(self):
        try:
            return self.find_element(*self._learning_tools_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_kids_heading_displayed(self):
        try:
            return self.find_element(*self._kids_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_teen_heading_displayed(self):
        try:
            return self.find_element(*self._teen_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_format_heading_displayed(self):
        try:
            return self.find_element(*self._format_heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def online_resource_checkboxes(self):
        return self.find_elements(*self._online_resource_checkboxes_locator)

    @property
    def learning_tools_checkboxes(self):
        return self.find_elements(*self._learning_tools_checkboxes_locator)

    @property
    def kids_checkboxes(self):
        return self.find_elements(*self._kids_checkboxes_locator)

    @property
    def teens_checkboxes(self):
        return self.find_elements(*self._teen_checkboxes_locator)

    @property
    def format_checkboxes(self):
        return self.find_elements(*self._format_checkboxes_locator)
