from selenium.webdriver.common.by import By
from pypom import Region
from pages.web.wpadmin.v3.media_library.media_library_base import MediaLibraryBasePage
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys


class MediaLibraryListPage(MediaLibraryBasePage):

    _rows_locator = (By.CSS_SELECTOR, "tr[id*=post-]")

    @property
    def rows(self):
        return [Row(self, element) for element in self.find_elements(*self._rows_locator)]

    def search_and_delete(self, title):
        self.search_input.send_keys(title, Keys.RETURN)
        self.rows[0].hover_on_title()
        self.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()


class Row(Region):

    _checkbox_locator = (By.CSS_SELECTOR, "th.check-column > input[type='checkbox']")
    _title_locator = (By.CSS_SELECTOR, "td[data-colname='File'] > strong > a")
    _edit_locator = (By.CSS_SELECTOR, "span.edit > a")
    _delete_locator = (By.CSS_SELECTOR, "span.delete > a")
    _view_locator = (By.CSS_SELECTOR, "span.view > a")
    _regenerate_thumbnails_locator = (By.CSS_SELECTOR, "span.regenerate_thumbnails > a")
    _crop_locator = (By.CSS_SELECTOR, "span.crop > a")
    _uploaded_to_content_title_locator = (By.CSS_SELECTOR, "td[data-colname='Uploaded to'] > strong > a")

    def hover_on_title(self):
        ActionChains(self.driver).move_to_element(self.title).perform()

    @property
    def checkbox(self):
        return self.find_element(*self._checkbox_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def edit(self):
        return self.find_element(*self._edit_locator)

    @property
    def delete(self):
        return self.find_element(*self._delete_locator)

    @property
    def view(self):
        return self.find_element(*self._view_locator)

    @property
    def regenerate_thumbnails(self):
        return self.find_element(*self._regenerate_thumbnails_locator)

    @property
    def crop(self):
        return self.find_element(*self._crop_locator)

    @property
    def uploaded_to_content_title(self):
        return self.find_element(*self._uploaded_to_content_title_locator)

    @property
    def is_uploaded_to_content_title_displayed(self):
        try:
            return self.find_element(*self._uploaded_to_content_title_locator).is_displayed()
        except NoSuchElementException:
            return False
