import pytest
import allure
import sure
import configuration.system
import configuration.user

from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage
from pages.web.wpadmin.login import LoginPage


@pytest.mark.stage
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("")
@allure.testcase("", "TestRail")
class Test001:
    def test_001(self):
        login_page = LoginPage(self.driver, configuration.system.urls_web[0]).open()
        login_page.log_in(configuration.user.user['web']['stage']['admin']['name'], configuration.user.user['web']['stage']['admin']['password'])

        default_settings_page = DefaultSettingsPageBuilderPage(self.driver, configuration.system.urls_web[0],
                                                               post_type='fl-builder-template',
                                                               page='default-page-builder-settings').open()
        home_page_dropdown_value = "Test"
        default_settings_page.select_home_page_locator_dropdown(home_page_dropdown_value)
        default_settings_page.select_row_width_dropdown("fixed", 1200)
        default_settings_page.select_heading_module_dropdown("Large")
        default_settings_page.select_standard_card_title_dropdown("Moderate")
        default_settings_page.select_structured_card_title_dropdown("Small")
        default_settings_page.select_card_description_dropdown("Hide Description")
        default_settings_page.select_taxonomy_links_dropdown(["Content Types", "Audience"])
        default_settings_page.select_image_position_dropdown("Below Text")
        default_settings_page.select_border_style_dropdown("Show Border")
        default_settings_page.select_tiled_card_border_dropdown("Show Border")
        default_settings_page.select_poll_card_style_dropdown("Response Options Open")
        default_settings_page.select_button_style_dropdown("Ghost")
        default_settings_page.select_background_hover_color_dropdown("Lighten 20%")
        default_settings_page.save_changes_button_click()
        expected_value = default_settings_page.verify_set_homepage_dropdown()
        expected_value.text.should.equal(home_page_dropdown_value)

