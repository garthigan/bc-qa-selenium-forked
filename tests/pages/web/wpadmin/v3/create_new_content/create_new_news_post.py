from selenium.webdriver.common.by import By
from pages.web.wpadmin.v3.create_new_content.new_blog_post_and_news_post_base import BlogPostNewsPostBasePage


class CreateNewNewsPostPage(BlogPostNewsPostBasePage):

    _subtitle_locator = (By.CSS_SELECTOR, "input#bc_news_subtitle")

    @property
    def loaded(self):
        return self.find_element(*self._card_image_locator)

    @property
    def subtitle(self):
        return self.find_element(*self._subtitle_locator)
