from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.core.base import BasePage
from pages.core.components.shelf_item import ShelfItem


class ShelvesPage(BasePage):
    # Usage: ShelvesPage(self.driver, base_url, user_id = [UserID], shelf = [Shelf]).open()
    URL_TEMPLATE = "/v2/users/{user_id}/shelves/{shelf}"

    _shelf_item_locator = (By.CSS_SELECTOR, ".cp-item-list")
    _heading_locator = (By.CSS_SELECTOR, ".cp-core-external-header")
    _find_available_button_locator = (By.CSS_SELECTOR, "[data-key='find-available-titles-button']")
    _in_progress_tab_locator = (By.CSS_SELECTOR, "[data-key='shelf-link-in_progress']")
    _completed_tab_locator = (By.CSS_SELECTOR, "[data-key='shelf-link-completed']")
    _for_later_tab_locator = (By.CSS_SELECTOR, "[data-key='shelf-link-for_later']")
    _current_shelf_completed_locator = (By.CSS_SELECTOR, "[data-current-shelf='completed']")
    _current_shelf_in_progress_locator = (By.CSS_SELECTOR, "[data-current-shelf='in_progress']")
    _current_shelf_for_later_locator = (By.CSS_SELECTOR, "[data-current-shelf='for_later']")
    _select_items_locator = (By.CSS_SELECTOR, "div.master-checkbox button[role='checkbox']")
    _manage_items_locator = (By.CSS_SELECTOR, "div[class*='dropup'] button[class*='actions-dropdown']")
    _manage_items_menu_locator = (By.CSS_SELECTOR, "ul[id='dropdownMenu_9']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def items(self):
        return [ShelfItem(self, element) for element in self.find_elements(*self._shelf_item_locator)]

    @property
    def is_find_available_button_displayed(self):
        try:
            return self.find_element(*self._find_available_button_locator)
        except NoSuchElementException:
            return False

    @property
    def find_available_button(self):
        return self.find_element(*self._find_available_button_locator)

    @property
    def in_progress_tab(self):
        return self.find_element(*self._in_progress_tab_locator)

    @property
    def completed_tab(self):
        return self.find_element(*self._completed_tab_locator)

    @property
    def for_later_tab(self):
        return self.find_element(*self._for_later_tab_locator)

    @property
    def is_current_shelf_completed(self):
        try:
            return self.find_element(*self._current_shelf_completed_locator)
        except NoSuchElementException:
            return False

    @property
    def is_current_shelf_in_progress(self):
        try:
            return self.find_element(*self._current_shelf_in_progress_locator)
        except NoSuchElementException:
            return False

    @property
    def is_current_shelf_for_later(self):
        try:
            return self.find_element(*self._current_shelf_for_later_locator)
        except NoSuchElementException:
            return False

    @property
    def select_items(self):
        return self.find_element(*self._select_items_locator)

    @property
    def manage_items(self):
        WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, self._manage_items_locator[1]))
        )
        return self.find_element(*self._manage_items_locator)

    @property
    def manage_items_menu(self):
        return self.find_element(*self._manage_items_menu_locator)
