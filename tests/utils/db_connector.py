import pymysql.cursors
from pymysql import OperationalError, DataError
import configuration.system


class DBConnector:

    def check_updated_post_title(self, title, postid):
        connection = pymysql.connect(host=configuration.system.wordpress_db_details[0],
                                     user=configuration.system.wordpress_db_details[1],
                                     password=configuration.system.wordpress_db_details[1],
                                     db=configuration.system.wordpress_db_details[2])

        try:
            with connection.cursor() as cursor:
                    sql = "SELECT `post_title` FROM `wp_2_posts` WHERE `ID` = %s"
                    cursor.execute(sql, postid)
                    result = cursor.fetchone()

                    if title in result:
                        connection.close()
                        return True
                    else:
                        connection.close()
                        raise DataError("Title has not been updated.")
        except OperationalError as error:
            raise error

    def check_updated_post_description(self, description, postid):
        connection = pymysql.connect(host=configuration.system.wordpress_db_details[0],
                                     user=configuration.system.wordpress_db_details[1],
                                     password=configuration.system.wordpress_db_details[1],
                                     db=configuration.system.wordpress_db_details[2])

        try:
            with connection.cursor() as cursor:
                    sql = "SELECT `post_content` FROM `wp_2_posts` WHERE `ID` = %s"
                    cursor.execute(sql, postid)
                    result = cursor.fetchone()

                    if description in result:
                        connection.close()
                        return True
                    else:
                        connection.close()
                        raise DataError("Description has not been updated.")
        except OperationalError as error:
            raise error
