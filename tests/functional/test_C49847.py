import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.v2.checked_out import CheckedOutPage
from pages.core.bib import BibPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C49847: Check out an overdrive ebook on bib page")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C49847", "TestRail")
class TestC49847:
    def test_C49847(self):
        self.item_id = "2010786126" #Title: "Waking up in Paris" #2010786126 2010796126
        self.name = "D059874977" #black_bat
        self.password = "60605"
        self.base_url = "https://chipublib.demo.bibliocommons.com"

        bib_page = BibPage(self.driver, self.base_url, item_id=self.item_id).open()
        bib_page.header.log_in(self.name, self.password)
        bib_page.wait.until(lambda s: bib_page.circulation.is_overdrive_place_hold_displayed)
        bib_page.circulation.overdrive_place_hold.click()
        bib_page.wait.until(lambda s: bib_page.overlay.overdrive_checkout.is_checkout_digital_item_displayed)
        bib_page.overlay.overdrive_checkout.checkout_digital_item.click()
        bib_page.wait.until(lambda s: bib_page.circulation.is_digital_inline_success_displayed)
        bib_page.circulation.is_digital_inline_success_displayed.should.be.true

        checked_out_page = CheckedOutPage(self.driver, self.base_url).open()
        checked_out_page.wait.until(lambda s: checked_out_page.is_list_displayed)
        checked_out_page.items[0].bib_title.text.should.contain("La Batalla Por el Paraíso")
        checked_out_page.items[0].return_now.click()
        checked_out_page.wait.until(lambda s: checked_out_page.is_list_empty_displayed)
        checked_out_page.is_checked_out_success_notification_displayed.should.be.true
