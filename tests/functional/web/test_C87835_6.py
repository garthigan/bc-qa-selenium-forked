import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.all_contents_page.all_online_resources import AllOnlineResourcesPage
from pages.web.wpadmin.v3.dashboard import DashboardPage
from pages.web.wpadmin.v3.create_new_content.create_new_online_resource import CreateNewOnlineResourcePage
from pages.web.wpadmin.v3.create_a_new_card.create_new_online_resource_card import CreateNewOnlineResourceCard
from pages.web.wpadmin.v3.all_contents_page.all_pages import AllPagesPage
from pages.web.page_builder import PageBuilderPage
from pages.web.wpadmin.v3.edit_online_resource import EditOnlineResourcePage
from pages.web.wpadmin.v3.settings_general_tab import SettingsGeneralTabPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from utils.selenium_helpers import click
from selenium.common.exceptions import NoSuchElementException
from pages.web.wpadmin.v3.create_new_content.create_new_page import CreateNewPagePage
from utils.image_download_helper import *
from pages.web.wpadmin.v3.all_contents_page.all_cards import AllCardsPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.wpadmin.v3.default_settings_page_builder_page import DefaultSettingsPageBuilderPage
from utils.db_connector import DBConnector
from pages.web.user import UserPage


PAGE = "bibliocommons-settings"
RESOURCE_INFO = {
    'title': ' '.join(Text('en').words(quantity=3)),
    'description': ' '.join(Text('en').words())
}
IMAGE_TITLE = ' '.join(Text('en').words(quantity=3))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")
UPDATED_RESOURCE_TITLE = "UPDATED: " + RESOURCE_INFO['title']
UPDATED_RESOURCE_DESCRIPTION = "UPDATED: " + RESOURCE_INFO['description']
PAGE_TITLE = '-'.join(Text('en').words(quantity=3))
CONTENT_TYPE = "Online Resource"


@pytest.fixture(scope='class')
def login_and_setup(request, selenium_setup_and_teardown):

    driver = request.cls.driver

    # Log in as Network Admin
    login_page = LoginPage(driver, configuration.system.base_url_web).open()
    login_page.log_in(configuration.user.user['web']['local']['admin']['name'],
                      configuration.user.user['web']['local']['admin']['password'])
    settings_system_settings_tab = SettingsSystemSettingsTabPage(driver, configuration.system.base_url_web,
                                                                 page=PAGE, tab='system').open()
    settings_system_settings_tab.v3_status_enabled.click()
    settings_system_settings_tab.save_changes.click()

    settings_general_tab = SettingsGeneralTabPage(driver, configuration.system.base_url_web, page=PAGE,
                                                  tab='generic').open()
    click(settings_general_tab.enable_make_tags_a_structured_taxonomy)
    settings_general_tab.save_changes.click()

    default_settings_page = DefaultSettingsPageBuilderPage(driver, configuration.system.base_url_web,
                                                           post_type='fl-builder-template',
                                                           page='default-page-builder-settings').open()
    default_settings_page.check_all_display_taxonomy_links_checkboxes()
    default_settings_page.save_changes_button_click()

    driver.delete_all_cookies()


@pytest.mark.usefixtures('login_and_setup')
@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
class Tests:
    @allure.title("C87835: Online Resource Card - New")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87835", "TestRail")
    def test_C87835(self):

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])
        dashboard_page = DashboardPage(self.driver)
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.content_blocks[5].create_new_button_content_resource.click()

        download_image("https://bit.ly/2Z82kcB", IMAGE_PATH)

        # Creating new Online Resource
        new_online_resource = CreateNewOnlineResourcePage(self.driver)
        new_online_resource.wait.until(lambda s: new_online_resource.is_resource_heading_displayed)
        new_online_resource.resource_title.send_keys(RESOURCE_INFO['title'])
        new_online_resource.driver.switch_to.frame(new_online_resource.visual.iframe)
        new_online_resource.visual.body.send_keys(RESOURCE_INFO['description'])
        new_online_resource.driver.switch_to.default_content()
        if not new_online_resource.mark_as_featured.is_selected():
            new_online_resource.mark_as_featured.click()
        if not new_online_resource.mark_as_new.is_selected():
            new_online_resource.mark_as_new.click()
        if not new_online_resource.ios_app_checkbox.is_selected():
            new_online_resource.ios_app_checkbox.click()
        new_online_resource.ios_app_link.send_keys("https://apple.com")
        new_online_resource.resource_link.send_keys("https://google.ca")
        click(new_online_resource.publish)
        new_online_resource.wait.until(lambda s: new_online_resource.is_success_message_displayed)
        new_online_resource.is_success_message_displayed.should.be.true
        new_online_resource.wpsidemenu.menu_dashboard.click()

        dashboard_page = DashboardPage(self.driver)
        dashboard_page.wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.card_blocks[5].create_new_button_card_resource.click()

        # Creating Online Resource Card
        new_resource_card = CreateNewOnlineResourceCard(self.driver)
        new_resource_card.select_an_online_resource_dropdown.click()
        new_resource_card.select_an_online_resource(RESOURCE_INFO['title']).click()
        new_resource_card.grab_info.click()
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_resource_card.is_validator_message_displayed)
        new_resource_card.is_validator_success_message_displayed.should.be.true
        new_resource_card.validator_success_message.text.should.match("Resource information successfully grabbed!")
        click(new_resource_card.card_image)
        new_resource_card.select_widget_image.upload_files_tab.click()
        new_resource_card.select_widget_image.upload_image.send_keys(IMAGE_PATH)
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda condition: new_resource_card.select_widget_image.add_image_to_widget_button.is_displayed())
        click(new_resource_card.select_widget_image.add_image_to_widget_button)
        new_resource_card.select_default_image_crop_views()
        click(new_resource_card.publish)

        # Launching PB from existing page
        new_page = CreateNewPagePage(self.driver, configuration.system.base_url_web, post_type='page').open()
        new_page.title.send_keys(PAGE_TITLE)
        new_page.page_builder_section.click()
        click(new_page.publish)

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.launch_page_builder(PAGE_TITLE)

        # Select single card module
        page_builder_page = PageBuilderPage(self.driver)
        page_builder_page.wait.until(lambda s: page_builder_page.page_builder.is_page_builder_header_bar_displayed)
        page_builder_page.add_single_card(CONTENT_TYPE, RESOURCE_INFO['title'])
        wait.until(lambda s: page_builder_page.user_facing_modules.single_cards[0].cards[0].is_card_title_displayed)
        page_builder_page.page_builder.done_and_publish()
        page_builder_page.log_out_from_header()

        # PART 3 - Patron view
        self.driver.delete_all_cookies()
        page_patron = UserPage(self.driver, configuration.system.base_url_web + PAGE_TITLE).open()
        page_patron.wpheader.is_wp_admin_header_displayed.should.be.false
        len(page_patron.user_facing_modules.single_cards).should.equal(1)
        len(page_patron.user_facing_modules.single_cards[0].cards).should.equal(1)
        page_patron.user_facing_modules.single_cards[0].cards[0].is_card_content_type_displayed.should.be.true
        page_patron.user_facing_modules.single_cards[0].cards[0].card_content_type.text.should.match(CONTENT_TYPE.upper())
        page_patron.user_facing_modules.single_cards[0].cards[0].card_title.text.should.match(RESOURCE_INFO['title'])

    @allure.title("C87836: Update V2 Content - Online Resource")
    @allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87836", "TestRail")
    def test_C87836(self):

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'],
                          configuration.user.user['web']['local']['libadmin']['password'])

        dashboard_page = DashboardPage(self.driver)
        wait = WebDriverWait(self.driver, 16, poll_frequency=2, ignored_exceptions=[IndexError, NoSuchElementException])
        wait.until(lambda s: dashboard_page.is_page_heading_displayed)
        dashboard_page.content_blocks[5].view_all_link_content_resource.click()

        # Edit an existing Online Resource
        all_online_resources = AllOnlineResourcesPage(self.driver)
        all_online_resources.rows[0].hover_on_title()
        all_online_resources.rows[0].edit.click()
        edit_online_resource = EditOnlineResourcePage(self.driver)
        wait.until(lambda s: edit_online_resource.is_resource_heading_displayed)
        edit_online_resource.is_resource_heading_displayed.should.be.true
        edit_online_resource.resource_title.clear()
        edit_online_resource.resource_title.send_keys(UPDATED_RESOURCE_TITLE)
        edit_online_resource.driver.switch_to.frame(edit_online_resource.visual.iframe)
        edit_online_resource.visual.body.clear()
        edit_online_resource.visual.body.send_keys(UPDATED_RESOURCE_DESCRIPTION)
        edit_online_resource.driver.switch_to.default_content()
        if not edit_online_resource.online_resource_checkboxes[0].is_selected():
            click(edit_online_resource.online_resource_checkboxes[0])
        edit_online_resource.update.click()
        wait.until(lambda s: edit_online_resource.is_success_message_displayed)
        edit_online_resource.is_success_message_displayed.should.be.true
        edit_online_resource.resource_title.get_attribute("value").should.equal(UPDATED_RESOURCE_TITLE)
        edit_online_resource.driver.switch_to.frame(edit_online_resource.visual.iframe)
        edit_online_resource.visual.body.text.should.equal(UPDATED_RESOURCE_DESCRIPTION)
        edit_online_resource.driver.switch_to.default_content()

        # Getting the Resource ID
        all_online_resources_page = AllOnlineResourcesPage(self.driver, configuration.system.base_url_web, post_type='bccms_online_link').open()
        all_online_resources_page.search_content_by_button(UPDATED_RESOURCE_TITLE)
        all_online_resources_page.rows[0].title.text.should.match(UPDATED_RESOURCE_TITLE)
        post_href = all_online_resources_page.rows[0].title.get_attribute("href")
        resource_id = post_href[58:-12]

        # Checking the DB if the resource title and description are updated
        # select id, post_author, post_date, post_title, post_content from wp_2_posts where id=722
        check_db = DBConnector()
        check_db.check_updated_post_title(UPDATED_RESOURCE_TITLE, resource_id).should.be.true
        check_db.check_updated_post_description(UPDATED_RESOURCE_DESCRIPTION, resource_id).should.be.true

        # PART 4 - Delete contents
        all_cards_page = AllCardsPage(self.driver, configuration.system.base_url_web, page='bw-content-card').open()
        all_cards_page.search_and_delete(RESOURCE_INFO['title'])
        all_cards_page.rows.should.be.empty

        all_pages_page = AllPagesPage(self.driver, configuration.system.base_url_web, post_type='page').open()
        all_pages_page.search_and_delete(PAGE_TITLE)
        all_pages_page.rows.should.be.empty

        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_and_delete(IMAGE_TITLE)
        media_library_page.rows.should.be.empty

        delete_downloaded_image(IMAGE_PATH)

        all_online_resources_page.open()
        all_online_resources_page.search_and_delete(UPDATED_RESOURCE_TITLE)
        all_online_resources_page.rows.should.be.empty
