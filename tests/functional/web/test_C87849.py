import pytest
import allure
import sure
import configuration.system
import configuration.user
from mimesis import Text
from selenium.common.exceptions import ElementNotInteractableException, NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from pages.web.wpadmin.login import LoginPage
from pages.web.wpadmin.v3.settings_system_settings_tab import SettingsSystemSettingsTabPage
from pages.web.wpadmin.v3.all_contents_page.all_blog_posts import AllBlogPostsPage
from pages.web.wpadmin.v3.edit_blog_post import EditBlogPostPage
from pages.web.wpadmin.v3.media_library.media_library_list import MediaLibraryListPage
from pages.web.user import UserPage
from utils.selenium_helpers import click
from utils.db_connector import DBConnector
from utils.image_download_helper import *
from selenium.webdriver.common.keys import Keys


PAGE = "bibliocommons-settings"
BLOG_TITLE_UPDATED = '-'.join(Text('en').words(quantity=3))
IMAGE_TITLE = ''.join(Text('en').words(quantity=2))
IMAGE_PATH = get_image_path_name(IMAGE_TITLE, ".jpg")


@pytest.mark.v3
@pytest.mark.release
@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C87849: Image Crop - Update")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/87849", "TestRail")
class TestC87849:
    def test_C87849(self):
        # Logging in as network admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['admin']['name'], configuration.user.user['web']['local']['admin']['password'])

        # Setting V3 status as enabled
        settings_system_settings_tab = SettingsSystemSettingsTabPage(self.driver, configuration.system.base_url_web, page=PAGE, tab='system').open()
        settings_system_settings_tab.v3_status_enabled.click()
        settings_system_settings_tab.save_changes.click()

        self.driver.delete_all_cookies()

        download_image("https://bit.ly/2IOmAJs", IMAGE_PATH)

        # Logging in as lib admin
        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.user['web']['local']['libadmin']['name'], configuration.user.user['web']['local']['libadmin']['password'])

        all_blogs_page = AllBlogPostsPage(self.driver, configuration.system.base_url_web, post_type='post').open()
        # Filtering for published blog posts
        all_blogs_page.published_filter.click()
        post_href = all_blogs_page.rows[0].title.get_attribute("href")
        blog_post_id = post_href[58:-12]

        # Checking that the created blog post contains all the expected information
        edit_blog_post = EditBlogPostPage(self.driver, configuration.system.base_url_web, post=blog_post_id, action='edit').open()

        # Updating the blog post title
        edit_blog_post.title.clear()
        edit_blog_post.title.send_keys(BLOG_TITLE_UPDATED)
        wait = WebDriverWait(self.driver, 5, poll_frequency=1)

        try:
            wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_one_preview_visible)
            wait.until(lambda condition: edit_blog_post.remove.is_displayed())
            click(edit_blog_post.remove)
        except NoSuchElementException:
            pass

        edit_blog_post.card_image.click()
        edit_blog_post.select_widget_image.upload_files_tab.click()
        edit_blog_post.insert_media.upload_image.send_keys(IMAGE_PATH)
        click(edit_blog_post.insert_media.add_image_to_widget_button)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=NoSuchElementException)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_cropper_modal_visible)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_one_visible)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_cropper_box_visible)
        edit_blog_post.image_cropper.image_one_crop()
        edit_blog_post.image_cropper.crop_image.click()
        edit_blog_post.image_cropper.next.click()
        wait.until(lambda condition: edit_blog_post.image_cropper.is_cropper_box_visible)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_one_visible == False)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_two_visible)
        edit_blog_post.image_cropper.image_two_crop()
        edit_blog_post.image_cropper.crop_image.click()
        edit_blog_post.image_cropper.done.click()
        wait.until(lambda condition: edit_blog_post.image_cropper.is_cropper_modal_visible == False)
        edit_blog_post.scroll_to_top()
        click(edit_blog_post.update)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_two_preview_visible)
        self.driver.refresh()
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_one_preview_visible)
        wait.until(lambda condition: edit_blog_post.image_cropper.is_crop_two_preview_visible)

        # Opening a new tab
        self.driver.execute_script("window.open();")
        self.driver.switch_to_window(self.driver.window_handles[1])

        # Asserting that the update image crop is present in the search results page
        UserPage(self.driver, configuration.system.base_url_web, s=BLOG_TITLE_UPDATED).open()
        self.driver.page_source.should.contain(IMAGE_TITLE)

        # Checking the DB if the blog's title is updated (the image crop will have also updated if this passes)
        check_db = DBConnector()
        check_db.check_updated_post_title(BLOG_TITLE_UPDATED, blog_post_id).should.be.true

        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

        # Deleting the uploaded image
        media_library_page = MediaLibraryListPage(self.driver, configuration.system.base_url_web, mode='list').open()
        media_library_page.search_input.send_keys(IMAGE_TITLE, Keys.RETURN)
        wait = WebDriverWait(self.driver, 10, poll_frequency=2, ignored_exceptions=[IndexError, ElementNotInteractableException])
        wait.until(lambda condition: len(media_library_page.rows) == 1)
        media_library_page.rows[0].hover_on_title()
        wait.until(lambda condition: media_library_page.rows[0].delete.is_displayed())
        media_library_page.rows[0].delete.click()
        alert = self.driver.switch_to_alert()
        alert.accept()
        wait.until(lambda condition: media_library_page.rows.should.be.empty)

        delete_downloaded_image(IMAGE_PATH)
