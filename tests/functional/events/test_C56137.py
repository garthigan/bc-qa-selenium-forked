import pytest
import allure
import sys
# noinspection PyUnresolvedReferences
import sure
sys.path.append('tests')
from pages.events.events_locations import EventLocationPage
from mimesis import Address


@pytest.mark.local
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C56137 - Find by address or ZIP code - invalid")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/56137", "TestRail")
class TestC56137:
    def test_c56137(self):

        self.base_url = "http://chipublib.local.bibliocommons.com/locations/"
        location_page = EventLocationPage(self.driver, self.base_url).open()
        language = Address('en')
        zip_code = 'a'+language.postal_code()
        location_page.wait.until(lambda s: location_page.is_find_by_address_or_zip_displayed)
        location_page.find_by_address_or_zip.send_keys(zip_code)
        location_page.search_button[4].click()

        location_page.wait.until(lambda s: location_page.is_events_location_invalid_search_displayed)
        location_page.events_location_invalid_search_text.text.should.match(zip_code)
