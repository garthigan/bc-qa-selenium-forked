from selenium.webdriver.common.by import By
from selenium.common.exceptions import ElementNotSelectableException
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from pypom import Region


class PageBuilderModuleBase(Region):

    _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
    _save_as_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save-as.fl-builder-button.fl-builder-button-large")
    _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

    # Content Tab
    _content_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=content]")
    _show_visually_locator = (By.CSS_SELECTOR, "#fl-field-module_heading > td > div > div > div.d-inline > label > input")
    _module_heading_locator = (By.CSS_SELECTOR, "input[name='module_heading[][title]']")
    _link_to_locator = (By.CSS_SELECTOR, "select[name='exit_link_type']")
    _edit_content_link_locator = (By.CSS_SELECTOR, "a.fl-form-field-edit")
    _add_content_button_locator = (By.CSS_SELECTOR, "a.fl-builder-field-add.fl-builder-button")
    _content_type_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_content_type > td > div")
    _content_type_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_content_type > td > div > div > div > ul > li")
    _audience_locator = (By.CSS_SELECTOR, "#fl-field-bw_audience_category > td > div")
    _audience_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_audience_category > td > div > div > div > ul > li")
    _related_format_locator = (By.CSS_SELECTOR, "#fl-field-bw_related_format > td > div")
    _related_format_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_related_format > td > div > div > div > ul > li")
    _programs_and_campaigns_locator = (By.CSS_SELECTOR, "#fl-field-bc_program > td > div")
    _programs_and_campaigns_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bc_program > td > div > div > div > ul > li")
    _genre_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_genre > td > div")
    _genre_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_genre > td > div > div > div > ul > li")
    _topic_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_topic > td > div")
    _topic_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-bw_tax_topic > td > div > div > div > ul > li")
    _tags_locator = (By.CSS_SELECTOR, "#fl-field-post_tag > td > div")
    _tags_dropdown_results_locator = (By.CSS_SELECTOR, "#fl-field-post_tag > td > div > div > div > ul > li")
    _featured_locator = (By.CSS_SELECTOR, "select[name='bw_card_featured']")
    _evergreen_locator = (By.CSS_SELECTOR, "select[name='bw_card_evergreen']")

    # Display Tab
    _display_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=display]")
    _card_description_locator = (By.CSS_SELECTOR, "select[name='show_description']")
    _author_information_locator = (By.CSS_SELECTOR, "select[name='show_author']")
    _content_types_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_tax_content_type")
    _audience_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_audience_category")
    _related_format_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_related_format")
    _programs_and_campaigns_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bc_program")
    _genre_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_tax_genre")
    _topic_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-bw_tax_topic")
    _tags_checkbox_locator = (By.CSS_SELECTOR, "#show_tax-post_tag")

    # Styles Tab
    _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=styles]")
    _size_locator = (By.CSS_SELECTOR, "select[name='module_heading_font_size']")
    _left_alignment_locator = (By.CSS_SELECTOR, "button[data-value='left']")
    _center_alignment_locator = (By.CSS_SELECTOR, "button[data-value='center']")
    _right_alignment_locator = (By.CSS_SELECTOR, "button[data-value='right']")
    _title_size_locator = (By.CSS_SELECTOR, "select[name='title_font_size']")
    _align_text_locator = (By.CSS_SELECTOR, "select[name='align_text']")
    _image_position_locator = (By.CSS_SELECTOR, "select[name='image_position']")
    _image_crop_locator = (By.CSS_SELECTOR, "select[name='image_size']")
    _border_style_locator = (By.CSS_SELECTOR, "select[name='show_border']")
    _poll_card_style_locator = (By.CSS_SELECTOR, "select[name='show_response_options']")

    # Advanced Tab
    _advanced_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=advanced]")
    _top_margin_locator = (By.CSS_SELECTOR, "input[name='margin_top']")
    _right_margin_locator = (By.CSS_SELECTOR, "input[name='margin_right']")
    _bottom_margin_locator = (By.CSS_SELECTOR, "input[name='margin_bottom']")
    _left_margin_locator = (By.CSS_SELECTOR, "input[name='margin_left']")
    _breakpoint_locator = (By.CSS_SELECTOR, "select[name='responsive_display']")
    _display_locator = (By.CSS_SELECTOR, "select[name='visibility_display']")

    @property
    def content_tab(self):
        return self.find_element(*self._content_tab_locator)

    @property
    def module_heading(self):
        return self.find_element(*self._module_heading_locator)

    @property
    def show_visually(self):
        _ = self.find_elements(*self._show_visually_locator)
        return _[0]

    @property
    def link_to(self):
        return self.find_element(*self._link_to_locator)

    def select_link_to(self, value):
        select_option(value, self.link_to)

    @property
    def edit_content_link(self):
        edit_content = self.find_elements(*self._edit_content_link_locator)
        return edit_content[len(edit_content) - 1]

    @property
    def edit_card(self):
        return self.PageBuilderEditCardBase(self)

    @property
    def add_content_button(self):
        return self.find_element(*self._add_content_button_locator)

    @property
    def edit_slide(self):
        return self.PageBuilderEditSlideBase(self)

    @property
    def edit_banner(self):
        return self.PageBuilderEditBannerBase(self)

    # Taxonomies
    def taxonomy(self, taxonomy):
        taxonomies = {
            "audience": self._audience_locator,
            "related format": self._related_format_locator,
            "programs and campaigns": self._programs_and_campaigns_locator,
            "genre": self._genre_locator,
            "topic": self._topic_locator,
            "tags": self._tags_locator
        }

        return self.find_element(*(taxonomies.get(taxonomy.casefold())))

    def select_taxonomy(self, taxonomy_dropdown, taxonomy_name):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == taxonomy_name:
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def featured(self):
        return self.find_element(*self._featured_locator)

    @property
    def evergreen(self):
        return self.find_element(*self._evergreen_locator)

    def select_featured_or_evergreen(self, option, value):
        options = ["Include Featured Content", "Limit to only Featured Content", "Exclude Featured Content"]
        if option.casefold() == "featured":
            select = Select(self.featured)
        elif option.casefold() == "evergreen":
            select = Select(self.evergreen)
        else:
            raise NoSuchElementException

        if value in options:
            select.select_by_visible_text(value)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def display_tab(self):
        return self.find_element(*self._display_tab_locator)

    @property
    def card_description(self):
        return self.find_element(*self._card_description_locator)

    def select_card_description(self, value):
        select_option(value, self.card_description)

    @property
    def author_information(self):
        return self.find_element(*self._author_information_locator)

    def select_author_information(self, value):
        select_option(value, self.author_information)

    @property
    def content_types(self):
        if self.find_element(*self._content_types_checkbox_locator).is_enabled():
            return self.find_element(*self._content_types_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def audience(self):
        if self.find_element(*self._audience_checkbox_locator).is_enabled():
            return self.find_element(*self._audience_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def related_format(self):
        if self.find_element(*self._related_format_checkbox_locator).is_enabled():
            return self.find_element(*self._related_format_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def programs_and_campaigns(self):
        if self.find_element(*self._programs_and_campaigns_checkbox_locator).is_enabled():
            return self.find_element(*self._programs_and_campaigns_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def genre(self):
        if self.find_element(*self._genre_checkbox_locator).is_enabled():
            return self.find_element(*self._genre_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def topic(self):
        if self.find_element(*self._topic_checkbox_locator).is_enabled():
            return self.find_element(*self._topic_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def tags(self):
        if self.find_element(*self._tags_checkbox_locator).is_enabled():
            return self.find_element(*self._tags_checkbox_locator)
        else:
            raise NoSuchElementException

    @property
    def styles_tab(self):
        return self.find_element(*self._styles_tab_locator)

    @property
    def size(self):
        return self.find_element(*self._size_locator)

    def select_size(self, value):
        select_option(value, self.size)

    @property
    def left_alignment(self):
        return self.find_element(*self._left_alignment_locator)

    @property
    def right_alignment(self):
        return self.find_element(*self._right_alignment_locator)

    @property
    def center_alignment(self):
        return self.find_element(*self._center_alignment_locator)

    @property
    def title_size(self):
        return self.find_element(*self._title_size_locator)

    def select_title_size(self, value):
        select_option(value, self.title_size)

    @property
    def align_text(self):
        return self.find_element(*self._align_text_locator)

    def select_align_text(self, value):
        select_option(value, self.align_text)

    @property
    def image_position(self):
        return self.find_element(*self._image_position_locator)

    def select_image_position(self, value):
        select_option(value, self.image_position)

    @property
    def image_crop(self):
        return self.find_element(*self._image_crop_locator)

    def select_image_crop(self, value):
        select_option(value, self.image_crop)

    @property
    def border_style(self):
        return self.find_element(*self._border_style_locator)

    def select_border_style(self, value):
        select_option(value, self.border_style)

    @property
    def poll_card_style(self):
        return self.find_element(*self._poll_card_style_locator)

    def select_poll_card_style(self, value):
        select_option(value, self.poll_card_style)

    @property
    def advanced_tab(self):
        return self.find_element(*self._advanced_tab_locator)

    @property
    def top_margin(self):
        return self.find_element(*self._top_margin_locator)

    @property
    def right_margin(self):
        return self.find_element(*self._right_margin_locator)

    @property
    def bottom_margin(self):
        return self.find_element(*self._bottom_margin_locator)

    @property
    def left_margin(self):
        return self.find_element(*self._left_margin_locator)

    @property
    def breakpoint(self):
        return self.find_element(*self._breakpoint_locator)

    def select_breakpoint(self, value):
        select_option(value, self.breakpoint)

    @property
    def display(self):
        return self.find_element(*self._display_locator)

    def select_display(self, value):
        select_option(value, self.display)

    def save(self):
        save = self.find_elements(*self._save_locator)
        self.driver.execute_script("arguments[0].click();", save[0])

    @property
    def save_as(self):
        return self.find_element(*self._save_as_locator)

    @property
    def cancel(self):
        cancel = self.find_elements(*self._cancel_locator)
        return cancel[0]

    class PageBuilderEditBannerBase(Region):

        _heading_locator = (By.CSS_SELECTOR, "form[data-type='bw_banner_form_field']")
        _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
        _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")
        _choose_banner_dropdown_locator = (By.CSS_SELECTOR, "select[name='banner']")
        _callout_display_dropdown_locator = (By.CSS_SELECTOR, "select[name='callout_display']")
        _add_banner_locator = (By.CSS_SELECTOR, "a[data-field='bw_banners']")

        @property
        def choose_banner_dropdown(self):
            choose_banner_dropdown = self.find_elements(*self._choose_banner_dropdown_locator)
            return choose_banner_dropdown[len(choose_banner_dropdown) - 1]

        def select_banner(self, value):
            select_option(value, self.choose_banner_dropdown)

        @property
        def callout_display_dropdown(self):
            callout_display_dropdown = self.find_elements(*self._callout_display_dropdown_locator)
            return callout_display_dropdown[len(callout_display_dropdown) - 1]

        def select_callout_display(self, value):
            select_option(value, self.callout_display_dropdown)

        @property
        def save(self):
            save = self.find_elements(*self._save_locator)
            return save[len(save) - 1]

        @property
        def cancel(self):
            cancel = self.find_elements(*self._cancel_locator)
            return cancel[len(cancel) - 1]

        @property
        def add_banner(self):
            return self.find_element(*self._add_banner_locator)

    class PageBuilderEditSlideBase(Region):

        _heading_locator = (By.CSS_SELECTOR, "form[data-type='bw_hero_slide_form_field']")
        _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
        _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")
        _select_slide_dropdown_locator = (By.CSS_SELECTOR, "select.js-bw-hero-slide-id")

        @property
        def hero_slide_dropdown(self):
            hero_slide_dropdown = self.find_elements(*self._select_slide_dropdown_locator)
            return hero_slide_dropdown[len(hero_slide_dropdown) - 1]

        def select_hero_slide(self, value):
            select_option(value, self.hero_slide_dropdown)

        @property
        def save(self):
            save = self.find_elements(*self._save_locator)
            return save[len(save) - 1]

        @property
        def cancel(self):
            cancel = self.find_elements(*self._cancel_locator)
            return cancel[len(cancel) - 1]

    class PageBuilderEditCardBase(Region):

        _heading_locator = (By.CSS_SELECTOR, "form[data-type='bw_card_form_field']")
        _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
        _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

        # General Tab
        _general_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=general]")
        _choose_card_type_locator = (By.CSS_SELECTOR, "select[name='post_type']")
        _choose_card_locator = (By.CSS_SELECTOR, "#fl-field-post_id > td > div > div")
        _choose_card_dropdown_locator = (By.CSS_SELECTOR, "#fl-field-post_id > td > div > div > div > ul > li")
        _choose_card_search_input_locator = (By.CSS_SELECTOR, ".chosen-search >.chosen-search-input")
        _active_label_locator = (By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--active")
        _expired_label_locator = (By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--expired")
        _scheduled_for_label_locator = (By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--scheduled")

        # Schedule
        _starting_locator = (By.CSS_SELECTOR, "tr#fl-field-schedule_start > td > div")
        _ending_locator = (By.CSS_SELECTOR, "tr#fl-field-schedule_end > td > div")

        # Styles Tab
        _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=general]")
        _solid_color_style_locator = (By.CSS_SELECTOR, "select[name='solid_color_style']")

        @property
        def general_tab(self):
            return self.find_element(*self._general_tab_locator)

        @property
        def card_type(self):
            card_type = self.find_elements(*self._choose_card_type_locator)
            return card_type[len(card_type) - 1]

        def select_card_type(self, value):
            select_option(value, self.card_type)

        @property
        def choose_card(self):
            self.wait.until(EC.presence_of_element_located(self._choose_card_locator))
            return self.find_element(*self._choose_card_locator)

        @property
        def choose_card_search_input(self):
            return self.find_element(*self._choose_card_search_input_locator)

        def select_card_by_title(self, card):
            self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
            results = self.find_elements(*self._choose_card_dropdown_locator)

            _ = None

            for index, result in enumerate(results):
                try:
                    if result.get_attribute("textContent").casefold() == card.casefold():
                        self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
                        # Redefining temporary list to avoid StaleElementException
                        _ = self.find_elements(*self._choose_card_dropdown_locator)
                        return _[index]
                except Exception as exception:
                    raise exception

            if _ is None:
                raise NoSuchElementException

        def select_card_by_index(self, index):
            self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
            results = self.find_elements(*self._choose_card_dropdown_locator)

            _ = []

            for index, result in enumerate(results):
                _.append(result)

            try:
                return _[index]
            except NoSuchElementException:
                raise NoSuchElementException

        @property
        def save(self):
            save = self.find_elements(*self._save_locator)
            return save[len(save) - 1]

        @property
        def cancel(self):
            cancel = self.find_elements(*self._cancel_locator)
            return cancel[len(cancel) - 1]

        @property
        def starting(self):
            return self.find_element(*self._starting_locator)

        @property
        def ending(self):
            return self.find_element(*self._ending_locator)

        @property
        def active_label(self):
            return self.find_elements(*self._active_label_locator)

        @property
        def expired_label(self):
            return self.find_elements(*self._expired_label_locator)

        @property
        def scheduled_for_label(self):
            return self.find_elements(*self._scheduled_for_label_locator)

        @property
        def schedule_picker(self):
            return self.SchedulePicker(self)

        class SchedulePicker(Region):

            _previous_next_month_locator = (By.CSS_SELECTOR,"div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-months > span > svg")
            _days_locator = (By.CSS_SELECTOR,"div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span")
            _today_locator = (By.CSS_SELECTOR,"div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span.flatpickr-day.today")
            _hour_minute_locator = (By.CSS_SELECTOR,"body > div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-time > div > input")
            _am_pm_locator = (By.CSS_SELECTOR,"body > div.flatpickr-calendar.hasTime.animate.showTimeInput.open.arrowBottom > div.flatpickr-time > span.flatpickr-am-pm")

            @property
            def previous_month(self):
                month = self.find_elements(*self._previous_next_month_locator)
                return month[0]

            @property
            def next_month(self):
                month = self.find_elements(*self._previous_next_month_locator)
                return month[1]

            def days(self, index):
                days = self.find_elements(*self._days_locator)
                return days[index]

            @property
            def today(self):
                return self.find_element(*self._today_locator)

            @property
            def hour(self):
                hour = self.find_elements(*self._hour_minute_locator)
                return hour[0]

            @property
            def minute(self):
                minute = self.find_elements(*self._hour_minute_locator)
                return minute[1]

            @property
            def am_pm(self):
                return self.find_element(*self._am_pm_locator)


class PageBuilderMasonryCardCollectionBase(PageBuilderModuleBase):

    _number_of_columns_locator = (By.CSS_SELECTOR, "#fl-field-num_cards_row > td > div > select")

    @property
    def number_of_columns(self):
        return self.find_element(*self._number_of_columns_locator)

    def select_number_of_columns(self, value):
        select_option(value, self.number_of_columns)


class PageBuilderTiledCardBase(PageBuilderModuleBase):

    _number_of_cards_locator = (By.CSS_SELECTOR, "#fl-field-num_cards > td > div > select")

    @property
    def number_of_cards(self):
        return self.find_element(*self._number_of_cards_locator)

    def select_number_of_cards(self, value):
        select_option(value, self.number_of_cards)


class PageBuilderHeroSliderBase(PageBuilderModuleBase):

    _controls_display_locator = (By.CSS_SELECTOR, "select[name='carousel_controls']")
    _slide_order_locator = (By.CSS_SELECTOR, "select[name='slide_order']")

    @property
    def controls_display(self):
        return self.find_element(*self._controls_display_locator)

    def select_controls_display(self, value):
        select_option(value, self.controls_display)

    @property
    def slide_order(self):
        return self.find_element(*self._slide_order_locator)

    def select_slide_order(self, value):
        select_option(value, self.slide_order)

    def select_slide(self, name):
        self.edit_content_link.click()
        self.edit_slide.hero_slide_dropdown.click()
        self.edit_slide.select_hero_slide(name)
        self.edit_slide.save.click()


class RowModulesBase(Region):

    @property
    def column_settings(self):
        return self.ColumnSettings(self)

    class ColumnSettings(Region):

        _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-lightbox-header-wrap > div.fl-builder-settings-tabs > a[href*='style']")
        _advanced_tab_locator = (By.CSS_SELECTOR, "div.fl-lightbox-header-wrap > div.fl-builder-settings-tabs > a[href*='advanced']")
        _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
        _save_as_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save-as.fl-builder-button.fl-builder-button-large")
        _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

        @property
        def styles_tab(self):
            return self.find_element(*self._styles_tab_locator)

        @property
        def styles_tab_contents(self):
            return self.Styles(self)

        class Styles(Region):

            _width_locator = (By.CSS_SELECTOR, "input[name='size']")
            _minimum_height_input_locator = (By.CSS_SELECTOR, "input[name='min_height']")
            _minimum_height_unit_locator = (By.CSS_SELECTOR, "select[name='min_height_unit']")
            _equalize_heights_locator = (By.CSS_SELECTOR, "#fl-field-equal_height > td > div > select")
            _vertical_alignment_locator = (By.CSS_SELECTOR, "#fl-field-content_alignment > td > div > select")
            _vertical_alignment_results_locator = (By.CSS_SELECTOR, "#fl-field-content_alignment > td > div > select")

            @property
            def width(self):
                return self.find_element(*self._width_locator)

            @property
            def minimum_height(self):
                return self.find_element(*self._minimum_height_input_locator)

            @property
            def minimum_height_unit(self):
                return self.find_element(*self._minimum_height_unit_locator)

            def select_minimum_height_unit(self, value):
                select_option(value, self.minimum_height_unit)

            @property
            def equalize_heights(self):
                return self.find_element(*self._equalize_heights_locator)

            def select_equalize_heights(self, value):
                select_option(value, self.equalize_heights)

            @property
            def vertical_alignment(self):
                return self.find_element(*self._vertical_alignment_locator)

            def select_vertical_alignment(self, value):
                select_option(value, self.vertical_alignment)

        @property
        def advanced_tab(self):
            return self.find_element(*self._advanced_tab_locator)

        @property
        def advanced_tab_contents(self):
            return self.Advanced(self)

        class Advanced(Region):

            _margins_top_locator = (By.CSS_SELECTOR, "input[name='margin_top']")
            _margins_right_locator = (By.CSS_SELECTOR, "input[name='margin_right']")
            _margins_bottom_locator = (By.CSS_SELECTOR, "input[name='margin_bottom']")
            _margins_left_locator = (By.CSS_SELECTOR, "input[name='margin_left']")
            _margins_unit_locator = (By.CSS_SELECTOR, "select[name='margin_unit']")
            _padding_top_locator = (By.CSS_SELECTOR, "input[name='padding_top']")
            _padding_right_locator = (By.CSS_SELECTOR, "input[name='padding_right']")
            _padding_bottom_locator = (By.CSS_SELECTOR, "input[name='padding_bottom']")
            _padding_left_locator = (By.CSS_SELECTOR, "input[name='padding_left']")
            _padding_unit_locator = (By.CSS_SELECTOR, "select[name='padding_unit']")
            _breakpoint_locator = (By.CSS_SELECTOR, "select[name='responsive_display']")
            _stacking_order_locator = (By.CSS_SELECTOR, "select[name='responsive_order']")
            _display_locator = (By.CSS_SELECTOR, "select[name='visibility_display']")

            @property
            def margins_top(self):
                return self.find_element(*self._margins_top_locator)

            @property
            def margins_right(self):
                return self.find_element(*self._margins_right_locator)

            @property
            def margins_bottom(self):
                return self.find_element(*self._margins_bottom_locator)

            @property
            def margins_left(self):
                return self.find_element(*self._margins_left_locator)

            @property
            def margins_unit(self):
                return self.find_element(*self._margins_unit_locator)

            def select_margins_unit(self, value):
                select_option(value, self.margins_unit)

            @property
            def padding_top(self):
                return self.find_element(*self._padding_top_locator)

            @property
            def padding_right(self):
                return self.find_element(*self._padding_right_locator)

            @property
            def padding_bottom(self):
                return self.find_element(*self._padding_bottom_locator)

            @property
            def padding_left(self):
                return self.find_element(*self._padding_left_locator)

            @property
            def padding_unit(self):
                return self.find_element(*self._padding_unit_locator)

            def select_padding_unit(self, value):
                select_option(value, self.padding_unit)

            @property
            def breakpoint(self):
                return self.find_element(*self._breakpoint_locator)

            def select_breakpoint(self, value):
                select_option(value, self.breakpoint)

            @property
            def stacking_order(self):
                return self.find_element(*self._stacking_order_locator)

            def select_stacking_order(self, value):
                select_option(value, self.stacking_order)

            @property
            def display(self):
                return self.find_element(*self._display_locator)

            def select_display(self, value):
                select_option(value, self.display)

        @property
        def save(self):
            return self.find_element(*self._save_locator)

        @property
        def save_as(self):
            return self.find_element(*self._save_as_locator)

        @property
        def cancel(self):
            return self.find_element(*self._cancel_locator)


class SidebarMenuBase(Region):

    _content_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=content]")
    _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=styles]")
    _advanced_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=advanced]")
    _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
    _save_as_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save-as.fl-builder-button.fl-builder-button-large")
    _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

    @property
    def content_tab(self):
        return self.find_element(*self._content_tab_locator)

    @property
    def content_tab_contents(self):
        return self.ContentTab(self)

    class ContentTab(Region):

        _select_a_menu_locator = (By.CSS_SELECTOR, "#fl-field-menu_id > td > div > div > a")
        _select_a_menu_results_locator = (By.CSS_SELECTOR, "#fl-field-menu_id > td > div > div > div > ul > li")

        @property
        def select_a_menu(self):
            return self.find_element(*self._select_a_menu_locator)

        def sidebar_menu_results(self, index):
            try:
                _ = self.find_elements(*self._select_a_menu_results_locator)
                return _[index]
            except IndexError:
                raise IndexError

    @property
    def styles_tab(self):
        return self.find_element(*self._styles_tab_locator)

    @property
    def style_tab_contents(self):
        return self.StylesTab(self)

    class StylesTab(Region):

        _alignment_of_menu_locator = (By.CSS_SELECTOR, "#fl-field-menu_alignment > td > div > select")

        @property
        def alignment_of_menu(self):
            return self.find_element(*self._alignment_of_menu_locator)

        def select_alignment_of_menu(self, value):
            select_option(value, self.alignment_of_menu)

    @property
    def advanced_tab(self):
        return self.find_element(*self._advanced_tab_locator)

    @property
    def advanced_tab_contents(self):
        return self.AdvancedTab(self)

    class AdvancedTab(Region):

        _margins_top_locator = (By.CSS_SELECTOR, "input[name='margin_top']")
        _margins_right_locator = (By.CSS_SELECTOR, "input[name='margin_right']")
        _margins_bottom_locator = (By.CSS_SELECTOR, "input[name='margin_bottom']")
        _margins_left_locator = (By.CSS_SELECTOR, "input[name='margin_left']")
        _margins_unit_locator = (By.CSS_SELECTOR, "select[name='margin_unit']")
        _breakpoint_locator = (By.CSS_SELECTOR, "select[name='responsive_display']")
        _display_locator = (By.CSS_SELECTOR, "select[name='visibility_display']")

        @property
        def margins_top(self):
            return self.find_element(*self._margins_top_locator)

        @property
        def margins_right(self):
            return self.find_element(*self._margins_right_locator)

        @property
        def margins_bottom(self):
            return self.find_element(*self._margins_bottom_locator)

        @property
        def margins_left(self):
            return self.find_element(*self._margins_left_locator)

        @property
        def margins_unit(self):
            return self.find_element(*self._margins_unit_locator)

        def select_margins_unit(self, value):
            select_option(value, self.margins_unit)

        @property
        def breakpoint(self):
            return self.find_element(*self._breakpoint_locator)

        def select_breakpoint(self, value):
            select_option(value, self.breakpoint)

        @property
        def display(self):
            return self.find_element(*self._display_locator)

        def select_display(self, value):
            select_option(value, self.display)

    @property
    def save(self):
        return self.find_element(*self._save_locator)

    @property
    def save_as(self):
        return self.find_element(*self._save_as_locator)

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)


def select_option(value, dropdown):
    options = []

    select = Select(dropdown)
    for option in select.options:
        options.append(option.get_attribute("textContent"))

    if value in options:
        select.select_by_visible_text(value)
    else:
        raise ElementNotSelectableException("Option not found.")


class CardSliderBase(Region):

    _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
    _save_as_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save-as.fl-builder-button.fl-builder-button-large")
    _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")
    _content_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=content]")
    _display_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=display]")
    _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=styles]")
    _advanced_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=advanced]")

    @property
    def content_tab(self):
        return self.find_element(*self._content_tab_locator)

    @property
    def content_tab_contents(self):
        return self.ContentTab(self)

    class ContentTab(Region):

        _show_visually_locator = (By.CSS_SELECTOR, "#fl-field-module_heading > td > div > div > div.d-inline > label > input")
        _module_heading_locator = (By.CSS_SELECTOR, "input[name='module_heading[][title]']")
        _link_to_locator = (By.CSS_SELECTOR, "select[name='exit_link_type']")
        _edit_card_locator = (By.CSS_SELECTOR, "a.fl-form-field-edit")
        _add_card_locator = (By.CSS_SELECTOR, "a[data-field='bw_cards']")

        @property
        def module_heading(self):
            return self.find_element(*self._module_heading_locator)

        @property
        def show_visually(self):
            _ = self.find_elements(*self._show_visually_locator)
            return _[0]

        @property
        def link_to(self):
            return self.find_element(*self._link_to_locator)

        def select_link_to(self, value):
            options = []

            select = Select(self.link_to)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def edit_card_button(self):
            edit_card = self.find_elements(*self._edit_card_locator)
            return edit_card[len(edit_card) - 1]

        @property
        def edit_card(self):
            return self.PageBuilderEditCardBase(self)

        class PageBuilderEditCardBase(Region):

            _heading_locator = (By.CSS_SELECTOR, "form[data-type='bw_card_form_field']")
            _save_locator = (
            By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
            _cancel_locator = (
            By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")

            # General Tab
            _general_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=general]")
            _choose_card_type_locator = (By.CSS_SELECTOR, "select[name='post_type']")
            _choose_card_locator = (By.CSS_SELECTOR, "#fl-field-post_id > td > div > div")
            _choose_card_dropdown_locator = (By.CSS_SELECTOR, "#fl-field-post_id > td > div > div > div > ul > li")
            _active_label_locator = (
                By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--active")
            _expired_label_locator = (
                By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--expired")
            _scheduled_for_label_locator = (
                By.CSS_SELECTOR, "div.fl-form-field-after.bw-scheduling-info.bw-scheduling-info--scheduled")

            # Schedule
            _starting_locator = (By.CSS_SELECTOR, "tr#fl-field-schedule_start > td > div")
            _ending_locator = (By.CSS_SELECTOR, "tr#fl-field-schedule_end > td > div")

            # Styles Tab
            _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=general]")
            _solid_color_style_locator = (By.CSS_SELECTOR, "select[name='solid_color_style']")

            @property
            def general_tab(self):
                return self.find_element(*self._general_tab_locator)

            @property
            def card_type(self):
                card_type = self.find_elements(*self._choose_card_type_locator)
                return card_type[len(card_type) - 1]

            def select_card_type(self, value):
                options = []

                select = Select(self.card_type)
                for option in select.options:
                    options.append(option.get_attribute("textContent"))

                if value in options:
                    select.select_by_visible_text(value)
                else:
                    raise ElementNotSelectableException("Option not found.")

            @property
            def choose_card(self):
                self.wait.until(EC.presence_of_element_located(self._choose_card_locator))
                return self.find_element(*self._choose_card_locator)

            def select_card_by_title(self, card):
                self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
                results = self.find_elements(*self._choose_card_dropdown_locator)

                _ = None

                for index, result in enumerate(results):
                    try:
                        if result.get_attribute("textContent").casefold() == card.casefold():
                            self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
                            # Redefining temporary list to avoid StaleElementException
                            _ = self.find_elements(*self._choose_card_dropdown_locator)
                            return _[index]
                    except Exception as exception:
                        raise exception

                if _ is None:
                    raise NoSuchElementException

            def select_card_by_index(self, index):
                self.wait.until(EC.visibility_of_all_elements_located(self._choose_card_dropdown_locator))
                results = self.find_elements(*self._choose_card_dropdown_locator)

                _ = []

                for index, result in enumerate(results):
                    _.append(result)

                try:
                    return _[index]
                except NoSuchElementException:
                    raise NoSuchElementException

            @property
            def save(self):
                save = self.find_elements(*self._save_locator)
                return save[len(save) - 1]

            @property
            def cancel(self):
                cancel = self.find_elements(*self._cancel_locator)
                return cancel[len(cancel) - 1]

            @property
            def starting(self):
                return self.find_element(*self._starting_locator)

            @property
            def ending(self):
                return self.find_element(*self._ending_locator)

            @property
            def active_label(self):
                return self.find_elements(*self._active_label_locator)

            @property
            def expired_label(self):
                return self.find_elements(*self._expired_label_locator)

            @property
            def scheduled_for_label(self):
                return self.find_elements(*self._scheduled_for_label_locator)

            @property
            def schedule_picker(self):
                return self.SchedulePicker(self)

            class SchedulePicker(Region):

                _previous_next_month_locator = (By.CSS_SELECTOR, "div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-months > span > svg")
                _days_locator = (By.CSS_SELECTOR, "div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span")
                _today_locator = (By.CSS_SELECTOR, "div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-innerContainer > div > div.flatpickr-days > div > span.flatpickr-day.today")
                _hour_minute_locator = (By.CSS_SELECTOR, "body > div.flatpickr-calendar.hasTime.animate.showTimeInput.arrowBottom.open > div.flatpickr-time > div > input")
                _am_pm_locator = (By.CSS_SELECTOR, "body > div.flatpickr-calendar.hasTime.animate.showTimeInput.open.arrowBottom > div.flatpickr-time > span.flatpickr-am-pm")

                @property
                def previous_month(self):
                    month = self.find_elements(*self._previous_next_month_locator)
                    return month[0]

                @property
                def next_month(self):
                    month = self.find_elements(*self._previous_next_month_locator)
                    return month[1]

                def days(self, index):
                    days = self.find_elements(*self._days_locator)
                    return days[index]

                @property
                def today(self):
                    return self.find_element(*self._today_locator)

                @property
                def hour(self):
                    hour = self.find_elements(*self._hour_minute_locator)
                    return hour[0]

                @property
                def minute(self):
                    minute = self.find_elements(*self._hour_minute_locator)
                    return minute[1]

                @property
                def am_pm(self):
                    return self.find_element(*self._am_pm_locator)

        @property
        def add_card(self):
            return self.find_element(*self._add_card_locator)

    @property
    def display_tab(self):
        return self.find_element(*self._display_tab_locator)

    @property
    def display_tab_contents(self):
        return self.DisplayTab(self)

    class DisplayTab(Region):

        _carousel_controls_locator = (By.CSS_SELECTOR, "select[name='carousel_controls']")
        _number_of_items_locator = (By.CSS_SELECTOR, "select[name='num_cards']")
        _slide_order_locator = (By.CSS_SELECTOR, "select[name='slide_order']")
        _card_title_locator = (By.CSS_SELECTOR, "select[name='show_card_title']")
        _card_description_locator = (By.CSS_SELECTOR, "select[name='show_description']")

        @property
        def carousel_controls(self):
            return self.find_element(*self._carousel_controls_locator)

        def select_carousel_controls(self, value):
            options = []

            select = Select(self.carousel_controls)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def number_of_items(self):
            return self.find_element(*self._number_of_items_locator)

        def select_number_of_items(self, value):
            options = []

            select = Select(self.number_of_items)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def slide_order(self):
            return self.find_element(*self._slide_order_locator)

        def select_slide_order(self, value):
            options = []

            select = Select(self.slide_order)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def card_title(self):
            return self.find_element(*self._card_title_locator)

        def select_card_title(self, value):
            options = []

            select = Select(self.card_title)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def card_description(self):
            return self.find_element(*self._card_description_locator)

        def select_card_description(self, value):
            options = []

            select = Select(self.card_description)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

    @property
    def styles_tab(self):
        return self.find_element(*self._styles_tab_locator)

    @property
    def styles_tab_contents(self):
        return self.StylesTab(self)

    class StylesTab(Region):

        _size_locator = (By.CSS_SELECTOR, "select[name='module_heading_font_size']")
        _left_alignment_locator = (By.CSS_SELECTOR, "button[data-value='left']")
        _center_alignment_locator = (By.CSS_SELECTOR, "button[data-value='center']")
        _right_alignment_locator = (By.CSS_SELECTOR, "button[data-value='right']")

        @property
        def size(self):
            return self.find_element(*self._size_locator)

        def select_size(self, value):
            options = []

            select = Select(self.size)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def left_alignment(self):
            return self.find_element(*self._left_alignment_locator)

        @property
        def right_alignment(self):
            return self.find_element(*self._right_alignment_locator)

        @property
        def center_alignment(self):
            return self.find_element(*self._center_alignment_locator)

    @property
    def advanced_tab(self):
        return self.find_element(*self._advanced_tab_locator)

    @property
    def advanced_tab_contents(self):
        return self.AdvancedTab(self)

    class AdvancedTab(Region):

        _top_margin_locator = (By.CSS_SELECTOR, "input[name='margin_top']")
        _right_margin_locator = (By.CSS_SELECTOR, "input[name='margin_right']")
        _bottom_margin_locator = (By.CSS_SELECTOR, "input[name='margin_bottom']")
        _left_margin_locator = (By.CSS_SELECTOR, "input[name='margin_left']")
        _margin_unit_locator = (By.CSS_SELECTOR, "select[name='margin_unit']")

        _breakpoint_locator = (By.CSS_SELECTOR, "select[name='responsive_display']")
        _display_locator = (By.CSS_SELECTOR, "select[name='visibility_display']")

        @property
        def margins_top(self):
            return self.find_element(*self._top_margin_locator)

        @property
        def margins_right(self):
            return self.find_element(*self._right_margin_locator)

        @property
        def margins_bottom(self):
            return self.find_element(*self._bottom_margin_locator)

        @property
        def margins_left(self):
            return self.find_element(*self._left_margin_locator)

        @property
        def margins_unit(self):
            return self.find_element(*self._margin_unit_locator)

        def select_margins_unit(self, value):
            options = []

            select = Select(self.margins_unit)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def breakpoint(self):
            return self.find_element(*self._breakpoint_locator)

        def select_breakpoint(self, value):
            options = []

            select = Select(self.breakpoint)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def display(self):
            return self.find_element(*self._display_locator)

        def select_display(self, value):
            options = []

            select = Select(self.display)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

    def save(self):
        save = self.find_elements(*self._save_locator)
        self.driver.execute_script("arguments[0].click();", save[0])

    @property
    def save_as(self):
        return self.find_element(*self._save_as_locator)

    @property
    def cancel(self):
        cancel = self.find_elements(*self._cancel_locator)
        return cancel[0]


class CarouselsBase(Region):

    _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
    _save_as_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save-as.fl-builder-button.fl-builder-button-large")
    _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")
    _content_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=content]")
    _styles_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=styles]")
    _advanced_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=advanced]")

    @property
    def content_tab(self):
        return self.find_element(*self._content_tab_locator)

    @property
    def content_tab_contents(self):
        return self.ContentTab(self)

    class ContentTab(Region):

        _show_visually_locator = (By.CSS_SELECTOR, "#fl-field-module_heading > td > div > div > div.d-inline > label > input")
        _module_heading_locator = (By.CSS_SELECTOR, "input[name='module_heading[][title]']")
        _search_url_locator = (By.CSS_SELECTOR, "input[name='carousel_url']")

        @property
        def module_heading(self):
            return self.find_element(*self._module_heading_locator)

        @property
        def show_visually(self):
            _ = self.find_elements(*self._show_visually_locator)
            return _[0]

        @property
        def search_url(self):
            return self.find_element(*self._search_url_locator)

    @property
    def styles_tab(self):
        return self.find_element(*self._styles_tab_locator)

    @property
    def styles_tab_contents(self):
        return self.StylesTab(self)

    class StylesTab(Region):

        _size_locator = (By.CSS_SELECTOR, "select[name='module_heading_font_size']")
        _left_alignment_locator = (By.CSS_SELECTOR, "button[data-value='left']")
        _center_alignment_locator = (By.CSS_SELECTOR, "button[data-value='center']")
        _right_alignment_locator = (By.CSS_SELECTOR, "button[data-value='right']")

        @property
        def size(self):
            return self.find_element(*self._size_locator)

        def select_size(self, value):
            options = []

            select = Select(self.size)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def left_alignment(self):
            return self.find_element(*self._left_alignment_locator)

        @property
        def right_alignment(self):
            return self.find_element(*self._right_alignment_locator)

        @property
        def center_alignment(self):
            return self.find_element(*self._center_alignment_locator)

    @property
    def advanced_tab(self):
        return self.find_element(*self._advanced_tab_locator)

    @property
    def advanced_tab_contents(self):
        return self.AdvancedTab(self)

    class AdvancedTab(Region):

        _top_margin_locator = (By.CSS_SELECTOR, "input[name='margin_top']")
        _right_margin_locator = (By.CSS_SELECTOR, "input[name='margin_right']")
        _bottom_margin_locator = (By.CSS_SELECTOR, "input[name='margin_bottom']")
        _left_margin_locator = (By.CSS_SELECTOR, "input[name='margin_left']")
        _margin_unit_locator = (By.CSS_SELECTOR, "select[name='margin_unit']")

        _breakpoint_locator = (By.CSS_SELECTOR, "select[name='responsive_display']")
        _display_locator = (By.CSS_SELECTOR, "select[name='visibility_display']")

        @property
        def margins_top(self):
            return self.find_element(*self._top_margin_locator)

        @property
        def margins_right(self):
            return self.find_element(*self._right_margin_locator)

        @property
        def margins_bottom(self):
            return self.find_element(*self._bottom_margin_locator)

        @property
        def margins_left(self):
            return self.find_element(*self._left_margin_locator)

        @property
        def margins_unit(self):
            return self.find_element(*self._margin_unit_locator)

        def select_margins_unit(self, value):
            options = []

            select = Select(self.margins_unit)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def breakpoint(self):
            return self.find_element(*self._breakpoint_locator)

        def select_breakpoint(self, value):
            options = []

            select = Select(self.breakpoint)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def display(self):
            return self.find_element(*self._display_locator)

        def select_display(self, value):
            options = []

            select = Select(self.display)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

    def save(self):
        save = self.find_elements(*self._save_locator)
        self.driver.execute_script("arguments[0].click();", save[0])

    @property
    def save_as(self):
        return self.find_element(*self._save_as_locator)

    @property
    def cancel(self):
        cancel = self.find_elements(*self._cancel_locator)
        return cancel[0]


class CommentsCarouselBase(CarouselsBase):

    _display_tab_locator = (By.CSS_SELECTOR, "div.fl-builder-settings-tabs > a[href*=display]")

    @property
    def content_tab_contents(self):
        return self.ContentTab(self)

    class ContentTab(Region):

        _show_visually_locator = (By.CSS_SELECTOR, "#fl-field-module_heading > td > div > div > div.d-inline > label > input")
        _module_heading_locator = (By.CSS_SELECTOR, "input[name='module_heading[][title]']")
        _link_to_locator = (By.CSS_SELECTOR, "select[name='exit_link_type']")
        _exit_link_url_locator = (By.CSS_SELECTOR, "input[name='exit_link']")
        _search_url_locator = (By.CSS_SELECTOR, "input[name='carousel_url']")

        @property
        def module_heading(self):
            return self.find_element(*self._module_heading_locator)

        @property
        def show_visually(self):
            _ = self.find_elements(*self._show_visually_locator)
            return _[0]

        @property
        def link_to(self):
            return self.find_element(*self._link_to_locator)

        def select_link_to(self, value):
            options = []

            select = Select(self.link_to)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")

        @property
        def exit_link_url(self):
            return self.find_element(*self._exit_link_url_locator)

        @property
        def search_url(self):
            return self.find_element(*self._search_url_locator)

    @property
    def display_tab(self):
        return self.find_element(*self._display_tab_locator)

    @property
    def display_tab_contents(self):
        return self.DisplayTab(self)

    class DisplayTab(Region):

        _number_of_comments_locator = (By.CSS_SELECTOR, "select[name='num_cards']")

        @property
        def number_of_comments(self):
            return self.find_element(*self._number_of_comments_locator)

        def select_number_of_comments(self, value):
            options = []

            select = Select(self.number_of_comments)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")


class QuotesCarouselBase(CommentsCarouselBase):

    @property
    def display_tab(self):
        return self.find_element(*self._display_tab_locator)

    @property
    def display_tab_contents(self):
        return self.DisplayTab(self)

    class DisplayTab(Region):

        _number_of_quotes_locator = (By.CSS_SELECTOR, "select[name='num_cards']")

        @property
        def number_of_quotes(self):
            return self.find_element(*self._number_of_quotes_locator)

        def select_number_of_quotes(self, value):
            options = []

            select = Select(self.number_of_quotes)
            for option in select.options:
                options.append(option.get_attribute("textContent"))

            if value in options:
                select.select_by_visible_text(value)
            else:
                raise ElementNotSelectableException("Option not found.")


class TabbedCatalogCarouselBase(CarouselsBase):

    @property
    def content_tab_contents(self):
        return self.ContentTab(self)

    class ContentTab(Region):

        _show_visually_locator = (By.CSS_SELECTOR, "#fl-field-module_heading > td > div > div > div.d-inline > label > input")
        _module_heading_locator = (By.CSS_SELECTOR, "input[name='module_heading[][title]']")
        _add_carousel_tab_locator = (By.CSS_SELECTOR, "a[data-field='bw_carousel_tab']")
        _edit_carousel_tab_locator = (By.CSS_SELECTOR, "a[data-type='bw_carousel_tab_form_field']")

        @property
        def module_heading(self):
            return self.find_element(*self._module_heading_locator)

        @property
        def show_visually(self):
            _ = self.find_elements(*self._show_visually_locator)
            return _[0]

        @property
        def add_carousel_tab(self):
            return self.find_element(*self._add_carousel_tab_locator)

        def edit_carousel_tab(self, index):
            edit_carousel_tabs = self.find_elements(*self._edit_carousel_tab_locator)
            return edit_carousel_tabs[index]

        @property
        def edit_carousel_tab_contents(self):
            return self.EditCarouselTab(self)

        class EditCarouselTab(Region):

            _save_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-save.fl-builder-button.fl-builder-button-large")
            _cancel_locator = (By.CSS_SELECTOR, "button.fl-builder-settings-cancel.fl-builder-button.fl-builder-button-large")
            _tab_label_locator = (By.CSS_SELECTOR, "input[name='tab_label']")
            _search_url_locator = (By.CSS_SELECTOR, "input[name='carousel_url']")

            @property
            def save(self):
                save = self.find_elements(*self._save_locator)
                return save[len(save) - 1]

            @property
            def cancel(self):
                cancel = self.find_elements(*self._cancel_locator)
                return cancel[len(cancel) - 1]

            @property
            def tab_label(self):
                return self.find_element(*self._tab_label_locator)

            @property
            def search_url(self):
                return self.find_element(*self._search_url_locator)
